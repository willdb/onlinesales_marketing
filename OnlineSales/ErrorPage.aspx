﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorPage.aspx.cs" Inherits="OnlineSales.ErrorPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta charset="UTF-8" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
<title>Error Handler | DirectBuy, Inc.</title>
<!-- Load Fonts -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,800italic,400,700,600,300" rel='stylesheet' type='text/css' />
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.css" />
    <!-- Custom styles for this project -->
<link rel="stylesheet" href="assets/css/main.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="fullwidth-title-bar row">
    <h2><asp:Label class="centering" ID="FriendlyErrorMsg" runat="server" ></asp:Label></h2>
</div>
    <p></p>    

    <asp:Panel ID="DetailedErrorPanel" runat="server" Visible="false">
        <p>&nbsp;</p>
        <div class="form-group">
        <h3>Detailed Error:</h3>
        </div>
        <p>
            <asp:Label ID="ErrorDetailedMsg" runat="server" Font-Size="Small" /><br />
        </p>

        <h4>Error Handler:</h4>
        <p>
            <asp:Label ID="ErrorHandler" runat="server" Font-Size="Small" /><br />
        </p>

        <h4>Detailed Error Message:</h4>
        <p>
            <asp:Label ID="InnerMessage" runat="server" Font-Size="Small" /><br />
        </p>
        <p>
            <asp:Label ID="InnerTrace" runat="server"  />
        </p>
    </asp:Panel>
    </div>
    </form>
</body>
</html>

﻿using System;
using System.Collections.Generic;

namespace OnlineSales
{
    public class JSONForAgreement
    {
        public Documentcreationinfo documentCreationInfo { get; set; }       
    }

    public class Documentcreationinfo
    {
        public string name { get; set; }
        public Recipient[] recipients { get; set; }
        public Mergefieldinfo[] mergeFieldInfo { get; set; }
        public Postsignoptions postSignOptions { get; set; }
        public string signatureType { get; set; }
        public string signatureFlow { get; set; }
        public Fileinfo[] fileInfos { get; set; }
        public string message { get; set; } 
    }

    public class Recipient
    {
        public string email { get; set; }
        public string role { get; set; }       
    }

    public class Mergefieldinfo
    {
        public string fieldName { get; set; }
        public string defaultValue { get; set; }
    }

    public class Postsignoptions
    {
        public string redirectDelay { get; set; }
        public string redirectUrl { get; set; }
    }

    public class Fileinfo
    {
        public string libraryDocumentId { get; set; }
        public string libraryDocumentName { get; set; }
    }
}

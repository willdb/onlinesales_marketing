﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineSales
{
    public class clsMemFinInfo
    {
        public string Membership_type_id { get; set; }
        public string Membership_type { get; set; }
        public string Down_payment { get; set; }
        public string Monthly_fee { get; set; }
        public string Membership_cost { get; set; }
        public string Tops_membership_cost { get; set; }
        public string TguideText { get; set; }
        public string Tops_membership_cost_negotiated { get; set; }       
    }
}
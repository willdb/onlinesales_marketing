﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineSales
{
    public class clsGetFetchedLeadDataParams
    {
        public string current_club_id { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string exclude_membership_purchased { get; set; }
    }
}
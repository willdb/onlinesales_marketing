﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Net;
using System.Text;
using System.IO;
using System.Xml;
using System.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using Newtonsoft.Json.Converters;
using System.Globalization;

namespace OnlineSales
{
    public class Helper
    {
        //Convert Dataset to JSON string
        internal string DataSetToJSON(DataSet ds)
        {
            try
            {
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (System.Data.DataColumn col in ds.Tables[0].Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }
                return serializer.Serialize(rows);
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> DataSetToJason", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        internal string DataReaderToJSON2fields(SqlDataReader dr)
        {
            try
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();
                while (dr.Read())
                {
                    dict.Add(dr[0].ToString(), dr[1].ToString());
                }
                JavaScriptSerializer json = new JavaScriptSerializer();
                return json.Serialize(dict);
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> DataReaderToJSON2fields", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        

        #region "Methods related to logging errors"

        //Log application error to Event viewer, this is requires sometimes for debugging purposes
        internal void WriteLogToToEventViewer(string msg)
        {
            try
            {
                if(!EventLog.SourceExists("OnlineSales"))
                {
                    EventLog.CreateEventSource("OnlineSales", "Application");
                }
                EventLog myLog = new EventLog();
                myLog.Source = "OnlineSales";
                myLog.WriteEntry(msg, EventLogEntryType.Error);
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> WriteLogToToEventViewer", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        //Log application level errors into database
        internal void WriteLogToDB(string ErrorType, string LeadSource, string ErrorDesc, string ErrorSource, string ErrorStack, DateTime ErrorTimeStamp)
        {
            try
            {
                DBConnection myConn = new DBConnection();
                SqlCommand myCmd = new SqlCommand();
                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = "spLogErrorsForOnLineSales";

                myCmd.Parameters.Add(new SqlParameter("@ErrorType", SqlDbType.Text)).Value = ErrorType;
                myCmd.Parameters.Add(new SqlParameter("@ErrorSource", SqlDbType.Text)).Value = ErrorSource;
                myCmd.Parameters.Add(new SqlParameter("@LeadSource", SqlDbType.Text)).Value = LeadSource;
                myCmd.Parameters.Add(new SqlParameter("@ErrorDesc", SqlDbType.Text)).Value = ErrorDesc;
                myCmd.Parameters.Add(new SqlParameter("@ErrorStack", SqlDbType.Text)).Value = ErrorStack;
                myCmd.Parameters.Add(new SqlParameter("@ErrorTimeStamp", SqlDbType.DateTime)).Value = ErrorTimeStamp;

                myCmd.Connection.Open();
                myCmd.ExecuteNonQuery();
                if (myCmd.Connection.State == ConnectionState.Open) myCmd.Connection.Close();

                myConn = null;
                myCmd.Dispose();
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> WriteErrorLogToDB", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        //Log application level errors into database
        internal void WriteLogToDB(string ErrorType, string ErrorSource, string LeadSource, string ErrorDesc, DateTime ErrorTimeStamp)
        {
            try
            {
                DBConnection myConn = new DBConnection();
                SqlCommand myCmd = new SqlCommand();
                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = "spLogErrorsForOnLineSales";

                myCmd.Parameters.Add(new SqlParameter("@ErrorType", SqlDbType.Text)).Value = ErrorType;
                myCmd.Parameters.Add(new SqlParameter("@ErrorSource", SqlDbType.Text)).Value = ErrorSource;
                myCmd.Parameters.Add(new SqlParameter("@LeadSource", SqlDbType.Text)).Value = LeadSource;
                myCmd.Parameters.Add(new SqlParameter("@ErrorDesc", SqlDbType.Text)).Value = ErrorDesc;
                myCmd.Parameters.Add(new SqlParameter("@ErrorTimeStamp", SqlDbType.DateTime)).Value = ErrorTimeStamp;

                myCmd.Connection.Open();
                myCmd.ExecuteNonQuery();
                if (myCmd.Connection.State == ConnectionState.Open) myCmd.Connection.Close();

                myConn = null;
                myCmd.Dispose();
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> WriteLogToDB", "", ex.Message, DateTime.Now);
                throw;
            }
        }


        //Log User Activity into database
        internal void WriteUserActitytoDB(string UserSessionID, string LeadSource, string ActivityStep, string ActivityStepStatus, string DataReceived, string DataSent, DateTime ActivityTimeStamp)
        {
            try
            {
                DBConnection myConn = new DBConnection();
                SqlCommand myCmd = new SqlCommand();
                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = "spLogUserActivityForOnLineSales";

                myCmd.Parameters.Add(new SqlParameter("@UserSessionID", SqlDbType.Text)).Value = UserSessionID;
                myCmd.Parameters.Add(new SqlParameter("@LeadSource", SqlDbType.Text)).Value = LeadSource;
                myCmd.Parameters.Add(new SqlParameter("@ActivityStep", SqlDbType.Text)).Value = ActivityStep;
                myCmd.Parameters.Add(new SqlParameter("@ActivityStepStatus", SqlDbType.Text)).Value = ActivityStepStatus;
                myCmd.Parameters.Add(new SqlParameter("@DataReceived", SqlDbType.Text)).Value = DataReceived;
                myCmd.Parameters.Add(new SqlParameter("@DataSent", SqlDbType.Text)).Value = DataSent;
                myCmd.Parameters.Add(new SqlParameter("@ActivityTimeStamp", SqlDbType.DateTime)).Value = ActivityTimeStamp;

                myCmd.Connection.Open();
                myCmd.ExecuteNonQuery();
                if (myCmd.Connection.State == ConnectionState.Open) myCmd.Connection.Close();

                myConn = null;
                myCmd.Dispose();
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> WriteUserActitytoDB", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        #endregion

        #region "Methods for DBMI  API Calls"

        //Test promo code for verification & process promo code
        internal PromoCodeResponse MkAPICall_ProcessPromoCode(ClsPromoParams PromoParams)
        {
            string myParams = getParamString(PromoParams);
            string URL = BuildURLDBAPIs("get_promo_code_data", myParams);
            string Result = string.Empty;
            try
            {
                using (var client = new WebClient())
                {
                    Result = client.DownloadString(URL);
                }
                PromoCodeResponse objPromocoderesponse = new PromoCodeResponse();
                objPromocoderesponse = deserializePromoResponse(Result);
                return objPromocoderesponse;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> MkAPICall_ProcessPromoCode", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        //Post Lead
        internal PostDataResponse MkAPICall_PostLead(clsPostLeadParams LeadParams, string trackingid=null)
        {
            string myParams = getParamString(LeadParams);
            //build URL to make a call to api
            string URL = BuildURLDBAPIs("post_lead", myParams, trackingid);
            string Result = string.Empty;
            try
            {
                using (var client = new WebClient())
                {
                    Result = client.DownloadString(URL);
                }

                //log actual request sent for update and actual result returned
                WriteUserActitytoDB(HttpContext.Current.Session.SessionID, trackingid, "post Lead - Raw Data ", "", Result, URL, DateTime.Now);

                PostDataResponse objPostLead = new PostDataResponse();
                objPostLead = deserializePostDataResponse(Result);
                return objPostLead;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> MkAPICall_PostLead", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // GET Club Lead Data - for checking duplicate lead
        internal FetchedLeadResponse MkAPICall_GetFetchedLeadData(clsGetFetchedLeadDataParams GetLeadParam)
        {
            string myParams = getParamString(GetLeadParam);
            string URL = BuildURLDBAPIs("get_club_lead_data", myParams);
            string Result = string.Empty;
            try
            {
                using (var client = new WebClient())
                {
                    Result = client.DownloadString(URL);
                }
                FetchedLeadResponse objFetchedLeadResposne = new FetchedLeadResponse();
                objFetchedLeadResposne = deserializeGetFetchedLeadDataResponse(Result);
                return objFetchedLeadResposne;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> MkAPICall_GetFetchedLeadData", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // GET Contract Information - one param only State ID
        internal string MkAPICall_GetContractInfo(clsGetContractInfoParams contractInfo)
        {
            string myParams = getParamString(contractInfo);
            string URL = BuildURLDBAPIs("get_contract_information", myParams);
            string Result = string.Empty;
            try
            {
                using (var client = new WebClient())
                {
                    Result = client.DownloadString(URL);
                }           
                return Result;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> MkAPICall_GetContractInfo", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // GET referral information
        internal ReferralResponse MkAPICall_GetReferralInfo(clsGetReferralInfoParams referralInfo)
        {
            string myParams = getParamString(referralInfo);
            string URL = BuildURLDBAPIs("get_referral_information", myParams);
            string Result = string.Empty;
            try
            {
                using (var client = new WebClient())
                {
                    Result = client.DownloadString(URL);
                }
                ReferralResponse objReferral = new ReferralResponse();
                objReferral = deserializeReferralResponse(Result);
                return objReferral;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> MkAPICall_GetReferralInfo", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // update lead information
        internal updateLeadResponse MkAPICall_UpdateLeadInfo(clsUpdateLeadParams UpdateLeadParam)
        {
            string myParams = getParamString(UpdateLeadParam);
            string URL = BuildURLDBAPIs("update_lead", myParams);
            string Result = string.Empty;
            try
            {
                using (var client = new WebClient())
                {
                    Result = client.DownloadString(URL);
                }
                //log actual request sent for update and actual result returned
                WriteUserActitytoDB(HttpContext.Current.Session.SessionID, "", "Update Lead - Raw Data", "", Result, URL, DateTime.Now);

                updateLeadResponse objUpdateLead = new updateLeadResponse();
                objUpdateLead = deserializeUpdateLeadResponse(Result);
                return objUpdateLead;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> MkAPICall_UpdateLeadInfo", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // Get club information by postal code
        internal ClubInfoResponse MkAPICall_GetClubByPostalCode(clsGetClubByPostalCode clubInfoByPostalCode)
        {
            string myParams = getParamString(clubInfoByPostalCode);
            string URL = BuildURLDBAPIs("get_club_by_postal_code", myParams);
            string Result = string.Empty;
            try
            {
                using (var client = new WebClient())
                {
                    Result = client.DownloadString(URL);
                }
                ClubInfoResponse objClubInfo = new ClubInfoResponse();
                objClubInfo = deserializeClubInfoResponse(Result);
                return objClubInfo;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> MkAPICall_GetClubByPostalCode", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        #endregion

        #region Methods for EcoSign API Calls"

        // oauth token refresh call to Eco sign
        internal string getEcoSignOAUTHRefreshToken()
        {
            string URL = BuildURLEcoSignOauthTokenRefresh();
            string Result = string.Empty;
            string response = string.Empty;
            try
            {
                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.Accept] = "application/json";
                    client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                    response = client.UploadString(URL, "");
                }              
                return response;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> getEcoSignOAUTHRefreshToken", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // Create Agreement Call to Eco sign
        internal string getEcoSignAgreement(string JSONForPosting, string accessToken)
        {
            string URL, Result = string.Empty;
            try
            {
                URL = BuildURLEcoSignCreateAgreement();
             
                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    client.Headers.Add("Access-Token", accessToken);
                    client.Encoding = Encoding.UTF8;
                    byte[] response = client.UploadData(URL, Encoding.UTF8.GetBytes(JSONForPosting));
                    Result = Encoding.UTF8.GetString(response);
                }
                return Result;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> getEcoSignAgreement", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // get eco sign agreement URL - once agreement is created successfully in the Ecosign
        internal string getEcoSignAgreementURL(string agreementID, string accesstoken)
        {
            string URL, Result = string.Empty;
            try
            {
                URL = BuildURLEcoSignAgreementURL(agreementID);
                               
                using (var client = new WebClient())
                {
                    client.Headers.Add("access-token", accesstoken);
                    Result = client.DownloadString(URL);
                }
                return Result;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> getEcoSignAgreementURL", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // Cancel agreement in Adobe eco sign
        internal string cancelAgreement(string agreementID, string accessToken, string JSONForPosting)
        {
            string URL, response = string.Empty;
            try
            {
                URL = BuildURLEcoSignCancelURL(agreementID);
                              
                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    client.Headers.Add("Access-Token", accessToken);                    
                    response = client.UploadString(URL,"PUT",JSONForPosting);                   
                }
                return response;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> getEcoSignAgreementURL", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        
        // get agreement status from Adobe eco sign
        internal string getAgreementStatus(string agreementID, string accessToken)
        {
            string URL, response = string.Empty;
            try
            {
                URL = BuildURLEcoSignGetStatusURL(agreementID);
                              
                using (var client = new WebClient())
                {
                    client.Headers[HttpRequestHeader.ContentType] = "application/json";
                    client.Headers.Add("Access-Token", accessToken);
                    response = client.DownloadString(URL);                  
                }
                return response;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> getAgreementStatus", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        #endregion

        #region "BuildURL and Generate a string from specific object for posting"

        // Generate URL using domain, login id , login key, parameters etc. for our internal APIS which are hosted in php servers
        internal string BuildURLDBAPIs(string API2Call, string myParams, string trackingid=null)
        {
            string loginId, loginKey = string.Empty;
            try
            {
                string[] TrackingIDForAlternateLoginCredentials = ConfigurationManager.AppSettings["TrackingIDForAlternateLoginCredentials"].ToString().Split(',');
                if (TrackingIDForAlternateLoginCredentials.Contains(trackingid))
                {
                    loginId = ConfigurationManager.AppSettings["Alternate_login_id"].ToString();
                    loginKey = ConfigurationManager.AppSettings["Alternate_login_key"].ToString();
                }        
                else
                {
                    loginId = ConfigurationManager.AppSettings["login_id"].ToString();
                    loginKey = ConfigurationManager.AppSettings["login_key"].ToString();
                }
                string Domain, URL = string.Empty;

                string ManualExecution = ConfigurationManager.AppSettings["ManualExecution"].ToString();
                if (ManualExecution.ToUpper() == "TRUE")
                {
                    Domain = ConfigurationManager.AppSettings["TestDomain"].ToString();
                }
                else
                {
                    Domain = ConfigurationManager.AppSettings["ProductionDomain"].ToString();
                }

                URL = string.Concat(Domain, API2Call, "?", "login_id=", loginId, "&login_key=", loginKey, myParams);
                return URL;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> BuildURLDBAPIs", "", ex.Message, DateTime.Now);
                throw;
            }
        }


        //Generate a string from specific object for posting
        private string getParamString(object myObj)
        {
            try
            {
                string paramString = string.Empty;
                foreach (var prop in myObj.GetType().GetProperties())
                {
                    paramString = string.Concat(paramString, "&", prop.Name, "=", prop.GetValue(myObj));
                }
                return paramString;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> getParamString", "", ex.Message, DateTime.Now);
                throw;
            }

        }

        #endregion

        #region "BuildURL for the Ecosign interactions"

        // Generate URL using clientid , secret key for oauth refresh
        internal string BuildURLEcoSignOauthTokenRefresh()
        {
            try
            {
                string ProdURLRefreshToken = ConfigurationManager.AppSettings["ProdURLRefreshToken"].ToString();
                string refresh_token = ConfigurationManager.AppSettings["refresh_token"].ToString();
                string client_id = ConfigurationManager.AppSettings["client_id"].ToString();
                string client_secret = ConfigurationManager.AppSettings["client_secret"].ToString();
                string grant_type = ConfigurationManager.AppSettings["grant_type"].ToString();
                string URL = string.Empty;

                URL = string.Concat(ProdURLRefreshToken, "?", "refresh_token=", refresh_token, "&client_id=", client_id, "&client_secret=", client_secret, "&grant_type=", grant_type);
                return URL;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> BuildURLEcoSignOauthTokenRefresh", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // Generate URL for Agreement generation
        internal string BuildURLEcoSignCreateAgreement()
        {
            try
            {
                string ProdURLCreateAgreement = ConfigurationManager.AppSettings["ProdURLCreateAgreement"].ToString();
                return ProdURLCreateAgreement;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> BuildURLEcoSignCreateAgreement", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // Generate URL for getting Agreement URL
        internal string BuildURLEcoSignAgreementURL(string agreementID)
        {
            try
            {
                string ProdURLCreateAgreement = ConfigurationManager.AppSettings["ProdURLCreateAgreement"].ToString();
                return string.Concat(ProdURLCreateAgreement, "/", agreementID, "/signingUrls?");
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> BuildURLEcoSignAgreementURL", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        //generate URL for cancel agreement call
        internal string BuildURLEcoSignCancelURL(string agreementID)
        {
            try
            {
                string ProdURLCreateAgreement = ConfigurationManager.AppSettings["ProdURLCancelAgreement"].ToString();
                return string.Concat(ProdURLCreateAgreement, "/", agreementID, "/status");
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> BuildURLEcoSignCancelURL", "", ex.Message, DateTime.Now);
                throw;
            }
        }


        //generate URL for getting agreement Status
        internal string BuildURLEcoSignGetStatusURL(string agreementID)
        {
            try
            {
                string ProdURLCreateAgreement = ConfigurationManager.AppSettings["ProdURLCancelAgreement"].ToString();
                return string.Concat(ProdURLCreateAgreement, "/", agreementID);
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> BuildURLEcoSignGetStatusURL", "", ex.Message, DateTime.Now);
                throw;
            }
        }



        #endregion

        #region "Deseriallize JSON Responses DirectBuy APIs"

        internal PromoCodeResponse deserializePromoResponse(string JsonText)
        {
            try
            {
                PromoCodeResponse myObj = JsonConvert.DeserializeObject<PromoCodeResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializePromoResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        internal PostDataResponse deserializePostDataResponse(string JsonText)
        {
            try
            {
                PostDataResponse myObj = JsonConvert.DeserializeObject<PostDataResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializePostDataResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        internal ContractInfoResponse deserializeContractInfoResponse(string JsonText)
        {
            try
            {
                ContractInfoResponse myObj = JsonConvert.DeserializeObject<ContractInfoResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializeContractInfoResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        internal FetchedLeadResponse deserializeGetFetchedLeadDataResponse(string JsonText)
        {
            try
            {
                FetchedLeadResponse myObj = JsonConvert.DeserializeObject<FetchedLeadResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializeGetFetchedLeadDataResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        internal ReferralResponse deserializeReferralResponse(string JsonText)
        {
            try
            {
                ReferralResponse myObj = JsonConvert.DeserializeObject<ReferralResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializeReferralResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        internal updateLeadResponse deserializeUpdateLeadResponse(string JsonText)
        {
            try
            {
                updateLeadResponse myObj = JsonConvert.DeserializeObject<updateLeadResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializeUpdateLeadResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        internal ClubInfoResponse deserializeClubInfoResponse(string JsonText)
        {
            try
            {
                ClubInfoResponse myObj = JsonConvert.DeserializeObject<ClubInfoResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializeClubInfoResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }

         #endregion

        #region "Deseriallize JSON Responses online Sales Services"

        // deserialize membership type serice response
        internal List<MembershipTypeResposne> deserializeMembershipTypeResposne(string JsonText)
        {
            try
            {
                List<MembershipTypeResposne> myObj = JsonConvert.DeserializeObject<List<MembershipTypeResposne>>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializeMembershipTypeResposne", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        #endregion

        #region "Deserialize JSON Responses from Eco Sign"

        // oath token refresh Success response from ecosign
        internal ecoSighTokenRefreshSuccessResponse deserializeTokenRefreshSuccessResponse(string JsonText)
        {
            try
            {
                ecoSighTokenRefreshSuccessResponse myObj = JsonConvert.DeserializeObject<ecoSighTokenRefreshSuccessResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializeTokenRefreshSuccessResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // oath token refresh Failure response from ecosign
        internal ecoSighTokenRefreshFailureResponse deserializeTokenRefreshFailureResponse(string JsonText)
        {
            try
            {
                ecoSighTokenRefreshFailureResponse myObj = JsonConvert.DeserializeObject<ecoSighTokenRefreshFailureResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializeTokenRefreshFailureResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // deserialize create agreement success response from ecosign
        internal ecoSignCreateAgreementSuccessResponse deserializeCreateAgreementSuccessResponse(string JsonText)
        {
            try
            {
                ecoSignCreateAgreementSuccessResponse myObj = JsonConvert.DeserializeObject<ecoSignCreateAgreementSuccessResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializeCreateAgreementSuccessResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // deserialize create agreement Failure response from ecosign
        internal ecoSignCreateAgreementFailureResponse deserializeCreateAgreementFailureResponse(string JsonText)
        {
            try
            {
                ecoSignCreateAgreementFailureResponse myObj = JsonConvert.DeserializeObject<ecoSignCreateAgreementFailureResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializeCreateAgreementFailureResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }


        // deserialize get agreement URL success response from ecosign
        internal ecoSignGETAgreemntURLSuccessResponse deserializeAgreementURLSuccessResponse(string JsonText)
        {
            try
            {
                ecoSignGETAgreemntURLSuccessResponse myObj = JsonConvert.DeserializeObject<ecoSignGETAgreemntURLSuccessResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializeAgreementURLSuccessResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }


        // deserialize get agreement URL failure response from ecosign
        internal ecoSignGETAgreemntURLFailureResponse deserializeAgreementURLFailureResponse(string JsonText)
        {
            try
            {
                ecoSignGETAgreemntURLFailureResponse myObj = JsonConvert.DeserializeObject<ecoSignGETAgreemntURLFailureResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializeAgreementURLFailureResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // deserialize cancel agreement success response from ecosign
        internal ecoSignCancelagreemntSuccessResponse deserializeCancelAgreementSuccessResponse(string JsonText)
        {
            try
            {
                ecoSignCancelagreemntSuccessResponse myObj = JsonConvert.DeserializeObject<ecoSignCancelagreemntSuccessResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializeCancelAgreementResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // deserialize cancel agreement failure response from ecosign
        internal ecoSignCancelagreemntFailureResponse deserializeCancelAgreementFailureResponse(string JsonText)
        {
            try
            {
                ecoSignCancelagreemntFailureResponse myObj = JsonConvert.DeserializeObject<ecoSignCancelagreemntFailureResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializeCancelAgreementResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // deserialize get agreement success response from ecosign
        internal ecoSignAgreementStatusSuccessResponse deserializeEcoSignAgreementStatusSuccessResponse(string JsonText)
        {
            try
            {
                ecoSignAgreementStatusSuccessResponse myObj = JsonConvert.DeserializeObject<ecoSignAgreementStatusSuccessResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializeEcoSignAgreementStatusResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // deserialize get agreement failure response from ecosign
        internal ecoSignAgreementStatusFailureResponse deserializeEcoSignAgreementStatusFailureResponse(string JsonText)
        {
            try
            {
                ecoSignAgreementStatusFailureResponse myObj = JsonConvert.DeserializeObject<ecoSignAgreementStatusFailureResponse>(JsonText);
                return myObj;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> deserializeEcoSignAgreementStatusFailureResponse", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        #endregion

        #region "Serialize object to JSON for eco sign"

        // Serialize object to JSON for agreement
        internal string serializeForAgreement(JSONForAgreement myObj)
        {
            try
            {
                string myString = JsonConvert.SerializeObject(myObj);
                return myString;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> serializeForAgreement", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // Serialize object to JSON for cancellation
        internal string serializeForCancelAgreement(JSONforCancellation myObj)
        {
            try
            {
                string myString = JsonConvert.SerializeObject(myObj);
                return myString;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> serializeForCancelAgreement", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        #endregion

        #region "database helpers get DataSet after execution of SP"

        // no params
        internal DataSet executeSPGetDS(string SPName)
        {
            try
            {
                DBConnection myConn = new DBConnection();
                DataSet myDS = new DataSet();
                SqlCommand myCmd = new SqlCommand();
                SqlDataAdapter myAdp = new SqlDataAdapter(myCmd);

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;
               
                myAdp.Fill(myDS);

                myAdp.Dispose();
                myConn = null;
                myCmd.Dispose();

                return myDS;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDS - No parameters", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // one param string
        internal DataSet executeSPGetDS(string SPName, string paramName1, string paramValue1)
        {
            try
            {
                DBConnection myConn = new DBConnection();
                DataSet myDS = new DataSet();
                SqlCommand myCmd = new SqlCommand();
                SqlDataAdapter myAdp = new SqlDataAdapter(myCmd);

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Text)).Value = paramValue1;

                myAdp.Fill(myDS);

                myAdp.Dispose();
                myConn = null;
                myCmd.Dispose();

                return myDS;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDS - one param string", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // one param integer
        internal DataSet executeSPGetDS(string SPName, string paramName1, int paramValue1)
        {
            try
            {
                DBConnection myConn = new DBConnection();
                DataSet myDS = new DataSet();
                SqlCommand myCmd = new SqlCommand();
                SqlDataAdapter myAdp = new SqlDataAdapter(myCmd);

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Int)).Value = paramValue1;

                myAdp.Fill(myDS);

                myAdp.Dispose();
                myConn = null;
                myCmd.Dispose();

                return myDS;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDS - one param integer", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // two params one string, second string
        internal DataSet executeSPGetDS(string SPName, string paramName1, string paramValue1, string paramName2, string paramValue2)
        {
            try
            {
                DBConnection myConn = new DBConnection();
                DataSet myDS = new DataSet();
                SqlCommand myCmd = new SqlCommand();
                SqlDataAdapter myAdp = new SqlDataAdapter(myCmd);

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Text)).Value = paramValue1;
                myCmd.Parameters.Add(new SqlParameter(paramName2, SqlDbType.Text)).Value = paramValue2;

                myAdp.Fill(myDS);

                myAdp.Dispose();
                myConn = null;
                myCmd.Dispose();

                return myDS;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDS - two params one string, second string", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // two params one string, second integer
        internal DataSet executeSPGetDS(string SPName, string paramName1, string paramValue1, string paramName2, int paramValue2)
        {
            try
            {
                DBConnection myConn = new DBConnection();
                DataSet myDS = new DataSet();
                SqlCommand myCmd = new SqlCommand();
                SqlDataAdapter myAdp = new SqlDataAdapter(myCmd);

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Text)).Value = paramValue1;
                myCmd.Parameters.Add(new SqlParameter(paramName2, SqlDbType.Int)).Value = paramValue2;

                myAdp.Fill(myDS);

                myAdp.Dispose();
                myConn = null;
                myCmd.Dispose();

                return myDS;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDS - two params one string, second integer", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // two params one integer second integer
        internal DataSet executeSPGetDS(string SPName, string paramName1, int paramValue1, string paramName2, int paramValue2)
        {
            try
            {
                DBConnection myConn = new DBConnection();
                DataSet myDS = new DataSet();
                SqlCommand myCmd = new SqlCommand();
                SqlDataAdapter myAdp = new SqlDataAdapter(myCmd);

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Int)).Value = paramValue1;
                myCmd.Parameters.Add(new SqlParameter(paramName2, SqlDbType.Int)).Value = paramValue2;

                myAdp.Fill(myDS);

                myAdp.Dispose();
                myConn = null;
                myCmd.Dispose();

                return myDS;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDS - two params one integer second integer", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // three params one string, second string, third String
        internal DataSet executeSPGetDS(string SPName, string paramName1, string paramValue1, string paramName2, string paramValue2, string paramName3, string paramValue3)
        {
            try
            {
                DBConnection myConn = new DBConnection();
                DataSet myDS = new DataSet();
                SqlCommand myCmd = new SqlCommand();
                SqlDataAdapter myAdp = new SqlDataAdapter(myCmd);

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Text)).Value = paramValue1;
                myCmd.Parameters.Add(new SqlParameter(paramName2, SqlDbType.Text)).Value = paramValue2;
                myCmd.Parameters.Add(new SqlParameter(paramName3, SqlDbType.Text)).Value = paramValue3;

                myAdp.Fill(myDS);

                myAdp.Dispose();
                myConn = null;
                myCmd.Dispose();

                return myDS;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDS - three params one string, second string, third String", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // three params one string, second string, third Integer
        internal DataSet executeSPGetDS(string SPName, string paramName1, string paramValue1, string paramName2, string paramValue2, string paramName3, int paramValue3)
        {
            try
            {
                DBConnection myConn = new DBConnection();
                DataSet myDS = new DataSet();
                SqlCommand myCmd = new SqlCommand();
                SqlDataAdapter myAdp = new SqlDataAdapter(myCmd);

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Text)).Value = paramValue1;
                myCmd.Parameters.Add(new SqlParameter(paramName2, SqlDbType.Text)).Value = paramValue2;
                myCmd.Parameters.Add(new SqlParameter(paramName3, SqlDbType.Int)).Value = paramValue3;

                myAdp.Fill(myDS);

                myAdp.Dispose();
                myConn = null;
                myCmd.Dispose();

                return myDS;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDS -  three params one string, second string, third Integer", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // three params one string, second Integer, third Integer
        internal DataSet executeSPGetDS(string SPName, string paramName1, string paramValue1, string paramName2, int paramValue2, string paramName3, int paramValue3)
        {
            try
            {
                DBConnection myConn = new DBConnection();
                DataSet myDS = new DataSet();
                SqlCommand myCmd = new SqlCommand();
                SqlDataAdapter myAdp = new SqlDataAdapter(myCmd);

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Text)).Value = paramValue1;
                myCmd.Parameters.Add(new SqlParameter(paramName2, SqlDbType.Int)).Value = paramValue2;
                myCmd.Parameters.Add(new SqlParameter(paramName3, SqlDbType.Int)).Value = paramValue3;

                myAdp.Fill(myDS);

                myAdp.Dispose();
                myConn = null;
                myCmd.Dispose();

                return myDS;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDS - three params one string, second Integer, third Integer", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // three params one Integer, second Integer, third Integer
        internal DataSet executeSPGetDS(string SPName, string paramName1, int paramValue1, string paramName2, int paramValue2, string paramName3, int paramValue3)
        {
            try
            {
                DBConnection myConn = new DBConnection();
                DataSet myDS = new DataSet();
                SqlCommand myCmd = new SqlCommand();
                SqlDataAdapter myAdp = new SqlDataAdapter(myCmd);

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Int)).Value = paramValue1;
                myCmd.Parameters.Add(new SqlParameter(paramName2, SqlDbType.Int)).Value = paramValue2;
                myCmd.Parameters.Add(new SqlParameter(paramName3, SqlDbType.Int)).Value = paramValue3;

                myAdp.Fill(myDS);

                myAdp.Dispose();
                myConn = null;
                myCmd.Dispose();

                return myDS;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDS - three params one Integer, second Integer, third Integer", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        #endregion

        #region "database helpers get sql datareader after execution of SP"

        //without parameter
        internal SqlDataReader executeSPGetDR(string SPName)
        {
            try
            {
                DBConnection myConn = new DBConnection();
                SqlCommand myCmd = new SqlCommand();
                SqlDataReader myRdr;

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;
                
                myCmd.Connection.Open();
                myRdr = myCmd.ExecuteReader();

                return myRdr;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDR - without param", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // one param string
        internal SqlDataReader executeSPGetDR(string SPName, string paramName1, string paramValue1)
        {
            try
            {
                DBConnection myConn = new DBConnection();               
                SqlCommand myCmd = new SqlCommand();
                SqlDataReader myRdr;

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Text)).Value = paramValue1;

                myCmd.Connection.Open();
                myRdr = myCmd.ExecuteReader();

                return myRdr;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDR - one param string", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // one param integer
        internal SqlDataReader executeSPGetDR(string SPName, string paramName1, int paramValue1)
        {
            try
            {
                DBConnection myConn = new DBConnection();               
                SqlCommand myCmd = new SqlCommand();
                SqlDataReader myRdr;

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Int)).Value = paramValue1;

                myCmd.Connection.Open();
                myRdr = myCmd.ExecuteReader();

                return myRdr;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDR - one param integer", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // two params one string, second String
        internal SqlDataReader executeSPGetDR(string SPName, string paramName1, string paramValue1, string paramName2, string paramValue2)
        {
            try
            {
                DBConnection myConn = new DBConnection();              
                SqlCommand myCmd = new SqlCommand();
                SqlDataReader myRdr;

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Text)).Value = paramValue1;
                myCmd.Parameters.Add(new SqlParameter(paramName2, SqlDbType.Text)).Value = paramValue2;

                myCmd.Connection.Open();
                myRdr = myCmd.ExecuteReader();

                return myRdr;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDR - two params one string, second String", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // two params one string, second integer
        internal SqlDataReader executeSPGetDR(string SPName, string paramName1, string paramValue1, string paramName2, int paramValue2)
        {
            try
            {
                DBConnection myConn = new DBConnection();               
                SqlCommand myCmd = new SqlCommand();
                SqlDataReader myRdr;

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Text)).Value = paramValue1;
                myCmd.Parameters.Add(new SqlParameter(paramName2, SqlDbType.Int)).Value = paramValue2;

                myCmd.Connection.Open();
                myRdr = myCmd.ExecuteReader();

                return myRdr;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDR - two params one string, second integer", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // two params one Integer, second integer
        internal SqlDataReader executeSPGetDR(string SPName, string paramName1, int paramValue1, string paramName2, int paramValue2)
        {
            try
            {
                DBConnection myConn = new DBConnection();             
                SqlCommand myCmd = new SqlCommand();
                SqlDataReader myRdr;

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Int)).Value = paramValue1;
                myCmd.Parameters.Add(new SqlParameter(paramName2, SqlDbType.Int)).Value = paramValue2;

                myCmd.Connection.Open();
                myRdr = myCmd.ExecuteReader();

                return myRdr;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDR - two params one Integer, second integer", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // three params one string, second string, third String
        internal SqlDataReader executeSPGetDR(string SPName, string paramName1, string paramValue1, string paramName2, string paramValue2, string paramName3, string paramValue3)
        {
            try
            {
                DBConnection myConn = new DBConnection();           
                SqlCommand myCmd = new SqlCommand();
                SqlDataReader myRdr;

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Text)).Value = paramValue1;
                myCmd.Parameters.Add(new SqlParameter(paramName2, SqlDbType.Text)).Value = paramValue2;
                myCmd.Parameters.Add(new SqlParameter(paramName3, SqlDbType.Text)).Value = paramValue3;

                myCmd.Connection.Open();
                myRdr = myCmd.ExecuteReader();

                return myRdr;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDR - three params one string, second string, third String", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // three params one string, second string, third Integer
        internal SqlDataReader executeSPGetDR(string SPName, string paramName1, string paramValue1, string paramName2, string paramValue2, string paramName3, int paramValue3)
        {
            try
            {
                DBConnection myConn = new DBConnection();             
                SqlCommand myCmd = new SqlCommand();
                SqlDataReader myRdr;

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Text)).Value = paramValue1;
                myCmd.Parameters.Add(new SqlParameter(paramName2, SqlDbType.Text)).Value = paramValue2;
                myCmd.Parameters.Add(new SqlParameter(paramName3, SqlDbType.Int)).Value = paramValue3;

                myCmd.Connection.Open();
                myRdr = myCmd.ExecuteReader();

                return myRdr;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDR - three params one string, second string, third Integer", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // three params one string, second Integer, third Integer
        internal SqlDataReader executeSPGetDR(string SPName, string paramName1, string paramValue1, string paramName2, int paramValue2, string paramName3, int paramValue3)
        {
            try
            {
                DBConnection myConn = new DBConnection();              
                SqlCommand myCmd = new SqlCommand();
                SqlDataReader myRdr;

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Text)).Value = paramValue1;
                myCmd.Parameters.Add(new SqlParameter(paramName2, SqlDbType.Int)).Value = paramValue2;
                myCmd.Parameters.Add(new SqlParameter(paramName3, SqlDbType.Int)).Value = paramValue3;

                myCmd.Connection.Open();
                myRdr = myCmd.ExecuteReader();

                return myRdr;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDR - three params one string, second Integer, third Integer", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        // three params one Integer, second Integer, third Integer
        internal SqlDataReader executeSPGetDR(string SPName, string paramName1, int paramValue1, string paramName2, int paramValue2, string paramName3, int paramValue3)
        {
            try
            {
                DBConnection myConn = new DBConnection();             
                SqlCommand myCmd = new SqlCommand();
                SqlDataReader myRdr;

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                myCmd.Parameters.Add(new SqlParameter(paramName1, SqlDbType.Int)).Value = paramValue1;
                myCmd.Parameters.Add(new SqlParameter(paramName2, SqlDbType.Int)).Value = paramValue2;
                myCmd.Parameters.Add(new SqlParameter(paramName3, SqlDbType.Int)).Value = paramValue3;

                myCmd.Connection.Open();
                myRdr = myCmd.ExecuteReader();

                return myRdr;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDR - three params one Integer, second Integer, third Integer", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        #endregion

        #region "database helpers execute sp with param collection SP"
        // one param string
        internal void executeSP(string SPName, SqlParameter[] myColl)
        {
            try
            {
                DBConnection myConn = new DBConnection();
                SqlCommand myCmd = new SqlCommand();       

                myCmd.Connection = myConn.getConnection();
                myCmd.CommandType = CommandType.StoredProcedure;
                myCmd.CommandText = SPName;

                int count = myColl.Length;

                if (count > 0)
                {
                    foreach (SqlParameter param in myColl)
                        myCmd.Parameters.Add(param);
                }
               
                myCmd.Connection.Open();
                myCmd.ExecuteScalar(); 
              
                
                
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> executeSPGetDR - one param string", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        #endregion

        #region "Generate ContractSessionKey"

        internal string getContractSessionKey()
        {
            try
            {
                String ContractSessionKey = Guid.NewGuid().ToString();
                return ContractSessionKey.Replace("-","").Replace("_","");
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> getContractSessionKey", "", ex.Message, DateTime.Now);
                throw;
            }

        }


        #endregion

        #region Generate JSON for Agreement eco sign calls Agreement and cancellation"

        // Generate JSON for Agreement
        internal string BuildJasonForAgreement(ContractInfoResponse objContractInfo, clsMemFinInfo objMemFinInfo, clsMember objMember, string inClubSales)
        {
            try
            {
                string jsonForAgreement = string.Empty;

                if (objMemFinInfo.Membership_type_id == "15" )
                {
                    objMemFinInfo.TguideText = "";
                }

                if (objMember.Country == "CA" || (objContractInfo.contract_information.state_active == "Y" && inClubSales == "N"))
                {
                    Helper myHelp = new Helper();

                    string section_c0a = string.Empty;
                    string section_c0b = string.Empty;
                    string section_c1 = string.Empty;
                    string section_c1b = string.Empty;
                    string section_c2 = string.Empty;
                    string section_c3 = string.Empty;
                    string section_e1 = string.Empty;
                    string section_e2 = string.Empty;
                    string section_notice = string.Empty;
                    string section_j = string.Empty;
                    string section_je = string.Empty;
                    string section_k = string.Empty;
                    string section_e1f14 = string.Empty;
                    string section_e2f14 = string.Empty;
                    string section_e1f10 = string.Empty;
                    string section_e2f10 = string.Empty;
                    string section_e1_bold = string.Empty;
                    string section_e2_bold = string.Empty;
                    string section_jf14 = string.Empty;
                    string section_j_bold = string.Empty;
                    string section_jef14 = string.Empty;
                    string statecancel36 = string.Empty;
                    string statecancel16 = string.Empty;
                    string statecancel8 = string.Empty;
                    string section_k14 = string.Empty;
                    string section_k10 = string.Empty;
                    string section_k_bold = string.Empty;
                    string contract_sent_on = string.Empty;
                    string contract_expires_on = string.Empty;

                    DateTime cdate = new DateTime(); cdate = DateTime.Now;
                    DateTime x3date = new DateTime();
                    DateTime x7date = new DateTime();
                    DateTime x30date = new DateTime();
                    

                    int bzday = (int)cdate.DayOfWeek;
                    int bz3day = 0;
                    int bz7day = 0;
                    if (bzday < 3)
                    {
                        bz3day = 3;
                        bz7day = 9;
                    }
                    else
                    {
                        if (bzday == 3)
                        {
                            bz3day = 5;
                            bz7day = 9;
                        }
                        else
                        {
                            if (bzday < 6)
                            {
                                bz3day = 5;
                                bz7day = 11;
                            }
                            else //if (bzday == 6)
                            {
                                bz3day = 4;
                                bz7day = 10;
                            }
                        }
                    }

                    x3date = cdate.AddDays(bz3day);
                    x7date = cdate.AddDays(bz7day);
                    x30date = cdate.AddDays(30);

                    string cdatestr = cdate.ToLongDateString();
                    string x3datestr = x3date.ToLongDateString();
                    string x7datestr = x7date.ToLongDateString();
                    string x30datestr = x30date.ToLongDateString();

                    string tmpstr = objContractInfo.contract_information.cancel_text;
                    string fmt_n = objContractInfo.contract_information.notice_format;
                    string fmt_e = objContractInfo.contract_information.cancel_format;
                    string fmt_j = objContractInfo.contract_information.section_j_format;
                    string fmt_je = objContractInfo.contract_information.section_jend_format;
                    string fmt_k = objContractInfo.contract_information.section_k_format;

                    section_e1 = myHelp.fixString(tmpstr, cdatestr, x3datestr, x7datestr, x30datestr);
                    section_notice = myHelp.fixString(objContractInfo.contract_information.cancel_notice, cdatestr, x3datestr, x7datestr, x30datestr);
                    section_j = myHelp.fixString(objContractInfo.contract_information.section_j, cdatestr, x3datestr, x7datestr, x30datestr);
                    section_je = myHelp.fixString(objContractInfo.contract_information.section_jend, cdatestr, x3datestr, x7datestr, x30datestr);
                    section_k = myHelp.fixString(objContractInfo.contract_information.section_k, cdatestr, x3datestr, x7datestr, x30datestr);

                    // Format Section E
                    int endxx = section_e1.Substring(0, 34).LastIndexOf(" ");
                    int endxl = section_e1.Substring(0, 52).LastIndexOf(" ");
                    int endxs = section_e1.Substring(0, 68).LastIndexOf(" ");

                    if (fmt_e == "F14")
                    {
                        section_e2 = section_e1.Substring(endxx + 1);
                        section_e1 = section_e1.Substring(0, endxx);
                        section_e1f14 = section_e1;
                        section_e2f14 = section_e2;
                        section_e1 = "";
                        section_e2 = "";
                    }
                    else if (fmt_e == "F10")
                    {
                        section_e2 = section_e1.Substring(endxl + 1);
                        section_e1 = section_e1.Substring(0, endxl);
                        section_e1f10 = section_e1;
                        section_e2f10 = section_e2;
                        section_e1 = "";
                        section_e2 = "";
                    }
                    else
                    {
                        section_e2 = section_e1.Substring(endxs + 1);
                        section_e1 = section_e1.Substring(0, endxs);

                        if (fmt_e.IndexOf("BOLD") > 0)
                        {
                            section_e1_bold = section_e1;
                            section_e2_bold = section_e2;
                        }
                    }

                    // Format Section J
                    if (fmt_j == "F14")
                    {
                        section_jf14 = section_j;
                        section_j = "";
                    }
                    else
                    {
                        if (fmt_j.IndexOf("BOLD") > 0)
                        {
                            section_j_bold = section_j;
                        }
                    }

                    // Format Section Je
                    if (fmt_je == "F14")
                    {
                        section_jef14 = section_je;
                    }

                    //Format Cancel Notice
                    if (fmt_n == "F20")
                    {
                        statecancel36 = section_notice;
                    }
                    else if (fmt_n == "F14")
                    {
                        statecancel16 = section_notice;
                    }
                    else
                    {
                        statecancel8 = section_notice;
                    }

                    //Format Section K
                    if (fmt_k == "F14")
                    {
                        section_k14 = section_k;
                        section_k = "";
                    }
                    else if (fmt_k == "F10")
                    {
                        section_k10 = section_k;
                        section_k = "";
                    }
                    else
                    {
                        if (fmt_k.IndexOf("BOLD") > 0)
                        {
                            section_k_bold = section_k;
                        }
                    }

                    // Sent and Expiration date
                    DateTime temp_sdt = new DateTime();
                    temp_sdt = DateTime.Now;
                    temp_sdt = temp_sdt.AddHours(-5);
                    DateTime sdt = new DateTime(temp_sdt.Year, temp_sdt.Month, temp_sdt.Day);
                    string sentdate = JsonConvert.SerializeObject(sdt);
                    string expireDate = JsonConvert.SerializeObject(sdt.AddYears(1));


                    JSONForAgreement myJSONObj = new JSONForAgreement();

                    myJSONObj.documentCreationInfo = new Documentcreationinfo();
                    myJSONObj.documentCreationInfo.name = "Your DirectBuy Membership Agreement";

                    myJSONObj.documentCreationInfo.recipients = new Recipient[1];

                    myJSONObj.documentCreationInfo.recipients[0] = new Recipient();
                    myJSONObj.documentCreationInfo.recipients[0].email = objMember.EMail;
                    myJSONObj.documentCreationInfo.recipients[0].role = "SIGNER";

                    myJSONObj.documentCreationInfo.mergeFieldInfo = new Mergefieldinfo[56];
                    // 1 - 5
                    myJSONObj.documentCreationInfo.mergeFieldInfo[0] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[0].fieldName = "Email";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[0].defaultValue = objMember.EMail;

                    if (inClubSales == "N")
                    {
                        myJSONObj.documentCreationInfo.mergeFieldInfo[1] = new Mergefieldinfo();
                        myJSONObj.documentCreationInfo.mergeFieldInfo[1].fieldName = "OrganizationName";
                        myJSONObj.documentCreationInfo.mergeFieldInfo[1].defaultValue = objContractInfo.contract_information.organization_name;
                    }
                    else
                    {
                        myJSONObj.documentCreationInfo.mergeFieldInfo[1] = new Mergefieldinfo();
                        myJSONObj.documentCreationInfo.mergeFieldInfo[1].fieldName = "OrganizationName";
                        myJSONObj.documentCreationInfo.mergeFieldInfo[1].defaultValue = "";
                    }

                    myJSONObj.documentCreationInfo.mergeFieldInfo[2] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[2].fieldName = "FullName";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[2].defaultValue = string.Concat(objMember.FirstName, " ", objMember.LastName);

                    myJSONObj.documentCreationInfo.mergeFieldInfo[3] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[3].fieldName = "AddressLine1";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[3].defaultValue = objMember.AddressLine1;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[4] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[4].fieldName = "AddressLine2";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[4].defaultValue = objMember.AddressLine2;

                    // 6-10
                    myJSONObj.documentCreationInfo.mergeFieldInfo[5] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[5].fieldName = "FirstName";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[5].defaultValue = objMember.FirstName;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[6] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[6].fieldName = "LastName";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[6].defaultValue = objMember.LastName;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[7] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[7].fieldName = "SentOn";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[7].defaultValue = sentdate;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[8] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[8].fieldName = "ExpiresOn";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[8].defaultValue = expireDate;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[9] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[9].fieldName = "Phone";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[9].defaultValue = objMember.Phone;

                    //11-15
                    myJSONObj.documentCreationInfo.mergeFieldInfo[10] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[10].fieldName = "mType";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[10].defaultValue = objMemFinInfo.Membership_type;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[11] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[11].fieldName = "mMFee";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[11].defaultValue = "$" + objMemFinInfo.Monthly_fee;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[12] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[12].fieldName = "mNFee";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[12].defaultValue = "$" + objMemFinInfo.Down_payment;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[13] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[13].fieldName = "mTFee";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[13].defaultValue = "$" + objMemFinInfo.Membership_cost;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[14] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[14].fieldName = "mRFee";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[14].defaultValue = "$" + objMemFinInfo.Monthly_fee;

                    if (inClubSales == "N")
                    {
                        if (objContractInfo.contract_information.use_alt_section_c == "X")
                        {
                            // 16-20 
                            myJSONObj.documentCreationInfo.mergeFieldInfo[15] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[15].fieldName = "sectionC0a";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[15].defaultValue = "submit the Payment Link and ends 12 months after that date. You agree to pay for your initial Membership according to the payment";

                            myJSONObj.documentCreationInfo.mergeFieldInfo[16] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[16].fieldName = "sectionC0b";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[16].defaultValue = "schedule in the Financial Summary below.";

                            myJSONObj.documentCreationInfo.mergeFieldInfo[17] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[17].fieldName = "sectionC1";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[17].defaultValue = "You may be given the opportunity to extend your Membership at the end of the initial Membership period according to the DirectBuy terms and policies then in effect.";

                            myJSONObj.documentCreationInfo.mergeFieldInfo[18] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[18].fieldName = "sectionC1B";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[18].defaultValue = " ";

                            myJSONObj.documentCreationInfo.mergeFieldInfo[19] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[19].fieldName = "sectionC2";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[19].defaultValue = "Should you decide to cancel, please mail or deliver a signed and dated";

                            //21-25
                            myJSONObj.documentCreationInfo.mergeFieldInfo[20] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[20].fieldName = "sectionC3";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[20].defaultValue = "written notice stating you would like to cancel to DirectBuy at the following address: 8450 Broadway, Merrillville, IN 46410.";
                        }
                        else // default
                        {
                            // 16-20 
                            myJSONObj.documentCreationInfo.mergeFieldInfo[15] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[15].fieldName = "sectionC0a";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[15].defaultValue = "submit the Payment Link as stated above and ends 12 months after that date. You agree to pay for your initial Membership according to the";

                            myJSONObj.documentCreationInfo.mergeFieldInfo[16] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[16].fieldName = "sectionC0b";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[16].defaultValue = "payment schedule in the Financial Summary below.";

                            myJSONObj.documentCreationInfo.mergeFieldInfo[17] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[17].fieldName = "sectionC1";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[17].defaultValue = "Your Membership will renew automatically on a month-to-month basis at the end of your initial Membership Period until you notify DirectBuy in writing that you do not wish to continue your Membership.";

                            myJSONObj.documentCreationInfo.mergeFieldInfo[18] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[18].fieldName = "sectionC1B";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[18].defaultValue = "Your Membership will renew automatically on a month-to-month basis at the end of your initial Membership Period until you notify DirectBuy in writing that you do not wish to continue your Membership.";

                            myJSONObj.documentCreationInfo.mergeFieldInfo[19] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[19].fieldName = "sectionC2";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[19].defaultValue = "Should you decide to cancel the month-to-month renewal after the initial";

                            //21-25
                            myJSONObj.documentCreationInfo.mergeFieldInfo[20] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[20].fieldName = "sectionC3";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[20].defaultValue = "Membership period, please mail or deliver a signed and dated written notice stating you would like to cancel to DirectBuy at the following address: 8450 Broadway, Merrillville, IN 46410 or by calling (877) 912-4067 or emailing MemberCares@directbuy.com.";

                        }
                    }
                    else
                    {
                        if (objContractInfo.contract_information.use_alt_section_c == "X")
                        {
                            // 16-20 
                            myJSONObj.documentCreationInfo.mergeFieldInfo[15] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[15].fieldName = "sectionC0a";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[15].defaultValue = "";

                            myJSONObj.documentCreationInfo.mergeFieldInfo[16] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[16].fieldName = "sectionC0b";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[16].defaultValue = "";

                            myJSONObj.documentCreationInfo.mergeFieldInfo[17] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[17].fieldName = "sectionC1";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[17].defaultValue = "You may be given the opportunity to extend your Membership at the end of the initial Membership period according to the DirectBuy terms and policies then in effect.";

                            myJSONObj.documentCreationInfo.mergeFieldInfo[18] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[18].fieldName = "sectionC1B";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[18].defaultValue = "";

                            myJSONObj.documentCreationInfo.mergeFieldInfo[19] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[19].fieldName = "sectionC2";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[19].defaultValue = " ";

                            //21-25
                            myJSONObj.documentCreationInfo.mergeFieldInfo[20] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[20].fieldName = "sectionC3";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[20].defaultValue = ".";
                        }
                        else // default
                        {
                            // 16-20 
                            myJSONObj.documentCreationInfo.mergeFieldInfo[15] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[15].fieldName = "sectionC0a";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[15].defaultValue = "";

                            myJSONObj.documentCreationInfo.mergeFieldInfo[16] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[16].fieldName = "sectionC0b";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[16].defaultValue = "";

                            myJSONObj.documentCreationInfo.mergeFieldInfo[17] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[17].fieldName = "sectionC1";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[17].defaultValue = "Your Membership will renew automatically on a month-to-month basis at the end of your initial Membership Period until you notify DirectBuy in writing that you do not wish to continue your Membership.";

                            myJSONObj.documentCreationInfo.mergeFieldInfo[18] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[18].fieldName = "sectionC1B";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[18].defaultValue = "";

                            myJSONObj.documentCreationInfo.mergeFieldInfo[19] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[19].fieldName = "sectionC2";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[19].defaultValue = "Your Membership will renew automatically on a month-to-month basis at the end of your initial Membership Period until you notify DirectBuy in writing that you do not wish to continue your Membership.";

                            //21-25
                            myJSONObj.documentCreationInfo.mergeFieldInfo[20] = new Mergefieldinfo();
                            myJSONObj.documentCreationInfo.mergeFieldInfo[20].fieldName = "sectionC3";
                            myJSONObj.documentCreationInfo.mergeFieldInfo[20].defaultValue = " or by calling (877) 912-4067 or emailing MemberCares@DirectBuy.com.";

                        }

                    }
                    myJSONObj.documentCreationInfo.mergeFieldInfo[21] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[21].fieldName = "sectionD";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[21].defaultValue = string.Concat(objMemFinInfo.Membership_type.ToUpper(), " FINANCIAL SUMMARY:");

                    myJSONObj.documentCreationInfo.mergeFieldInfo[22] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[22].fieldName = "sectionDB";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[22].defaultValue = string.Concat(objMemFinInfo.Membership_type.ToUpper(), " FINANCIAL SUMMARY:");

                    myJSONObj.documentCreationInfo.mergeFieldInfo[23] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[23].fieldName = "sectionE1F08";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[23].defaultValue = section_e1;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[24] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[24].fieldName = "sectionE1F08B";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[24].defaultValue = section_e1_bold;

                    //26-30
                    myJSONObj.documentCreationInfo.mergeFieldInfo[25] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[25].fieldName = "sectionE1F10";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[25].defaultValue = section_e1f10;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[26] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[26].fieldName = "sectionE1F10B";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[26].defaultValue = section_e1f10;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[27] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[27].fieldName = "sectionE1F14";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[27].defaultValue = section_e1f14;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[28] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[28].fieldName = "sectionE1F14B";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[28].defaultValue = section_e1f14;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[29] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[29].fieldName = "sectionE2F08";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[29].defaultValue = section_e2;

                    //31-35
                    myJSONObj.documentCreationInfo.mergeFieldInfo[30] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[30].fieldName = "sectionE2F08B";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[30].defaultValue = section_e2_bold;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[31] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[31].fieldName = "sectionE2F10";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[31].defaultValue = section_e2f10;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[32] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[32].fieldName = "sectionE2F10B";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[32].defaultValue = section_e2f10;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[33] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[33].fieldName = "sectionE2F14";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[33].defaultValue = section_e2f14;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[34] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[34].fieldName = "sectionE2F14B";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[34].defaultValue = section_e2f14;

                    //36-40
                    myJSONObj.documentCreationInfo.mergeFieldInfo[35] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[35].fieldName = "sectionJ";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[35].defaultValue = section_j;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[36] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[36].fieldName = "sectionJB";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[36].defaultValue = section_j_bold;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[37] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[37].fieldName = "sectionJ14";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[37].defaultValue = section_jf14;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[38] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[38].fieldName = "sectionJ14B";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[38].defaultValue = section_jf14;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[39] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[39].fieldName = "sectionJE";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[39].defaultValue = section_je;

                    //41-45
                    myJSONObj.documentCreationInfo.mergeFieldInfo[40] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[40].fieldName = "sectionJE14";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[40].defaultValue = section_jef14;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[41] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[41].fieldName = "sectionJE14B";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[41].defaultValue = section_jef14;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[42] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[42].fieldName = "tempstatename";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[42].defaultValue = objContractInfo.contract_information.state_name;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[43] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[43].fieldName = "statecancel8";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[43].defaultValue = statecancel8;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[44] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[44].fieldName = "statecancel8B";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[44].defaultValue = statecancel8;

                    //46-50
                    myJSONObj.documentCreationInfo.mergeFieldInfo[45] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[45].fieldName = "statecancel14";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[45].defaultValue = statecancel16;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[46] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[46].fieldName = "statecancel14B";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[46].defaultValue = statecancel16;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[47] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[47].fieldName = "statecancel20";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[47].defaultValue = statecancel36;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[48] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[48].fieldName = "statecancel20B";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[48].defaultValue = statecancel36;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[49] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[49].fieldName = "sectionK08";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[49].defaultValue = section_k;

                    //51-56
                    myJSONObj.documentCreationInfo.mergeFieldInfo[50] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[50].fieldName = "sectionK08B";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[50].defaultValue = section_k_bold;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[51] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[51].fieldName = "sectionK10";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[51].defaultValue = section_k10;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[52] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[52].fieldName = "sectionK10B";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[52].defaultValue = section_k10;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[53] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[53].fieldName = "sectionK14";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[53].defaultValue = section_k14;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[54] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[54].fieldName = "sectionK14B";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[54].defaultValue = section_k14;

                    myJSONObj.documentCreationInfo.mergeFieldInfo[55] = new Mergefieldinfo();
                    myJSONObj.documentCreationInfo.mergeFieldInfo[55].fieldName = "XtravelG";
                    myJSONObj.documentCreationInfo.mergeFieldInfo[55].defaultValue = objMemFinInfo.TguideText;


                    string ManualExecution = ConfigurationManager.AppSettings["ManualExecution"].ToString();
                    string EcoSignRedirectURL_Staging = ConfigurationManager.AppSettings["EcoSignRedirectURL_Staging"].ToString();
                    string EcoSignRedirectURL_PROD = ConfigurationManager.AppSettings["EcoSignRedirectURL_PROD"].ToString();
                    string[] TrackingID_AvoidOnlyPayment = ConfigurationManager.AppSettings["TrackingID_AvoidOnlyPayment"].ToString().Split(',');

                    myJSONObj.documentCreationInfo.postSignOptions = new Postsignoptions();
                    myJSONObj.documentCreationInfo.postSignOptions.redirectDelay = "0";
                    if (ManualExecution.ToUpper() == "TRUE")
                    {
                        if (TrackingID_AvoidOnlyPayment.Contains(objMember.TrackingID))
                        {
                            myJSONObj.documentCreationInfo.postSignOptions.redirectUrl = EcoSignRedirectURL_Staging + "?" + objMember.ContractSessionKey + "&ProcessId=1";
                        }
                        else
                        {
                           myJSONObj.documentCreationInfo.postSignOptions.redirectUrl = EcoSignRedirectURL_Staging + "?" + objMember.ContractSessionKey;
                        }
                       
                    }
                    else
                    {
                        if (TrackingID_AvoidOnlyPayment.Contains(objMember.TrackingID))
                        {
                            myJSONObj.documentCreationInfo.postSignOptions.redirectUrl = EcoSignRedirectURL_PROD + "?" + objMember.ContractSessionKey + "&ProcessId=1";
                        }
                        else
                        {
                            myJSONObj.documentCreationInfo.postSignOptions.redirectUrl = EcoSignRedirectURL_PROD + "?" + objMember.ContractSessionKey;
                        }
                    }

                    //log payment url in the database
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, objMember.TrackingID, "PaymentURL", "", myJSONObj.documentCreationInfo.postSignOptions.redirectUrl, "", DateTime.Now);

                    myJSONObj.documentCreationInfo.signatureType = "ESIGN";
                    myJSONObj.documentCreationInfo.signatureFlow = "SENDER_SIGNATURE_NOT_REQUIRED";

                    myJSONObj.documentCreationInfo.fileInfos = new Fileinfo[1];
                    myJSONObj.documentCreationInfo.fileInfos[0] = new Fileinfo();

                    //fetch eco sigh templates
                    SqlDataReader myRdr;
                    // if country is canada then State_Active param will be "N" for all canadian states as on 10/18/2016 so, 
                    // we need to avoid the state id passed by contract information and use the state id passed by user.
                    string stateAbbr = string.Empty;
                    if (objMember.Country.ToUpper() == "CA")
                    {
                        stateAbbr = objMember.State;
                    }
                    else
                    {
                        stateAbbr = objContractInfo.contract_information.state_abbreviation;
                    }

                    //check if template exists for the Tracking id , state and country combination if yes then fetch that tracking id , otherwise use default tracking id to fetch default template.
                    string RecordCount = string.Empty;
                    myRdr = myHelp.executeSPGetDR("getCheckIfTemplateExists", "State", stateAbbr, "Country", objMember.Country == "CA" ? "CAN" : objMember.Country, "TrackingID", objMember.TrackingID);
                    while (myRdr.Read())
                    {
                        RecordCount = myRdr["RecordCount"].ToString();
                    }
                    if (int.Parse(RecordCount) > 0)
                    {
                        myRdr = myHelp.executeSPGetDR("GetEcoSignTemplateByState", "State", stateAbbr, "Country", objMember.Country == "CA" ? "CAN" : objMember.Country, "TrackingID", objMember.TrackingID);
                    }
                    else
                    {
                        myRdr = myHelp.executeSPGetDR("GetEcoSignTemplateByState", "State", stateAbbr, "Country", objMember.Country == "CA" ? "CAN" : objMember.Country, "TrackingID", ConfigurationManager.AppSettings["defaultTrackingID"].ToString());
                    }
                    while (myRdr.Read())
                    {
                        myJSONObj.documentCreationInfo.fileInfos[0].libraryDocumentId = myRdr["esign_document_template"].ToString();
                        myJSONObj.documentCreationInfo.fileInfos[0].libraryDocumentName = myRdr["esign_document_template_name"].ToString();
                    }
                    myRdr = null;

                    myJSONObj.documentCreationInfo.message = "Thank you for joining DirectBuy. Attached, please find a copy of your DirectBuy Membership Agreement. If you have any questions about your membership, please contact our Member Care team at 1-855-871-7788.\r\n\r\nTogether, we can do amazing things.";

                    jsonForAgreement = myHelp.serializeForAgreement(myJSONObj);
                }
                else
                {
                    WriteLogToDB("Error", "", "State Selected is not an active State", "Helper class --> BuildJasonForAgreement", "", DateTime.Now);
                }
                return jsonForAgreement;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> BuildJasonForAgreement", "", ex.Message, ex.StackTrace, DateTime.Now);
                throw;
            }
        }

        internal string BuildJsonForCancelAgreement()
        {
            string cancelAgreementJSON = string.Empty;
            try
            {
                JSONforCancellation myObj = new JSONforCancellation();
                myObj.value = "CANCEL";

                cancelAgreementJSON = serializeForCancelAgreement(myObj);
                return cancelAgreementJSON;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> BuildJsonForCancelAgreement", "", ex.Message, DateTime.Now);
                throw;
            }
        }
        #endregion

        #region "Misc methods"

        //Fix String - Joson creation for creating agreement
        internal string fixString(string string2Fix, string cdatestr, string x3datestr, string x7datestr, string x30datestr)
        {
            try
            {
                StringBuilder sb = new StringBuilder(string2Fix);
                sb.Replace("jjwnewlinejjw", "\r\n");
                sb.Replace("JJWNEWLINEJJW", "\r\n");
                sb.Replace("u2019", "'");
                sb.Replace("U2019", "'");
                sb.Replace("jmcurdtjm", cdatestr);
                sb.Replace("JMCURDTJM", cdatestr);                
                sb.Replace("jmplus3jm", x3datestr);
                sb.Replace("JMPLUS3JM", x3datestr);
                sb.Replace("jmplus7jm", x7datestr);
                sb.Replace("JMPLUS7JM", x7datestr);
                sb.Replace("jmplus30jm", x30datestr);
                sb.Replace("JMPLUS30JM", x30datestr);
                return sb.ToString();
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> fixString", "", ex.Message, DateTime.Now);
                throw;
            }
        }        

        //Fix XML Remove all namespaces from xml
        public XElement RemoveAllNamespaces(XElement e)
        {
            try
            {
                return new XElement(e.Name.LocalName,
             (from n in e.Nodes()
              select ((n is XElement) ? RemoveAllNamespaces(n as XElement) : n)),
                 (e.HasAttributes) ?
                   (from a in e.Attributes()
                    where (!a.IsNamespaceDeclaration)
                    select new XAttribute(a.Name.LocalName, a.Value)) : null);
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> RemoveAllNamespaces", "", ex.Message, DateTime.Now);
                throw;
            }           
        }

        internal string checkIfTrackingIDExists(string trackingId)
        {
            try
            {
                Helper myhelp = new Helper();
                SqlDataReader myRdr;
                string TSCount = string.Empty;
                myRdr = myhelp.executeSPGetDR("getCheckIfTrackingIDExists", "TrackingID", trackingId);
                if (myRdr.HasRows)
                {
                    while (myRdr.Read())
                    {
                       TSCount= myRdr["TScount"].ToString();
                    }
                }
                return TSCount;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> checkIfTrackingIDExists", "", ex.Message, DateTime.Now);
                throw;
            }
        }

        //check the validitiy of JSON String
        internal bool IsValidJson(string strInput)
        {
            Helper myHelp = new Helper();
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {                    
                    var obj = JToken.Parse(strInput);
                    return true;
                }                
                catch (Exception ex) //some other exception
                {
                    myHelp.WriteLogToDB("Error", "Helper class --> IsValidJson", "", ex.Message, DateTime.Now);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        internal string getCurrentDateJSON()
        {
            string jsondate = string.Empty;
            try
            {
                var date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTimeKind.Local);
                var obj =  new DateTimeOffset(date) ;

                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.Converters.Add(new IsoDateTimeConverter
                {
                    DateTimeFormat = "yyyy-MM-ddTHH:mm:ssZ",
                    DateTimeStyles = DateTimeStyles.AdjustToUniversal
                });
                jsondate= JsonConvert.SerializeObject(obj, settings);
                
                return jsondate.Replace("\"","");               
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> convertDate2JSON", "", ex.Message, DateTime.Now);
                throw;
            }        
        }

        internal string getContractEndDateJSON()
        {
            string jsondate = string.Empty;
            try
            {
                var date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTimeKind.Local);
                date = date.AddYears(1);            
                var obj = new DateTimeOffset(date);

                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.Converters.Add(new IsoDateTimeConverter
                {
                    DateTimeFormat = "yyyy-MM-ddTHH:mm:ssZ",
                    DateTimeStyles = DateTimeStyles.AdjustToUniversal
                });
                jsondate = JsonConvert.SerializeObject(obj, settings);
                return jsondate.Replace("\"", "");
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> convertDate2JSON", "", ex.Message, DateTime.Now);
                throw;
            }
        }     
    

        #endregion

        #region "generate XML for posting to TOPS webService and make call"
        
        // calling tops service to create a new member in TOPS
        internal string[] CreateMemberInTops(GetMemberID clsGetMemberID)
        {
            try
            {
                string ManualExecution = ConfigurationManager.AppSettings["ManualExecution"].ToString();
                string TOPSWebService = string.Empty;
                if (ManualExecution.ToUpper() == "TRUE")
                {
                    TOPSWebService = ConfigurationManager.AppSettings["TOPSWebService_Stage"].ToString();
                }
                else
                {
                    TOPSWebService = ConfigurationManager.AppSettings["TOPSWebService_Production"].ToString();
                }           

                string xmlRequest = string.Empty;
                string[] returnVars = new string[2];                

                var stringwriter = new System.IO.StringWriter();
                var serializer = new XmlSerializer(clsGetMemberID.GetType());
                serializer.Serialize(stringwriter, clsGetMemberID);
                xmlRequest = stringwriter.ToString();

                XElement root = XElement.Parse(xmlRequest);
                xmlRequest = root.ToString();

                StringBuilder oRequest = new StringBuilder();
                oRequest.Append("<?xml version=\"1.0\" encoding=\"utf-16\"?>");
                oRequest.Append("<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">");
                oRequest.Append("<soap:Body>");
                oRequest.Append(xmlRequest);
                oRequest.Append("</soap:Body>");
                oRequest.Append("</soap:Envelope>");

                //Builds the connection to the WebService.
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(TOPSWebService);
                req.ContentType = "text/xml; charset=\"utf-8\"";
                req.Accept = "text/xml";
                req.Method = "POST";

                //Passes the SoapRequest String to the WebService
                using (Stream stm = req.GetRequestStream())
                {
                    using (StreamWriter stmw = new StreamWriter(stm))
                    {
                        stmw.Write(oRequest);
                    }
                }
                //Gets the response
                WebResponse response = req.GetResponse();
                //Writes the Response
                Stream responseStream = response.GetResponseStream();
                string responseString = string.Empty;

                XmlDocument doc = new XmlDocument();

                responseStream = response.GetResponseStream();
                responseString = new StreamReader(responseStream).ReadToEnd();

                response.Close();

                //count successful responses                    
                doc.LoadXml(responseString);
                XmlNodeList nodeList = doc.GetElementsByTagName("MemberID");
                foreach (XmlNode xn in nodeList)
                {
                    //get member id
                    returnVars[0] = xn.InnerText;
                }
                returnVars[1] = xmlRequest;
                return returnVars;
            }
            catch (Exception ex)
            {
                WriteLogToDB("Error", "Helper class --> CreateMemberInTops", "", ex.Message, ex.StackTrace, DateTime.Now);
                throw;
            }
        }
   
        #endregion

      

    }
}
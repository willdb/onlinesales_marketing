﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineSales
{
    public class ClsPromoParams
    {
        public string promo_code { get; set; }
        public string amount_to_discount { get; set; }
        public string down_payment { get; set; }
        public string membership_cost { get; set; }
    }
}
# README #

Online Sales - Marketing is the code base for the membership.directbuy.com interface to buying a membership.  

The website uses Echosign to handle contract generation and signing, and then payment is handled by a web page in the members website exposed as non-authenticated helper resource.
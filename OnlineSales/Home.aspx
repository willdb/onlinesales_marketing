﻿<%@ Page Language="C#"   EnableEventValidation="false"  MaintainScrollPositionOnPostback ="true" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="OnlineSales.Home" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!doctype html>
<!--[if IE 8]><html lang="en" class="ie8"><![endif]-->
<!--[if IE 9]><html lang="en" class="ie9"><![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
<meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">
<meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
<title>Membership | DirectBuy, Inc.</title>
<!-- Load Fonts -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,800italic,400,700,600,300" rel='stylesheet' type='text/css'>
<%--<link rel="stylesheet" type="text/css" href="//cloud.typography.com/7553472/621626/css/fonts.css" />--%>
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.css">
<!-- Custom styles for this project -->
<link rel="stylesheet" href="assets/css/main.css">
<link href="assets/css/colorbox.css" rel="stylesheet" />
<!-- Adding Modernizr for support detection -->
<script src="assets/js/modernizr-2.8.3.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="assets/Scripts/jquery.validate.min.js"></script>
<script src="assets/Scripts/colorbox/jquery.colorbox.js"></script>
    <script>
        history.pushState(null, null, document.URL); 
        window.addEventListener('popstate', function () 
        { 
            //for(var i=1;i<=8;i++)
            //{
            //    history.pushState(null, null, document.URL); 
            //}
        });
        
    </script>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NPC6K3');</script>
</head>
<script type="text/javascript">
      
    $(document).ready(function () {
        $.validator.addMethod("emailmatch",function(value,element) {               
            return this.optional(element) || /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9\-]+\.[a-zA-Z]{2,18}(\.[a-zA-Z]{2,10}){0,1}$/i.test(value);
                
        },"Please enter a valid email address.");
        $.validator.addMethod("namematch",function(value,element){             
            return this.optional(element) || /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/.test(value.trim());
        },"Your name can contain only letters");

        $.validator.addMethod("phonenumber",function(value,element){
            return this.optional(element) || /^([01][- .])?(\(\d{3}\)|\d{3})[- .]?\d{3}[- .]\d{4}$/.test(value);
        },"Please enter a valid phone number.");
        $.validator.addMethod("zipmatch",function(value,element){
            return this.optional(element) || /(^\d{5}(-\d{4})?$)|(^[aAbBcCeEgGhHjJkKlLmMnNpPrRsStTvVxXyY]{1}\d{1}[a-zA-Z]{1} *\d{1}[a-zA-Z]{1}\d{1}$)/.test(value);
        },"Please enter a valid zip/postal code");
            
        $.validator.addMethod("addressmatch",function(value,element){
            return this.optional(element) || /^[0-9a-zA-Z. -\'\#]{4,}$/.test(value);
        },"Please enter valid address");

        $.validator.addMethod("statematch", function(value, element){
            return this.optional(element) || value !=-1;
        }, "Please choose a State");

        $.validator.addMethod("countrymatch", function(value, element){
            return this.optional(element) || value !=-1;
        }, "Please choose a Country");
              

        $("#form1").validate({
            rules: {

                <%=fname.UniqueID%> : {
                    required:true,
                    rangelength:[2,25],
                    namematch:true
                },
                <%=lname.UniqueID%> : {
                    required:true,
                    rangelength:[2,25],
                    namematch:true
                },

                <%=email1.UniqueID%> : {
                    emailmatch:true,
                    required:true,

                },
                <%=phone1.UniqueID%> : {
                    required:true,
                    phonenumber:true 
                },
                <%=city1.UniqueID%> : {
                    required:true,
                    namematch:true
                },
                <%=ddlState.UniqueID%> : {
                    required:true,
                    statematch: true                      
                },
                <%=ddlCountry.UniqueID%> : {

                    required:true,
                    countrymatch:true
                                              
                },
                <%=zip.UniqueID%> : {
                    required:true,
                    zipmatch:true
                },
                <%=address1.UniqueID%> : {
                    required : true,
                    addressmatch : true
                },
                <%=spousefname.UniqueID%> : {
                    required:function(element) {
                        if ($("#chkIsCoApplicant").is(':checked')) {           
                            return true;
                        }
                        else {
                            return false;
                        }
                    },
                    rangelength:[2,25],
                    namematch:true
                },  
                <%=spouselname.UniqueID%> : {
                    required:function(element) {
                        if ($("#chkIsCoApplicant").is(':checked')) {           
                            return true;
                        }
                        else {
                            return false;
                        }
                    },
                    rangelength:[2,25],
                    namematch:true
                }, 
                <%=spouseemail.UniqueID%> : {
                    emailmatch:true,
                    required:function(element) {
                        if ($("#chkIsCoApplicant").is(':checked')) {           
                            return true;
                        }
                        else {
                            return false;
                        }
                    },
                },
                membership: {
                    required:true
                }

            },
            messages: {
                <%=fname.UniqueID%> : {
                    required : "First Name is required.",
                    rangelength:"First Name must be between {0} and {1} characters."
                },
                <%=email1.UniqueID%> : {
                    required:"Email is required."
                },
                <%=phone1.UniqueID%> : {
                    required: "Phone Number is required."
                },
                <%=lname.UniqueID%> : {
                    required: "Last Name is Required.",
                    rangelength:"Last Name must be between {0} and {1} characters."
                },
                <%=city1.UniqueID%> : {
                    required: "City is required."
                },
                <%=zip.UniqueID%> : {
                    required: "Zip/Postal code is required."
                },
                <%=address1.UniqueID%> : {
                    required: "Address is required."
                },
                membership:{
                    required : "Membership type is required."
                }
            },
            errorPlacement : function(error,element)
            {                           
                if ( element.is(":radio") ) 
                {  
                    error.appendTo( element.parents('#membertype') );                           
                }
                else 
                { // This is the default behavior 
                    error.insertAfter( element );
                }                                        
            },              
            submitHandler: function(form) {
                // do other things for a valid form
                $.colorbox({href:"assets/images/ajax-loading-new.gif",transition:"elastic",opacity:0.65,fixed:true})                       
                $('#chkIsAgreeTerms').prop("checked", false);
                $('#chkTCPAagreement').prop("checked", false);
                form.submit();
            }, 
      
        });
    });

    
</script>  
         
    <script type="text/javascript">  
        function openPhoneAgree() {           
            $.colorbox({html:"<div style='padding:50px; background:#ccc;'><b>By checking this box, you agree to receive phone calls from DirectBuy and its franchisees to discuss DirectBuy membership options, promotions or special events. We may use automated technology to make these calls. You do not need to agree to these calls in order to purchase products or memberships from DirectBuy</b></Div>",transition:"elastic",width:"40%", height:"20%",opacity:0.65});
        }    

        function openColorBox(p) {
            $.colorbox({ href: p,iframe:true, transition:"elastic",width:"85%", height:"90%",opacity:0.65,overlayClose:false,fastIframe:false,escKey:false,speed:350,fixed:true,closeButton:true,close:"close"});
        }

        function openPDF() {
            var country = $('#hfCountry').val();           
            if (country.substr(0, 2) == "US") {
                window.location.href  = '//www.directbuy.com/DirectBuy-Membership-Guide.aspx';         
            }
            else {
                window.location.href  = '//www.directbuy.com/DirectBuy_Membership_Guide_Canada.aspx';    
            }      
        }  

        function openPolicy(){
            window.open('//www.directbuy.com/company/privacy-policy?utm_campaign=&_ga=1.88581595.190670347.1478895457', target="_blank"); 
        }

    </script> 


<body>  
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NPC6K3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<div id="mobile-wrap-1" class="container">
<header role="banner" class="masthead" id="masthead">
    <div class="top">
        <div class="container-fluid">
            <h1 class="logo"> <a title="DirectBuy, Inc." href="#" class="icon-logo"><span class="sr-only">DirectBuy, Inc.</span></a> </h1>
            <div class="messages">
                <h5>Questions? Contact us 1.888.871.7781</h5>
            </div>
        </div>
    </div>
</header>
<div class="fullwidth-title-bar row">
    <h1 class="centering">Create Your Membership RIGHT NOW</h1>
</div>
<section class="row fullwidth-title">
    <div class="padding-tb-30">
        <h5>Being a DirectBuy member means access to unbelievable savings and exclusive offers on the products you love. And that means creating the lifestyle you’ve always dreamed of.</h5>
    </div>
</section>
<form id="form1" runat="server" autocomplete="on">


        <div class="container">
            <section class="col-lg-3 col-md-3 col-sm-2"></section>
            <section class="col-lg-6 col-md-6 col-sm-8">
                <div id="membertype">
                <h3>Choose your membership</h3>
                        <div id="divSig" class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 row" style="margin-bottom: -14px;">
                        <div  class="radio">
                            <asp:RadioButton ID="cbxSig" runat="server" Text="Signature (One time $699 + 39.95/month)" GroupName="membership" Value="signature" />
                        </div>
                    </div>
                        <div id="divPrem" class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
                        <div  class="radio">
                            <asp:RadioButton ID="cbxPrem" runat="server" Text="Premier (One time $999 + 49.95/month)" GroupName="membership" Value="premier" />
                            </div>
                        </div>
                        <div id="divDiscover" class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
                            <div class="radio">
                                <asp:RadioButton ID="cbxDiscover" runat="server" Text="Premier (One time $0 + 39.95/month)" GroupName="membership" Value="Discover" />
                        </div>
                    </div>                
                         <div id="divEntSignature" class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
                            <div class="radio">
                                <asp:RadioButton ID="cbxEntSignature" runat="server" Text="Premier (One time $0 + 29.95/month)" GroupName="membership" Value="EnterpriseSignature" />
                            </div>
                        </div>
                         <div id="divNationalEntTrial" class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
                            <div class="radio">
                                <asp:RadioButton ID="cbxNationalEntTrial" runat="server" Text="Premier (One time $0 + 0/month)" GroupName="membership" Value="NationalEnterpriseTrial" />
                            </div>
                        </div>
                        <div id="divDiscover180" class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
                            <div class="radio">
                                <asp:RadioButton ID="cbxDiscover180" runat="server" Text="Discover 180 (One time $0 + 0/month)" GroupName="membership" Value="Discover180" />
                            </div>
                        </div>
                        <div id="divNEntSigCan" class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
                            <div class="radio">
                                <asp:RadioButton ID="cbxNationalEntSigCAN" runat="server" Text="Nation Enterprise signature Canada (One time $0 + 0/month)" GroupName="membership" Value="NationalEntSigCAN" />
                            </div>
                        </div>
                </div>
            
                <div>
                    <h3 class="">Member Information</h3>
                </div>
              <%--  <div class="checkbox">
                    <input type="checkbox" id="one" value="">
                    <label for="one">THIS MEMBERSHIP IS A GIFT</label>
                </div>--%>
               
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="field1">FIRST NAME</label>
                            <asp:TextBox ID="fname" runat="server" class="form-control" placeholder="" MaxLength="25"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="field1">LAST NAME</label>
                            <asp:TextBox ID="lname" runat="server" class="form-control" placeholder="" MaxLength="25"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="field1">EMAIL</label>
                            <asp:TextBox ID="email1" runat="server" class="form-control" placeholder="" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="field1">PHONE NUMBER</label>
                            <asp:TextBox ID="phone1" runat="server" class="form-control" placeholder="" MaxLength="14"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="field1">ADDRESS</label>
                            <asp:TextBox ID="address1" runat="server" class="form-control" placeholder="" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="field1">APT / SUITE(OPTIONAL)</label>
                            <asp:TextBox ID="address2" runat="server" class="form-control" placeholder="" MaxLength="15"></asp:TextBox>
                        </div>
                    </div>
                </div>
              

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="title">COUNTRY</label> 
                            
                            <asp:DropDownList ID="ddlCountry" runat="server" class="form-control"  AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" >
                                <asp:ListItem Text="Select Country" Value="-1" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="USA"   Value="US" ></asp:ListItem>
                                <asp:ListItem Text="CANADA" Value="CA"></asp:ListItem>
                            </asp:DropDownList>
                 
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="title">STATE</label>   
                                                   <asp:DropDownList ID="ddlState" runat="server" class="form-control" >                              
                            </asp:DropDownList>  
                                            </div>
                    </div>                    
                </div>
           
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="field1">CITY</label>
                            <asp:TextBox ID="city1" runat="server" class="form-control" placeholder="" MaxLength="20"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="field1">ZIP/POSTAL CODE</label>
                            <asp:TextBox ID="zip" runat="server" class="form-control" placeholder="" MaxLength="7"></asp:TextBox>
                        </div>
                    </div>
                </div>
             
                <div class="row">
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                        <div class="form-group">
                            <div class="checkbox">
                                <input type="checkbox" id="chkIsCoApplicant" value="">
                                <label for="one"><b>ADD A CO-APPLICANT</b></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="coApplicant">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="field1">CO-APPLICANT FIRST NAME</label>
                            <asp:TextBox ID="spousefname" runat="server" class="form-control" placeholder="" MaxLength="25"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="field1">CO-APPLICANT LAST NAME</label>
                            <asp:TextBox ID="spouselname" runat="server" class="form-control" placeholder="" MaxLength="25"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="row" id="coApplicant1">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="field1">CO-APPLICANT EMAIL</label>
                            <asp:TextBox ID="spouseemail" runat="server" class="form-control" placeholder="" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </section>
        </div>
   
    <div id ="summary" class="row summary-bg">
        <section class="col-lg-3 col-md-3 col-sm-2"></section>
        <div class="col-lg-6 col-md-6 col-sm-8">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                       <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                        <div class="form-group">
                                <div class="checkbox">
                                    <input type="checkbox" id="DivPromoCode" value="">
                                    <label for="one"><b>Have a Promo Code?</b></label>
                                </div>
                        </div>
                        </div>

						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						    <div class="form-group">
                                    <asp:TextBox ID="PromoID" runat="server" class="form-control" placeholder="" MaxLength="20"></asp:TextBox>
							</div>
						</div>
							<div class="col-md-3 col-sm-3 col-xs-3 block">
                                <input type="button" id="ApplyButton" class="btn btn-primary" style="height: 45px" value="Apply" />
                              <%--  <input class="btn btn-primary" type ="submit" value="Apply" />--%>
							</div>
						<%--</div>--%>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                <b class="text-success">
                                    <asp:Label ID="invalid" runat="server"></asp:Label></b>
                            </div>
                    </div>
                    <h3>Summary </h3>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
                        <table class="row1">
                            <tr>
                                <td style="padding-bottom: 5px">
                                    <asp:Label ID="signupLabel" runat="server">Sign Up Fees:</asp:Label>
                                    <%-- <label for="field1">Sign Up Fees:</label></td>--%>
                                <td class="text-right">
                                    <asp:Label runat="server" ID="signupfee"></asp:Label></td>
                            </tr>
                            <tr>
                                <td class="text-right" style="padding-bottom: 5px"><b class="text-danger">
                                    <asp:Label ID="savings" runat="server">You Saved :</asp:Label></b></td>
                                <td class="text-right">
                                    <asp:Label runat="server" ID="promoapplied"></asp:Label></td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="field1">Monthly Renewal Payments:</label></td>
                                <td class="text-right">
                                    <asp:Label ID="monthly" runat="server"></asp:Label></td>
                            </tr>
                            <%--<tr>
                                <td><label for="field1">Tax:</label></td>
                                <td class="text-right"><asp:label id="tax" runat="server">$10.45</asp:label></td>
                            </tr>--%>
                            <tr>
                                <td>
                                    <h3>Due Today(Excluding Tax)</h3>
                                </td>
                                <td class="text-right">
                                    <h4>
                                        <asp:Label ID="total" runat="server"></asp:Label></h4>
                                </td>
                            </tr>
                        </table>
                    </div>
               <%-- </div>--%>
        </div>
    </div>

    <asp:HiddenField ID="actualmembershipfee" runat="server" Value="" />
    <asp:HiddenField ID="promo" runat="server" Value="" />
    <asp:HiddenField ID="renewal" runat="server" Value="" />
    <asp:HiddenField ID="downpayment" runat="server" Value="" />   
    <asp:HiddenField ID="hfMemberTypeJson" runat="server" Value="" />                 
    <asp:HiddenField  id="hfCurr" runat="server" value="" />
    <asp:HiddenField  id="hfCountry" runat="server" Value=""/>
  

    <div class="row">
        <section class="col-lg-3 col-md-3 col-sm-2"></section>
        <div class="col-lg-6 col-md-6 col-sm-8">
            <%--<h3 class="">Membership Agreement</h3>
            <h5>Please read the policies, terms and conditions before using the website.</h5>--%>
            
            
            <div class="checkbox">
                <input type="checkbox" id="chkIsAgreeTerms" value="" runat="server">
             <!--   <label for="one">I agree to the Membership <a  id="aPolicies" onclick="openPDF()" class="underline">Policies, Terms & Conditions</a></label>-->
                        <label for="one" style="display: inline">By checking this box, you agree to receive emails, phonecalls and/or texts from DirectBuy to discuss membership options. We may use automated technology to make these calls/texts. You do not need to agree to these calls/texts in order to purchase products or memberships from DirectBuy.  <a id="aPolicies" onclick="openPolicy()" class="underline">Privacy Policy</a></label>
            </div>
                    <div class="checkbox" style="display:none">
                <input type="checkbox" id="chkTCPAagreement" value="">
                <!-- <label for="one" >I agree to receive phone calls from DirectBuy, Inc.&nbsp;<a id="aTCPA" onclick="openPhoneAgree()"  class="phoneagree">Click here to read full terms.</a></label> -->
                        <label for="one" style="display: inline">By checking this box, you agree to pay your deposit, which we will hold until you have reviewed and e-signed the Membership Agreement that has been emailed to you. We must receive your electronic signature within 14 days.</label>
            </div>
            <div class="padding-tb-30"> 
                <asp:Button ID="ContractBtn" class="btn btn-primary col-lg-12 col-md-12 col-sm-12 col-xs-12" Text="Submit"  runat="server" OnClick="ContractBtn_Click"  ></asp:Button>
            </div>
        </div>
    </div>

           
 </form>   
<%--</section>--%>
        <p>&nbsp;</p>
           <p>&nbsp;</p>
           <p>&nbsp;</p>
           <p>&nbsp;</p>
<div class="clearfix"></div>
<!-- SECTION FOOTER -->
        <footer id="footer" class="container-wide" style="display:none">



            <footer class="Footer">
                <ul class="menu">
                    <li class="find">
                        <p class="desc">Find a DirectBuy                             <br />                             Club Near You</p>
                        <p class="zip">
                            <input type="text" class="textbox" placeholder="Postal Code">
                            <a class="submit ir" href="//www.directbuy.com/Appointment-Center/">Go</a>

          
                        </p>
                        <li>
          
             
                        <p class="desc">Become a Member <br /> of the DirectBuy Team</p>
                            <a class="LinkButton" href="//www.directbuy.com/company/careers/" target="_self">Career</a>
                    <li>
                        <p class="desc">Contact Us</p>
                        <a class="LinkButton" href="tel:+1-844-398-2713%E2%80%8B" target="_self">+1-844-398-2713</a>
                    <li>
                        <p class="desc">You Have Questions.<br />  We Have Answers.</p>
                        <a class="LinkButton" href="//www.directbuy.com/faq-page/" target="_self">FAQs</a>
                </ul>
              
                <ul class="block social col-sm-12">
                    <li><a class="icon-facebook" href="//www.facebook.com/directbuy"><span>Facebook</span></a></li>
                    <li><a class="icon-twitter" href="//www.twitter.com/directbuy"><span>Twitter</span></a></li>
                    <li><a class="icon-youtube" href="//www.youtube.com/user/directbuy"><span>YouTube</span></a></li>
                    <li><a class="icon-yelp" href="//www.yelp.com"><span>Yelp</span></a></li>
                    <li><a class="icon-pinterest" href="//www.pinterest.com/directbuy"><span>Pinterest</span></a></li>
                    <li><a class="icon-gplus" href="https://plus.google.com/+DirectBuy"><span>Google+</span></a></li>
                    
                </ul>
       
                <ul class="final">

                    <li class="link"><a href="//www.directbuy.com/locations/" title="">OUR LOCATIONS</a> </li>
                    <li class="link"><a href="//www.directbuy.com/Sitemap/" title="">SITE MAP</a> </li>
                    <li class="link"><a href="//subscribe.directbuy.com/?_ga=1.108818251.854953285.1479742220" title="">EMAIL MANAGEMENT</a> </li>
                    <li class="link"><a href="//www.directbuy.com/company/privacy-policy" title="">PRIVACY POLICY</a> </li>
                    <li class="link"><a href="//www.directbuy.com/company/policies-and-guidelines/effective-communication-policy/" title="">EFFECTIVE COMMUNICATION POLICY</a> </li>
                    <li class="link">© 2016 </li>
        </ul>
            </footer>

</footer>
</div>
<!-- Include jQuery --> 
<script src="assets/js/jquery.min.js"></script> 

<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="assets/js/plugins.js"></script> 

<!-- Includes site-specific Javascript --> 
<script src="assets/js/main.js"></script>

<script src="assets/js/MembershipJS.js"></script>
    
</body>
</html>
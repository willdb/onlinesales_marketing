﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
namespace OnlineSales
{
    public partial class frmAddTrackingID : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getMembershipData();
            }
        }

        protected void GvMembershipTypes_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                pnlAdd.Visible = true;
                pnlInsert.Visible = false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            try
            {
                pnlInsert.Visible = true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void getMembershipData(string trackingid="")
        {
            try
            {
                GVMembershipTypes.DataSource = getMemberShipDataDS(trackingid);
                GVMembershipTypes.DataBind();

                GVMembershipTypes.BottomPagerRow.Visible = true;
                pnlInsert.Visible = false;
                pnlAdd.Visible = true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private DataSet getMemberShipDataDS(string trackingid)
        {
            Helper myHelp = new Helper();
            DataSet myDS;

            myDS = myHelp.executeSPGetDS("onlineSalesTrackingIDSelect", "@trackingID", trackingid);
            return myDS;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Helper myHelp = new Helper();
                SqlParameter[] parameter = {
                new SqlParameter("@LeadSource",txtLeadSource.Text),
                new SqlParameter("@TrackingID",txtTrackingId.Text),
                new SqlParameter("@MemberTypeID",txtMemberTypeId.Text),
                new SqlParameter("@MemberTypeDesc",txtMemDesc.Text),
                new SqlParameter("@MembershipFee",txtMFees.Text),
                new SqlParameter("@MembershipRenewalFee",txtMRFees.Text),
                new SqlParameter("@MembershipFeeCAN",txtMFeesCAN.Text),
                new SqlParameter("@MembershipRenewalFeeCAN",txtMRFeesCAN.Text)
                };

                myHelp.executeSP("onlineSalesTrackingIDInsert", parameter);
                string tid = string.IsNullOrEmpty(txtTID.Text) ? "" : txtTID.Text;
                GVMembershipTypes.DataSource = getMemberShipDataDS(tid);
                GVMembershipTypes.DataBind();
                GVMembershipTypes.BottomPagerRow.Visible = true;
                pnlInsert.Visible = false;
                pnlAdd.Visible = true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                resetTextboxes();
                pnlAdd.Visible = true;
                pnlInsert.Visible = false;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            resetTextboxes();
        }

        private void resetTextboxes()
        {
            try
            {
                txtLeadSource.Text = "";
                txtTrackingId.Text = "";
                txtMemberTypeId.Text = "";
                txtMemDesc.Text = "";
                txtMFees.Text = "";
                txtMRFees.Text = "";
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        protected void GVMembershipTypes_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GVMembershipTypes.EditIndex = e.NewEditIndex;
            string tid = string.IsNullOrEmpty(txtTID.Text) ? "" : txtTID.Text;
            GVMembershipTypes.DataSource = getMemberShipDataDS(tid);
            GVMembershipTypes.DataBind();
        }

        protected void GVMembershipTypes_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            Helper myHelp = new Helper();
            SqlParameter[] parameter = {  
              new SqlParameter("@Id", GVMembershipTypes.Rows[e.RowIndex].Cells[1].Text),
              new SqlParameter("@MembershipFee",e.NewValues[0]),
              new SqlParameter("@MembershipRenewalFee",e.NewValues[1]),
               new SqlParameter("@MembershipFeeCAN",e.NewValues[2]),
              new SqlParameter("@MembershipRenewalFeeCAN",e.NewValues[3])
            };

            myHelp.executeSP("onlineSalesTrackingIDUpdate", parameter);           
            GVMembershipTypes.EditIndex = -1;
            string tid = string.IsNullOrEmpty(txtTID.Text) ? "" : txtTID.Text;
            GVMembershipTypes.DataSource = getMemberShipDataDS(tid);
            GVMembershipTypes.DataBind();

            GVMembershipTypes.BottomPagerRow.Visible = true;
            pnlInsert.Visible = false;
            pnlAdd.Visible = true;
        }

        protected void GVMembershipTypes_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GVMembershipTypes.EditIndex = -1;
            string tid = string.IsNullOrEmpty(txtTID.Text) ? "" : txtTID.Text;
            GVMembershipTypes.DataSource = getMemberShipDataDS(tid);
            GVMembershipTypes.DataBind();
        }

        protected void butSubmit_Click(object sender, EventArgs e)
        {
            GVMembershipTypes.DataSource = getMemberShipDataDS(txtTID.Text);
            GVMembershipTypes.DataBind();            
        }

        protected void btnPrev_Click(object sender, EventArgs e)
        {

        }

        protected void btnNext_Click(object sender, EventArgs e)
        {

        }

       
    }
}
﻿jQuery(document).ready(function ($) {
    $("#ApplyButton").attr("disabled", "disabled");
    $("#ContractBtn").attr("disabled", "disabled");
    $("#spousefname").attr("disabled", "disabled");
    $("#spouselname").attr("disabled", "disabled");
    $("#spouseemail").attr("disabled", "disabled");
    $("#coApplicant").hide();
    $("#coApplicant1").hide();
    $("#PromoID").hide();
    $("#ApplyButton").hide();

    //Cheking if Enter into Promo code then only enable Apply button.
    $("#PromoID").keyup(function (data) {
        if ($(this).val() != "") {
            $("#ApplyButton").removeAttr("disabled");
        }
        else {
            $("#ApplyButton").attr("disabled", "disabled");
        }
    });

    //Cheking if Enter into Promo code then only enable Apply button on blur event.
    //$("#PromoID").blur(function (data) {
    //    if ($(this).val() != "") {
    //        $("#ApplyButton").removeAttr("disabled");
    //    }
    //    else {
    //        $("#ApplyButton").attr("disabled", "disabled");
    //    }
    //});

    // Checking if Agree Terms & Condition checkbox is checked or not ?
    $("#chkIsAgreeTerms").click(function () {
        if ($("#chkIsAgreeTerms").is(':checked') && $("#chkTCPAagreement").is(':checked')) {
            $("#ContractBtn").removeAttr("disabled");
        }
        else {
            $("#ContractBtn").attr("disabled", "disabled");
        }
    });

    //Checking if TCPA Agreement checkbox is checked or not ?
    $("#chkTCPAagreement").click(function () {
        if ($("#chkIsAgreeTerms").is(':checked') && $("#chkTCPAagreement").is(':checked')) {
            $("#ContractBtn").removeAttr("disabled");
        }
        else {
            $("#ContractBtn").attr("disabled", "disabled");
        }
    });

    // Toggle if Co-Applicant is check then Enable section else Disabled.
    $("#chkIsCoApplicant").click(function () {
        if ($(this).is(':checked')) {
            $("#spousefname").removeAttr("disabled");
            $("#spouselname").removeAttr("disabled");
            $("#spouseemail").removeAttr("disabled");
            $("#coApplicant").show();
            $("#coApplicant1").show();
        }
        else {
            $("#spousefname").attr("disabled", "disabled");
            $("#spouselname").attr("disabled", "disabled");
            $("#spouseemail").attr("disabled", "disabled");
            $("#coApplicant").hide();
            $("#coApplicant1").hide();
        }
    });

    //$("#DivPromoCode").click(function () {
    //    $("#PromoID").toggle(25);
    //    $("#ApplyButton").toggle(25);
    //});


    ////Checking Textbox empty validation
    $('#ContractBtn').click(function (e) {
        var isValid = true;
        var countrySelect = $('#ddlCountry option:selected').text().toLowerCase();
        var stateSelect = $('#ddlState option:selected').text().toLowerCase();
                
        if ($.trim($("#fname").val()) == '') {
            isValid = false;
            $("#fname").addClass("error");
        }
        else {
            $("#fname").removeClass("error");
        }

        if ($.trim($("#lname").val()) == '') {
            isValid = false;
            $("#lname").addClass("error");
        }
        else {
            $("#lname").removeClass("error");
        }

        if ($.trim($("#email1").val()) == '') {
            isValid = false;
            $("#email1").addClass("error");
        }
        else {
            $("#email1").removeClass("error");
        }

        if ($.trim($("#phone1").val()) == '') {
            isValid = false;
                $("#phone1").addClass("error");
        }
        else {
                $("#phone1").removeClass("error");
        }

        if ($.trim($("#adddress1").val()) == '') {
            isValid = false;
                $("#adddress1").addClass("error");
        }
        else {
                $("#adddress1").removeClass("error");
        }

    //    if ($.trim($("#txtHomeNumber").val()) == '') {
    //        isValid = false;
    //        $("#txtHomeNumber").addClass("error");
    //    }
    //    else {
    //        $("#txtHomeNumber").removeClass("error");
    //    }

        if ($.trim($("#zip").val()) == '') {
            isValid = false;
            $("#zip").addClass("error");
        }
        else {
            $("#zip").removeClass("error");
        }

        if ($.trim($("#city1").val()) == '') {
            isValid = false;
            $("#city1").addClass("error");
        }
        else {
            $("#city1").removeClass("error");
        }

        if (countrySelect == "select country") {
            isValid = false;
            $("#ddlCountry").closest("div")
            .css("border", "1px solid red");
            //$("#ddlCountry").addClass("error");
        }
        else {
            $("#ddlCountry").closest("div")
            .css("border", "1px solid white");
        }

        if (stateSelect == "select state") {
            isValid = false;
            $("#ddlState").closest("div")
            .css("border", "1px solid red");
        }
        else {
            $("#ddlState").closest("div")
             .css("border", "1px solid white");
        }

    //    if ($("#chkIsCoApplicant").is(':checked')) {
    //        if ($.trim($("#txtCoApplicantFirstName").val()) == '') {
    //            isValid = false;
    //            $("#txtCoApplicantFirstName").addClass("error");
    //        }
    //        else {
    //            $("#txtCoApplicantFirstName").removeClass("error");
    //        }

    //        if ($.trim($("#txtCoApplicantLastName").val()) == '') {
    //            isValid = false;
    //            $("#txtCoApplicantLastName").addClass("error");
    //        }
    //        else {
    //            $("#txtCoApplicantLastName").removeClass("error");
    //        }

    //        if ($.trim($("#txtCoApplicantEmail").val()) == '') {
    //            isValid = false;
    //            $("#txtCoApplicantEmail").addClass("error");
    //        }
    //        else {
    //            $("#txtCoApplicantEmail").removeClass("error");
    //        }
    //    }

    //    if ($("#txtPaymentCardNumber").val() == '') {
    //        isValid = false;
    //        $("#txtPaymentCardNumber").addClass("error");
    //    }
    //    else {
    //        $("#txtPaymentCardNumber").removeClass("error");
    //    }

    //    if ($("#txtPaymentCardCVVNumber").val() == '') {
    //        isValid = false;
    //        $("#txtPaymentCardCVVNumber").addClass("error");
    //    }
    //    else {
    //        $("#txtPaymentCardCVVNumber").removeClass("error");
    //    }

    //    if ($("#ddlPaymentCardExpirationDate").val() == "Month") {
    //        isValid = false;
    //        $("#ddlPaymentCardExpirationDate").closest("div")
    //        .css("border", "1px solid red");
    //    }
    //    else {
    //        $("#ddlPaymentCardExpirationDate").removeClass("error");
    //    }

    //    if ($("#ddlPaymentCardYear").val() == "0") {
    //        isValid = false;
    //        $("#ddlPaymentCardYear").closest("div")
    //        .css("border", "1px solid red");
    //    }
    //    else {
    //        $("#ddlPaymentCardYear").removeClass("error");
    //    }

    


        if (isValid == false) {
            e.preventDefault();
            return false;
           
        }
    });

    //function pageLoad() {
    //    $("#<% =ddlCountry.ClientID %>").addClass("form-control select show-tick");
    //    $("#<% =ddlState.ClientID %>").addClass("form-control select show-tick");
    //    $("#<% =fname.ClientID %>").focus();
    //}

       
});


jQuery(document).on('click', function (e) {
    var elem = jQuery(e.target).closest('#DivPromoCode'),
        box = jQuery(e.target).closest('#PromoID');

    if (elem.length) {
        e.preventDefault();
        jQuery('#PromoID').toggle();
        jQuery('#ApplyButton').toggle();
    } else if (!box.length) {
        jQuery('#PromoID').hide();
        jQuery('#ApplyButton').hide();
    }
});
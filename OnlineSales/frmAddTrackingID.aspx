﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmAddTrackingID.aspx.cs" Inherits="OnlineSales.frmAddTrackingID" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 50%;
        }
    </style>
    <script src="assets/js/modernizr-2.8.3.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="assets/Scripts/jquery.validate.min.js"></script>
    <link href="assets/css/main.css" rel="stylesheet" />

    <script>

        $(document).ready(function () {
      
            // Initialize form validation on the registration form.
            // It has the name attribute "registration"
            $("#frmAdd").validate({
                // Specify validation rules
                rules: {
                    // The key name on the left side is the name attribute
                    // of an input field. Validation rules are defined
                    // on the right side
                    <%=txtLeadSource.UniqueID%> : {
                        required:true                         
                    },
                    <%=txtTrackingId.UniqueID%> : {
                        required:true                         
                    },
                    <%=txtMemberTypeId.UniqueID%> : {
                        required:true
                    },
                    <%=txtMemDesc.UniqueID%> : {
                        required:true
                    },
                    <%=txtMFees.UniqueID%> : {
                        required:true
                    },
                    <%=txtMRFees.UniqueID%> : {
                        required:true
                    }
                },
                    // Specify validation error messages
                    messages: {
                        <%=txtLeadSource.UniqueID%> : {
                             required:"Please enter lead source"  
                         },
                        <%=txtLeadSource.UniqueID%> : {
                             required:"Please enter tracking id"  
                         },
                        <%=txtMemberTypeId.UniqueID%> : {
                             required:"Please enter member type id"  
                         },
                        <%=txtMemDesc.UniqueID%> : {
                             required:"Please enter member description"  
                         },
                        <%=txtMFees.UniqueID%> : {
                             required:"Please enter membership fees"  
                         },
                        <%=txtMRFees.UniqueID%> : {
                             required:"Please enter membership renewal fees"  
                         }
                    },
                    errorPlacement : function(error,element)
                    {                           
                        error.insertAfter( element );                                        
                    },
                    // Make sure the form is submitted to the destination defined
                    // in the "action" attribute of the form when valid
                    submitHandler: function (form) {
                    
                        form.submit();
                    },
                });
        });


    </script>
</head>
<body>
    <form id="frmAdd" runat="server">
        <p />

        <div>
            <asp:Label ID="Label7" runat="server" Text="Tracking ID : "></asp:Label>
            <asp:TextBox ID="txtTID" runat="server"></asp:TextBox>
            <asp:Button ID="butSubmit" runat="server" Text="Submit" OnClick="butSubmit_Click" />
        </div>
        <p />
        <p />
        <div>

            <asp:GridView ID="GVMembershipTypes" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" AutoGenerateEditButton="True" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCommand="GvMembershipTypes_RowCommand" OnRowCancelingEdit="GVMembershipTypes_RowCancelingEdit" OnRowEditing="GVMembershipTypes_RowEditing" OnRowUpdating="GVMembershipTypes_RowUpdating" PageSize="30" >
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:BoundField DataField="id" HeaderText="id" ReadOnly="True"/>
                    <asp:BoundField DataField="LeadSource" HeaderText="Lead Source" ReadOnly="True"  />
                    <asp:BoundField DataField="TrackingID" HeaderText="Tracking ID" ReadOnly="True" />
                    <asp:BoundField DataField="MemberTypeID" HeaderText="Member Type ID" ReadOnly="True" />
                    <asp:BoundField DataField="MemberTypeDesc" HeaderText="Member Type Desc" ReadOnly="True" />
                    <asp:BoundField DataField="MembershipFee" HeaderText="Membership Fee" />
                    <asp:BoundField DataField="MembershipRenewalFee" HeaderText="Membership Renewal Fee" />
                          <asp:BoundField DataField="MembershipFeeCAN" HeaderText="Membership Fee CANADA" />
                    <asp:BoundField DataField="MembershipRenewalFeeCAN" HeaderText="Membership Renewal Fee CANADA" />
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <PagerTemplate>
                     <asp:Button ID="btnPrev" runat="server" Text="<" OnClick="btnPrev_Click"   />
                     <asp:Button ID="btnNext" runat="server" Text=">" OnClick="btnNext_Click"   />
                </PagerTemplate>
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>

        </div>
        <br />
        <asp:Panel ID="pnlAdd" runat="server" Visible="True">
            <asp:Button ID="btnInsert" runat="server" Text="Add" OnClick="btnInsert_Click"  class="cancel" />
        </asp:Panel>
        <br />
        <asp:Panel ID="pnlInsert" runat="server" Visible="false">

            <table class="auto-style1">
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Lead Source"></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtLeadSource" CssClass="textbox" runat="server" MaxLength="25"  ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Tracking Id"></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtTrackingId" CssClass="textbox" runat="server" MaxLength="10"  ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Member Type Id"></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtMemberTypeId" CssClass="textbox" runat="server" MaxLength="3"  ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="Member Type Description"></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtMemDesc" CssClass="textbox" runat="server" MaxLength="150" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label5" runat="server" Text="Mem Fee"></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtMFees" CssClass="textbox" runat="server" MaxLength="10" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label6" runat="server" Text="Mem Renewal Fee"></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtMRFees" CssClass="textbox" runat="server" MaxLength="10" ></asp:TextBox>
                    </td>
                </tr>
                    <tr>
                    <td>
                        <asp:Label ID="Label8" runat="server" Text="Mem Fee CANADA"></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtMFeesCAN" CssClass="textbox" runat="server" MaxLength="10" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label9" runat="server" Text="Mem Renewal Fee CANADA"></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtMRFeesCAN" CssClass="textbox" runat="server" MaxLength="10" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                        &nbsp;
                    <asp:Button ID="btnCancel"  runat="server" Text="Cancel" class="cancel" OnClick="btnCancel_Click" /></td>
                    <td>
                        <asp:Button ID="btnReset"  runat="server" Text="Reset" class="cancel" OnClick="btnReset_Click" /></td>
                </tr>
            </table>
        </asp:Panel>

        <br />
        <br />
    </form>
</body>
</html>

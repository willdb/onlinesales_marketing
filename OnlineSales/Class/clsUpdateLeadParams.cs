﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineSales
{
    public class clsUpdateLeadParams
    {
         // Mandatory Params
        public string leads_id {get; set;}
        public string email {get; set;}

         // Optional Params
        public string first_name {get; set;}
        public string last_name {get; set;}
        public string spouse_first_name {get; set;}
        public string spouse_last_name {get; set;}
        public string address1 {get; set;}
        public string address2 { get; set; }
        public string city {get; set;}
        public string state {get; set;}
        public string postal_code {get; set;}
        public string country {get; set;}
        public string primary_phone {get; set;}
        public string primary_phone_type {get; set;}
        public string secondary_phone {get; set;}
        public string secondary_phone_type {get; set;}
        public string member_id { get; set; }
        public string lead_status { get; set; }
        public string notes { get; set; } // enter contract session key in notes field as this also is used in email campaigns
        public string url { get; set; } // eco sign url
        public string Contract_Signed { get; set; }  //contract signed yes or no
        public string bionic_status { get; set; } 
    }
}
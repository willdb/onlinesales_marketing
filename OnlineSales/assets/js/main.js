!function($) {

    // Readied
    $(function() {

        var $shopMenu = $('#shop-menu'),
            $shopSubMenu = $shopMenu.find('.submenu'),
            $accountMenu = $('#account-menu'),
            $pageHeight = $('#mobile-wrap').outerHeight(),
            $customSelects = $("select:not(.ignore)"),
            $customInputs = $('input[type="checkbox"]:not(.no-transform), input[type="radio"]:not(.no-transform)');

        // Set shop/account menu height to be equal to the page height
        $shopMenu.height($pageHeight);
        $shopSubMenu.height($pageHeight);
        $accountMenu.height($pageHeight);

        // Account Menu functionality
        $('.account-link-trigger').on('click', function(e) {
            e.preventDefault();
            if($accountMenu.hasClass('opened')) {
                $closeMenu();
            } else {
                $('#mobile-wrap').prepend('<div class="page-shadow"></div>');
                $accountMenu.addClass('opened');

                $('.top-close a.close-trigger').on('click', function(e) {
                    e.preventDefault();
                    $closeMenu();
                });
            }
        });

        // Function that handles removing the open class from the shop and account menus, as well as clearing out
        // active menus and breadcrumbs
        function $closeMenu() {
            $shopMenu.add($accountMenu).removeClass('opened');
            $shopMenu.find('.parent').removeClass('active').find('.submenu').removeClass('show');
            $('.page-shadow').remove();
            // Also, if it is the mobile nav, we want to remove all breadcrumbs
            if($shopMenu.hasClass('mobile')) {
                $shopMenu.find('.breadcrumbs a').remove();
            }
        }

        // Utility nav dropdowns
        $('#masthead .utility-nav').Menu({
            'speed': 300,
            'delay': 100,
            useHI: true,
            'triggerSelector' : '.parent',
            'menuSelector' : '.dropdown-menu'
        });


         // Initialize custom radios/checkboxes
        if ($customInputs.length) {
            $customInputs.CustomInputs();
        }

        // Initialize custom selects
        if($customSelects.length) {
            $customSelects.CustomSelects();
        }

        Respond.to({
            'media': '(min-width: 1400px)',
            'namespace': '1400_transition_global',
            'fallback': 'else',
            'default': 'else',
            'if': function () {

                // Attach the Shop Menu on desktop
                $shopMenu.addClass('attached');

            },
            'else' : function() {

                // Hide the Shop Menu on smaller desktop/tablet/mobile
                $shopMenu.removeClass('attached');

                $('.shop-menu-trigger').on('click', function(e) {
                    e.preventDefault();
                    if($shopMenu.hasClass('opened')) {
                        $closeMenu();
                    } else {
                        $('#mobile-wrap').prepend('<div class="page-shadow"></div>');
                        $shopMenu.addClass('opened');

                        $('.top-close a.close-trigger').on('click', function(e) {
                            e.preventDefault();
                            $closeMenu();
                        });
                    }
                });

            }
        });


        Respond.to({
            'media': '(max-width: 768px)',
            'namespace': '768_transition_global',
            'fallback': 'else',
            'default': 'else',
            'if': function () {
                // Variables for Mobile shop menu
                var $shopMenuParentLink = $shopMenu.find('li.parent > a'),
                    $menuBreadcrumbs = $shopMenu.find('.breadcrumbs'),
                    $shopMenuBackLink = $shopMenu.find('.back-link');

                // Unset the desktop navigation experience and add a mobile class
                $shopMenu.Menu('unset').addClass('mobile');


                // Mobile navigation
                $shopMenuParentLink.on('click', function(e) {

                    var $catDepth = $(this).next().data('depth') + 1;

                    e.preventDefault();

                    // Show the back link at this point
                    $shopMenuBackLink.show();

                    // Show the submenu
                    $(this).addClass('active').next('.submenu').addClass('show');

                    // If there are previous breadcrumbs, make them navigate-able and set the latest one to false
                    if($('.breadcrumbs a').length) {

                        $('.breadcrumbs a').attr('data-is-breadcrumb', 'true');
                        $(this).clone().appendTo($menuBreadcrumbs).attr({
                            'data-is-breadcrumb': 'false',
                            'id': "level" + $catDepth
                        });

                    } else {
                        $(this).clone().appendTo($menuBreadcrumbs).attr({
                            'data-is-breadcrumb': 'false',
                            'id': "level" + $catDepth
                        });
                    }

                    // Show the breadcrumbs
                    $menuBreadcrumbs.show();

                    // Scroll to top of page when selection is made
                    $('html, body').animate({scrollTop : 0}, 400, function() { });

                    // Clicking a breadcrumb, we want either no action, or to go back 1 category
                    $('.breadcrumbs a').on('click', function(e) {
                        e.preventDefault();

                        var $levelDepth = $(this).attr('id');

                        // If this is a breadcrumb or "back" link, do some logic based on our data attribute and ID,
                        // however, we only want to do this is "is-breadcrumb" is true. Otherwise, it acts as just an
                        // indication of the parent category you're currently viewing.
                        if($(this).data('is-breadcrumb') === true) {

                            $shopMenu.find('ul.' + $levelDepth).removeClass('show');

                            // Clean up the breadcrumb list
                            $(this).nextAll('a').remove();
                        }
                    });


                });

                // Clicking the back link clears breadcrumbs and active categories
                $shopMenuBackLink.on('click', function(e) {
                    e.preventDefault();

                    // Close all active submenus
                    $shopMenu.find('.submenu').removeClass('show');

                    // Remove the breadcrumbs
                    $menuBreadcrumbs.find('a').remove();

                    // Hide the back link
                    $(this).hide();
                });



                // Add class "collapse" to footer links for accordions
                $('#footer .block .links').addClass('collapse');


            },
            'else': function () {

                $('#footer .block .links').removeClass('collapse');

                // Remove the click bound on the parent links
                $shopMenu.find('li.parent > a').off('click');

                // Remove the breadcrumbs if changing screen resolution
                $('#shop-menu .breadcrumbs a').remove();

                // Shop navigation flyout menu on tablet/desktop
                $shopMenu.Menu({
                    'speed': 300,
                    'delay': 100,
                    useHI: true,
                    'triggerSelector' : '.parent',
                    'menuSelector' : '.submenu'
                });


                $shopMenu.removeClass('mobile');
            }

        });

        // Smooth scroll to top utility
        if ($('.scrollable').length) {
            $(document).on('click', '.scrollable a, a.scrollable, a[href="#top"]', function(e) {
                e.preventDefault();
                var $this = $(this);
                if ($this.attr("href").match(/^#/)) {
                    var top = $($this.attr("href")).length ? $($this.attr("href")).offset().top : 0;
                    $('html, body').animate({scrollTop : top}, 400, function() { });
                    return false;
                }
            });
        }

    });

}(window.jQuery);

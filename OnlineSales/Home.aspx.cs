﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Configuration;

namespace OnlineSales
{
    public partial class Home : System.Web.UI.Page
    {
        public string inClubSales = "N";
        public clsMember memberObj = new clsMember();
        Lead ExistingLead = new Lead();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getMemberShipTypes("");
                string leadid = Request.QueryString["leadid"] != null ? Request.QueryString["leadid"].ToString() : "";
                string referByMemberID = Request.QueryString["memberid"] != null ? Request.QueryString["memberid"].ToString() : "";
                //from sponsord paged there will be two query strings 1) tracking id 531408 2) refer code. Refercode we need to populate in the promo text box
                string referCode = Request.QueryString["refercode"] != null ? Request.QueryString["refercode"].ToString() : "";
                PromoID.Text = referCode;

                if (leadid != "")
                {
                    //NOTE: we are commenting this call out for now to see if it has any relation to the host monitor downtime we keep seeing
                    populateLeadInformation(leadid);

                    //store lead id in persistant object
                    // this lead id from memberobj will be used when we will update memberid in to the lead.  
                    if (ExistingLead.LeadID != null)
                    {
                        memberObj.LeadID = ExistingLead.LeadID.ToString();
                    }
                }
                memberObj.referByMemberID = referByMemberID;
                memberObj.traffic_source = Request.ServerVariables["HTTP_REFERER"];
                Session.Add("memberObj", memberObj);
            }
        }

        protected void ContractBtn_Click(object sender, EventArgs e)
        {
            memberObj = (clsMember)Session["memberObj"];
            CreateEchosignAgreement();
            UpdateLeadWithMemberID();
        }

        internal void getMemberShipTypes(string cntry)
        {
            Helper myHelp = new Helper();
            //OSServices.OSServicesClient stateservice = new OSServices.OSServicesClient();
            try
            {
                string Country, TSCount = string.Empty;
                int clubid = 0;

                //Expect two parameters in every request we got from ion, otherwise default the 
                //TrackingID to 531393   and   country to USA 
                // note : we are offering club 925 for USA OOT and club 935 for canadian OOT so we will pass 925 if country is USA and 935 if country is canada

                string trackingid = Request.QueryString["tid"] != null ? Request.QueryString["tid"].ToString() : ConfigurationManager.AppSettings["defaultTrackingID"].ToString();

                Country = Request.QueryString["country"] != null ? Request.QueryString["country"].ToString() : ConfigurationManager.AppSettings["DefaultCountry"].ToString();

                // this is to assign country code and fetch the membership payments data if user changes the country from drop down
                if (cntry != "")
                {
                    Country = cntry;
                }

                //push country code in hidden field to show currency labels
                hfCountry.Value = Country;

                
                clubid = Country.ToUpper() == "CAN" ? int.Parse(ConfigurationManager.AppSettings["OOT_CANClubID"].ToString()) : int.Parse(ConfigurationManager.AppSettings["OOT_USAClubID"].ToString());
                TSCount = myHelp.checkIfTrackingIDExists(trackingid);
                trackingid = int.Parse(TSCount) > 0 ? trackingid : "531393";
                //trackingid = "531393";

                //string memberShipTypeJson = stateservice.GetMembershipType(trackingid, clubid);

                DataSet myDS;   
                myDS = myHelp.executeSPGetDS("GetMembershipTypeByTrackingID", "@TrackingID", trackingid, "@CenterID", clubid);
                string memberShipTypeJson = myHelp.DataSetToJSON(myDS);
                //string memberShipTypeJson =
                //    "[{\"id\":1,\"leadSource\":\"C_DBCOM_OLS\",\"TrackingID\":\"531393\",\"MemberTypeID\":15,\"MemberTypeDesc\":\"Concierge Shopping Signature\",\"MembershipFee\":\"199\",\"Term\":\"12\",\"MembershipRenewalFee\":\" 39.95\"},{\"id\":3,\"leadSource\":\"C_DBCOM_OLS\",\"TrackingID\":\"531393\",\"MemberTypeID\":16,\"MemberTypeDesc\":\"Concierge Shopping Premier with Travel\",\"MembershipFee\":\"999\",\"Term\":\"12\",\"MembershipRenewalFee\":\"49.95\"},{\"id\":33,\"leadSource\":\"C_DBCOM_OLS\",\"TrackingID\":\"531393\",\"MemberTypeID\":13,\"MemberTypeDesc\":\"Discover 30\",\"MembershipFee\":\"1\",\"Term\":\"12\",\"MembershipRenewalFee\":\"1\"}]";


                // verify if we get some membership values for the provided tracking id and club combination as a response
                // from the service if yes then we proceed further otherwise we will stop
                if (memberShipTypeJson.Length > 2)
                {
                    //saving json to hiddenfiled so that it can be bound to UI
                    hfMemberTypeJson.Value = memberShipTypeJson;

                    //store traffic source and country in persistant object
                    memberObj.TrackingID = trackingid;
                    memberObj.Country = Country;

                    //add all received values for membership type to the persistant storage
                    List<MembershipTypeResposne> objMTinfoResponse = new List<MembershipTypeResposne>();
                    objMTinfoResponse = myHelp.deserializeMembershipTypeResposne(memberShipTypeJson);

                    memberObj.MTResponse = new OnlineSales.MembershipTypeResposne[objMTinfoResponse.Count];
                    int i = 0;
                    foreach (MembershipTypeResposne mtResponse in objMTinfoResponse)
                    {
                        memberObj.MTResponse[i] = new MembershipTypeResposne();
                        memberObj.MTResponse[i].id = mtResponse.id;
                        memberObj.MTResponse[i].MembershipFee = mtResponse.MembershipFee;
                        memberObj.MTResponse[i].MembershipRenewalFee = mtResponse.MembershipRenewalFee;
                        memberObj.MTResponse[i].MemberTypeDesc = mtResponse.MemberTypeDesc;
                        memberObj.MTResponse[i].MemberTypeID = mtResponse.MemberTypeID;
                        memberObj.MTResponse[i].Term = mtResponse.Term;
                        memberObj.MTResponse[i].LeadSource = mtResponse.LeadSource;
                        memberObj.MTResponse[i].TrackingID = mtResponse.TrackingID;
                        i++;
                    }
                    Session.Add("memberObj", memberObj);
                    //Log add received data to the user activity tracket
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, trackingid, "GetMemberShipTypes", "Success", memberShipTypeJson, "LeadSource : " + memberObj.MTResponse[0].LeadSource + " - ClubId : " + clubid, DateTime.Now);

                    ddlCountry.SelectedValue = Country.Substring(0, 2);
                    populateStates(Country);
                }
                else
                {
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, trackingid, "GetMemberShipTypes", "Failure", memberShipTypeJson, "LeadSource : " + memberObj.MTResponse[0].LeadSource + " - ClubId : " + clubid, DateTime.Now);
                    throw new NoNullAllowedException("GetMemberShipType service returned null value");
                }
            }
            catch (NoNullAllowedException ex)
            {
                myHelp.WriteLogToDB("Error", "HomePage --> getMemberShipTypes()", "", ex.Message, DateTime.Now);
                Server.Transfer("ErrorPage.aspx");
            }
            catch (Exception ex)
            {
                myHelp.WriteLogToDB("Error", "HomePage --> getMemberShipTypes()", "", ex.Message, DateTime.Now);
                throw;
            }
            finally
            {
                myHelp = null;
                //stateservice = null;
            }
        }

        internal void populateLeadInformation(string leadid)
        {
            Helper myHelp = new Helper();
            string LeadsTable = string.Empty;
            DataSet myDS;
            try
            {
                string ManualExecution = ConfigurationManager.AppSettings["ManualExecution"].ToString();
                if (ManualExecution.ToUpper() == "TRUE")
                {
                    // if we are doing  manual execution we will hit the test table
                    LeadsTable = "leads_test";
                }
                else
                {
                    // if we are doing automated execution we will hit the produciton table
                    LeadsTable = "leads";
                }

                myDS = myHelp.executeSPGetDS("GetLeadDetailsByLeadID", "@LeadID", leadid, "@LeadsTable", LeadsTable);
                if (myDS.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow myRow in myDS.Tables[0].Rows)
                    {
                        ExistingLead.LeadID = myRow["id"].ToString();
                        ExistingLead.SubmitDate = myRow["submit_date"].ToString();
                        ExistingLead.ClubID = myRow["clubs_id"].ToString();
                        ExistingLead.CurrentClubID = myRow["current_club_id"].ToString();
                        ExistingLead.LeadStatus = myRow["lead_status"].ToString();
                        ExistingLead.LeadSource = myRow["lead_source"].ToString();
                        ExistingLead.TrackingID = myRow["tracking_id"].ToString();
                        ExistingLead.FirstName = myRow["first_name"].ToString();
                        ExistingLead.LastName = myRow["last_name"].ToString();
                        ExistingLead.Street1 = myRow["street1"].ToString();
                        ExistingLead.Street2 = myRow["street2"].ToString();
                        ExistingLead.City = myRow["city"].ToString();
                        ExistingLead.State = myRow["state"].ToString();
                        ExistingLead.Country = myRow["country"].ToString();
                        ExistingLead.PostalCode = myRow["postal_code"].ToString();
                        ExistingLead.Email = myRow["email"].ToString();
                        ExistingLead.PrimaryPhone = myRow["primary_phone"].ToString();
                        ExistingLead.PrimaryPhone = ExistingLead.PrimaryPhone == "" ? "" : ExistingLead.PrimaryPhone.Insert(3, "-").Insert(7, "-");
                        ExistingLead.TCPA = myRow["tcpa"].ToString();
                    }

                    // assign lead values to the form
                    fname.Text = ExistingLead.FirstName;
                    lname.Text = ExistingLead.LastName;
                    email1.Text = ExistingLead.Email;
                    phone1.Text = ExistingLead.PrimaryPhone;
                    address1.Text = ExistingLead.Street1;
                    address2.Text = ExistingLead.Street2;
                    ddlCountry.SelectedValue = ExistingLead.Country;
                    if (ExistingLead.Country == "CA") populateStates("CA");
                    zip.Text = ExistingLead.PostalCode;
                    if (ExistingLead.TCPA == "1") chkIsAgreeTerms.Checked = true;

                    string dataReceived = string.Concat("LeadID : ", ExistingLead.LeadID, " | ", "submitDate : ", ExistingLead.SubmitDate, " | ", "ClubID : ", ExistingLead.ClubID, " | ", "CurrentClubID : ", ExistingLead.CurrentClubID, " | ", "FirstName : ", ExistingLead.FirstName, " | ", "LastName : ", ExistingLead.LastName, " | ", "Email : ", ExistingLead.Email, " | ", "PrimaryPhone : ", ExistingLead.PrimaryPhone);
                    //L O G G the successful lead extraction from query string lead id and population of form based on that information
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "Pouplate lead from Query String leadID", "Success", dataReceived, "Lead ID : " + ExistingLead.LeadID, DateTime.Now);
                }
                else
                {
                    //L O G G lead has no data - either lead is not presnet or lead information is not extracted
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "Pouplate lead from Query String leadID", "Failure", "Lead is not present or failed to extract lead", "LeadID = " + leadid, DateTime.Now);
                }

                HttpContext.Current.Session.Add("existingLead", ExistingLead);
            }
            catch (Exception ex)
            {
                myHelp.WriteLogToDB("Error", "HomePage --> populateLeadInformation()", "", ex.Message, DateTime.Now);
                throw;
            }
            finally
            {
                myHelp = null;
            }
        }

        internal void populateStates(string countryCode)
        {
            Helper myHelp = new Helper();
            try
            {
                ddlState.DataSource = myHelp.executeSPGetDS("spOnlineSales_GetServiceableStates", "@Country", countryCode);
                ddlState.DataTextField = "StateName";
                ddlState.DataValueField = "StateShortName";
                ddlState.DataBind();
                ddlState.Items.Insert(0, new ListItem("Select State", "-1"));
            }
            catch (Exception ex)
            {
                myHelp.WriteLogToDB("Error", "HomePage --> populateStates()", "", ex.Message, DateTime.Now);
                throw;
            }
            finally
            {
                myHelp = null;
            }
        }

        internal void PostLead()
        {
            Helper myHelp = new Helper();
            memberObj = (clsMember)Session["memberObj"];
            string dataPosted = string.Empty;
            string dataReceived = string.Empty;
            string centerID = string.Empty;

            string EnterpriseUSAClubID = ConfigurationManager.AppSettings["EnterpriseUSAClubID"].ToString();
            string EnterpriseCANClubID = ConfigurationManager.AppSettings["EnterpriseCANClubID"].ToString();

            string[] EnterpriseTrackingIDs = ConfigurationManager.AppSettings["EnterpriseTrackingIDs"].ToString().Split(',');
            if (EnterpriseTrackingIDs.Contains(memberObj.TrackingID))
            {
                centerID = ddlCountry.SelectedValue == "US" ? EnterpriseUSAClubID : EnterpriseCANClubID;
            }
            else
            {
                centerID = ddlCountry.SelectedValue == "US" ? "2" : "935";
            }

            try
            {

                clsPostLeadParams myLead = new clsPostLeadParams();
                myLead.first_name = fname.Text.Trim();
                myLead.last_name = lname.Text.Trim();
                myLead.email = email1.Text.Trim();
                myLead.clubs_id = centerID;
                myLead.tracking_id = memberObj.TrackingID != null ? memberObj.TrackingID : ConfigurationManager.AppSettings["defaultTrackingID"].ToString();
                myLead.spouse_first_name = spousefname.Text.Trim();
                myLead.spouse_last_name = spouselname.Text.Trim();
                myLead.address1 = address1.Text.Trim();
                myLead.address2 = address2.Text.Trim();
                myLead.city = city1.Text.Trim();
                myLead.postal_code = zip.Text.Trim();
                myLead.state = ddlState.SelectedItem.Value;
                myLead.country = ddlCountry.SelectedItem.Value;
                myLead.primary_phone = phone1.Text.Replace("(", "").Replace(".", "").Replace("-", "").Replace(")", "");
                myLead.primary_phone_type = "home";
                myLead.secondary_phone = "";
                myLead.secondary_phone_type = "";

                myLead.traffic_source = memberObj.traffic_source;
                myLead.utm_term = "";
                myLead.affiliate_lead_id = "";
                myLead.hubspotutk = "";
                myLead.referrer = "";
                myLead.call_disposition = "";
                myLead.notes = "";
                myLead.affiliate_lead_id = "";

                PostDataResponse objPostData = new PostDataResponse();
                objPostData = myHelp.MkAPICall_PostLead(myLead, myLead.tracking_id);

                dataPosted = "FirstName : " + myLead.first_name + " - " + "LastName : " + myLead.last_name + " - " + "Email : " + myLead.email + "primary phone : " + myLead.primary_phone;

                if (objPostData.response_status == "success")
                {
                    //store lead id in persistant object
                    memberObj.LeadID = objPostData.response_message.ToString();
                    //L O G G user data used for lead creation to the user activity tracker
                    dataReceived = "Lead ID : " + memberObj.LeadID;
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "PostLead", objPostData.response_status, dataReceived, dataPosted, DateTime.Now);
                }
                else if (objPostData.response_status == "failure")
                {
                    //L O G G user data used for lead creation to the user activity tracker
                    dataReceived = "Response Message : " + objPostData.response_message + "-- Response Message Variables : " + objPostData.response_message_variables;
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "PostLead", objPostData.response_status, dataReceived, dataPosted, DateTime.Now);
                    throw new HttpUnhandledException("POST_LEAD API call resulted in a failure : " + objPostData.response_message_variables);
                }
            }
            catch (HttpUnhandledException ex)
            {
                myHelp.WriteLogToDB("Error", "HomePage --> PostLead()", "", ex.Message, DateTime.Now);
                Server.Transfer("ErrorPage.aspx");
            }
            catch (Exception ex)
            {
                myHelp.WriteLogToDB("Error", "HomePage --> PostLead()", "", ex.Message, DateTime.Now);
                throw;
            }
            finally
            {
                myHelp = null;
            }
        }

        // get url with will redirect the user to either staging or production payment portal
        internal string getRedirectURL()
        {
            Helper myHelp = new Helper();
            try
            {
                string redirectURL = string.Empty;
                string ContractSessionKey = HttpContext.Current.Session["ContractSessionKey"].ToString();

                string ManualExecution = ConfigurationManager.AppSettings["ManualExecution"].ToString();
                string EcoSignRedirectURL_Staging = ConfigurationManager.AppSettings["EcoSignRedirectURL_Staging"].ToString();
                string EcoSignRedirectURL_PROD = ConfigurationManager.AppSettings["EcoSignRedirectURL_PROD"].ToString();

                if (ManualExecution.ToUpper() == "TRUE")
                {
                    redirectURL = EcoSignRedirectURL_Staging + "?" + ContractSessionKey;
                }
                else
                {
                    redirectURL = EcoSignRedirectURL_PROD + "?" + ContractSessionKey;
                }
                return redirectURL;
            }
            catch (Exception ex)
            {
                myHelp.WriteLogToDB("Error", "HomePage --> getRedirectURL()", "", ex.Message, DateTime.Now);
                throw;
            }
            finally
            {
                myHelp = null;
            }
        }

        internal void CreateEchosignAgreement()
        {
            Helper myHelp = new Helper();
            try
            {

                // get list of tracking ids which dont require document signing
                string[] TrackingIDS_SkipDocumentSign = ConfigurationManager.AppSettings["TrackingID_AvoidContractSign"].ToString().Split(',');
                // get list of tracking ids which dont require payment processing signing
                string[] TrackingIDS_SkipPaymentProcessing = ConfigurationManager.AppSettings["TrackingID_AvoidPaymentProcessing"].ToString().Split(',');

                memberObj = (clsMember)Session["memberObj"];
                if (memberObj.LeadID == "" || memberObj.LeadID == null)
                {
                    PostLead();
                }
                else
                {
                    UpdateLead();
                }

                string ContractSessionKey = myHelp.getContractSessionKey();
                GetMemberID._WSOnlineInfo myWSinfo = new GetMemberID._WSOnlineInfo();

                // prepare financial information for json creation
                clsMemFinInfo objMemFinInfo = new clsMemFinInfo();

                //todo make this information dynamic
                if (cbxSig.Checked == true)
                {
                    if (memberObj.MTResponse[0].MemberTypeID == "15")
                    {
                        objMemFinInfo.Membership_type_id = memberObj.MTResponse[0].MemberTypeID;
                        objMemFinInfo.Membership_type = "Signature";
                        objMemFinInfo.Monthly_fee = Double.Parse(memberObj.MTResponse[0].MembershipRenewalFee).ToString("####.00");
                        objMemFinInfo.Tops_membership_cost = Double.Parse(memberObj.MTResponse[0].MembershipFee).ToString("####.00");
                        objMemFinInfo.Membership_cost = (Double.Parse(memberObj.MTResponse[0].MembershipFee) + (Double.Parse(memberObj.MTResponse[0].MembershipRenewalFee) * int.Parse(memberObj.MTResponse[0].Term))).ToString("####.00");
                    }
                    else if (memberObj.MTResponse[1].MemberTypeID == "15")
                    {
                        objMemFinInfo.Membership_type_id = memberObj.MTResponse[1].MemberTypeID;
                        objMemFinInfo.Membership_type = "Signature";
                        objMemFinInfo.Monthly_fee = Double.Parse(memberObj.MTResponse[1].MembershipRenewalFee).ToString("####.00");
                        objMemFinInfo.Tops_membership_cost = Double.Parse(memberObj.MTResponse[1].MembershipFee).ToString("####.00");
                        objMemFinInfo.Membership_cost = (Double.Parse(memberObj.MTResponse[1].MembershipFee) + (Double.Parse(memberObj.MTResponse[1].MembershipRenewalFee) * int.Parse(memberObj.MTResponse[1].Term))).ToString("####.00");
                    }
                }
                else if (cbxPrem.Checked == true)
                {
                    if (memberObj.MTResponse[0].MemberTypeID == "16")
                    {
                        objMemFinInfo.Membership_type_id = memberObj.MTResponse[0].MemberTypeID;
                        objMemFinInfo.Membership_type = "Premier";
                        objMemFinInfo.Monthly_fee = Double.Parse(memberObj.MTResponse[0].MembershipRenewalFee).ToString("####.00");
                        objMemFinInfo.Tops_membership_cost = Double.Parse(memberObj.MTResponse[0].MembershipFee).ToString("####.00");
                        objMemFinInfo.Membership_cost = (Double.Parse(memberObj.MTResponse[0].MembershipFee) + (Double.Parse(memberObj.MTResponse[0].MembershipRenewalFee) * int.Parse(memberObj.MTResponse[0].Term))).ToString("####.00");
                    }
                    else if (memberObj.MTResponse[1].MemberTypeID == "16")
                    {
                        objMemFinInfo.Membership_type_id = memberObj.MTResponse[1].MemberTypeID;
                        objMemFinInfo.Membership_type = "Premier";
                        objMemFinInfo.Monthly_fee = Double.Parse(memberObj.MTResponse[1].MembershipRenewalFee).ToString("####.00");
                        objMemFinInfo.Tops_membership_cost = Double.Parse(memberObj.MTResponse[1].MembershipFee).ToString("####.00");
                        objMemFinInfo.Membership_cost = (Double.Parse(memberObj.MTResponse[1].MembershipFee) + (Double.Parse(memberObj.MTResponse[1].MembershipRenewalFee) * int.Parse(memberObj.MTResponse[1].Term))).ToString("####.00");
                    }
                }
                else if (cbxDiscover.Checked == true)
                {
                    if (memberObj.MTResponse[0].MemberTypeID == "13")
                    {
                        objMemFinInfo.Membership_type_id = memberObj.MTResponse[0].MemberTypeID;
                        objMemFinInfo.Membership_type = "Discover30";
                        objMemFinInfo.Monthly_fee = Double.Parse(memberObj.MTResponse[0].MembershipRenewalFee).ToString("####.00");
                        objMemFinInfo.Tops_membership_cost = memberObj.MTResponse[0].MembershipFee == null ? "0" : Double.Parse(memberObj.MTResponse[0].MembershipFee).ToString("####.00");
                        objMemFinInfo.Membership_cost = (Double.Parse(memberObj.MTResponse[0].MembershipFee) + (Double.Parse(memberObj.MTResponse[0].MembershipRenewalFee) * int.Parse(memberObj.MTResponse[0].Term))).ToString("####.00");
                    }

                }
                else if (cbxEntSignature.Checked == true)
                {
                    if (memberObj.MTResponse[0].MemberTypeID == "30")
                    {
                        objMemFinInfo.Membership_type_id = memberObj.MTResponse[0].MemberTypeID;
                        objMemFinInfo.Membership_type = "National Enterprise Signature";
                        objMemFinInfo.Monthly_fee = Double.Parse(memberObj.MTResponse[0].MembershipRenewalFee).ToString("####.00");
                        objMemFinInfo.Tops_membership_cost = memberObj.MTResponse[0].MembershipFee == null ? "0" : Double.Parse(memberObj.MTResponse[0].MembershipFee).ToString("####.00");
                        objMemFinInfo.Membership_cost = (Double.Parse(memberObj.MTResponse[0].MembershipFee) + (Double.Parse(memberObj.MTResponse[0].MembershipRenewalFee) * int.Parse(memberObj.MTResponse[0].Term))).ToString("####.00");
                    }
                }
                else if (cbxNationalEntTrial.Checked == true)
                {
                    if (memberObj.MTResponse[0].MemberTypeID == "36")
                    {
                        objMemFinInfo.Membership_type_id = memberObj.MTResponse[0].MemberTypeID;
                        objMemFinInfo.Membership_type = "National Enterprise Trial";
                        objMemFinInfo.Monthly_fee = Double.Parse(memberObj.MTResponse[0].MembershipRenewalFee).ToString("####.00");
                        objMemFinInfo.Tops_membership_cost = memberObj.MTResponse[0].MembershipFee == null ? "0" : Double.Parse(memberObj.MTResponse[0].MembershipFee).ToString("####.00");
                        objMemFinInfo.Membership_cost = (Double.Parse(memberObj.MTResponse[0].MembershipFee) + (Double.Parse(memberObj.MTResponse[0].MembershipRenewalFee) * int.Parse(memberObj.MTResponse[0].Term))).ToString("####.00");
                    }
                }
                else if (cbxDiscover180.Checked == true)
                {
                    if (memberObj.MTResponse[0].MemberTypeID == "42")
                    {
                        objMemFinInfo.Membership_type_id = memberObj.MTResponse[0].MemberTypeID;
                        objMemFinInfo.Membership_type = "Discover 180";
                        objMemFinInfo.Monthly_fee = Double.Parse(memberObj.MTResponse[0].MembershipRenewalFee).ToString("####.00");
                        objMemFinInfo.Tops_membership_cost = memberObj.MTResponse[0].MembershipFee == null ? "0" : Double.Parse(memberObj.MTResponse[0].MembershipFee).ToString("####.00");
                        objMemFinInfo.Membership_cost = (Double.Parse(memberObj.MTResponse[0].MembershipFee) + (Double.Parse(memberObj.MTResponse[0].MembershipRenewalFee) * int.Parse(memberObj.MTResponse[0].Term))).ToString("####.00");
                    }

                }
                else if (cbxNationalEntSigCAN.Checked == true)
                {
                    if (memberObj.MTResponse[0].MemberTypeID == "34")
                    {
                        objMemFinInfo.Membership_type_id = memberObj.MTResponse[0].MemberTypeID;
                        objMemFinInfo.Membership_type = "National Enterprise Signature-Canada";
                        objMemFinInfo.Monthly_fee = Double.Parse(memberObj.MTResponse[0].MembershipRenewalFee).ToString("####.00");
                        objMemFinInfo.Tops_membership_cost = memberObj.MTResponse[0].MembershipFee == null ? "0" : Double.Parse(memberObj.MTResponse[0].MembershipFee).ToString("####.00");
                        objMemFinInfo.Membership_cost = (Double.Parse(memberObj.MTResponse[0].MembershipFee) + (Double.Parse(memberObj.MTResponse[0].MembershipRenewalFee) * int.Parse(memberObj.MTResponse[0].Term))).ToString("####.00");
                    }

                }

                //objMemFinInfo.Monthly_fee = objMemFinInfo.Monthly_fee.Remove(objMemFinInfo.Monthly_fee.Length - 2, 2);
                //objMemFinInfo.Tops_membership_cost = objMemFinInfo.Tops_membership_cost.Remove(objMemFinInfo.Tops_membership_cost.Length - 2, 2);
                //objMemFinInfo.Membership_cost = objMemFinInfo.Membership_cost.Remove(objMemFinInfo.Membership_cost.Length - 2, 2);

                objMemFinInfo.Down_payment = Double.Parse(downpayment.Value).ToString("####.00");
                string promoVal = promo.Value == "" ? "0" : promo.Value;
                objMemFinInfo.Membership_cost = (Double.Parse(objMemFinInfo.Membership_cost) - double.Parse(promoVal)).ToString("####.00");
                objMemFinInfo.TguideText = ":Check to acknowledge you have received and agree to the Travel Guide.";

                //collect user entered information for json creation
                clsMember objMember = new clsMember();
                
                objMember.FirstName = fname.Text.Trim();
                objMember.LastName = lname.Text.Trim();
                objMember.EMail = email1.Text.Trim();
                objMember.AddressLine1 = address1.Text.Trim();
                objMember.AddressLine2 = address2.Text.Trim();
                objMember.City = city1.Text.Trim();
                objMember.State = ddlState.SelectedItem.Value;
                objMember.Country = ddlCountry.SelectedItem.Value;
                objMember.Zip = zip.Text.Trim();
                objMember.Phone = phone1.Text.Replace("(", "").Replace(".", "").Replace("-", "").Replace(")", "");
                objMember.LeadID = memberObj.LeadID;
                objMember.LeadStatus = "";
                objMember.MemberID = "";
                objMember.SecondaryFirstName = "";
                objMember.SecondaryLastName = "";
                objMember.SecondaryEmail = "";
                objMember.ContractSessionKey = ContractSessionKey;
                objMember.TrackingID = memberObj.TrackingID;

                memberObj.ContractSessionKey = objMember.ContractSessionKey;
                Session.Add("memberObj", memberObj);

                //save ContractSessionKey in database user activity tracker table for sending user directly to payment gateway 
                // if user has signed the agreement and pressed the refresh   
                // myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "ContractSessionKey", "", ContractSessionKey, "", DateTime.Now); 
                // ViewState.Add("ContractSessionKey", ContractSessionKey);
                
                ContractInfoResponse objContractInfoResponse = new ContractInfoResponse();
                objContractInfoResponse = GetContractInfo(ddlCountry.SelectedValue);

                // Generate JSON for agreement
                string jsonForAgreement = myHelp.BuildJasonForAgreement(objContractInfoResponse, objMemFinInfo, objMember, inClubSales);

                if (!myHelp.IsValidJson(jsonForAgreement))
                {
                    //Log information if joson creation failed
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "JSONforAgreement", "Failure", jsonForAgreement, "Multiple Objects sent to local function to generate JSON", DateTime.Now);
                    throw new HttpUnhandledException("Invalid JSON String");
                }
                else
                {
                    //Log information if joson creation success
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "JSONforAgreement", "Success", jsonForAgreement, "Multiple Objects sent to local function to generate JSON", DateTime.Now);
                }

                //generate eco sing oauth refresh token , we need to place token in header of each subsequent call to the ecosign              
                string tokenResposneJSON = myHelp.getEcoSignOAUTHRefreshToken();
                ecoSighTokenRefreshSuccessResponse objTokenRefSuccessResponse = new ecoSighTokenRefreshSuccessResponse();
                ecoSighTokenRefreshFailureResponse objTokenRefFailureResponse = new ecoSighTokenRefreshFailureResponse();
                if (!tokenResposneJSON.Contains("invalid_request"))
                {
                    objTokenRefSuccessResponse = myHelp.deserializeTokenRefreshSuccessResponse(tokenResposneJSON);

                    //Log if EcoSign Token created successfully
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "EcoSignToken", "Success", objTokenRefSuccessResponse.access_token, " ", DateTime.Now);
                    //  ViewState.Add("EcoSignToken", objTokenRefSuccessResponse.access_token);
                }
                else
                {
                    objTokenRefFailureResponse = myHelp.deserializeTokenRefreshFailureResponse(tokenResposneJSON);

                    //Log if EcoSign Token creation failed
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "EcoSignToken", "Success", objTokenRefFailureResponse.error_description, " ", DateTime.Now);
                    throw new HttpUnhandledException("Adobe EcoSign OAUTH Token refresh failure");
                }

                               
                // if membership is FTO then we need to skip the EcoSign document creation step. 
                string agreementid = string.Empty;
                if (!TrackingIDS_SkipDocumentSign.Contains(memberObj.TrackingID))
                {               
                    //generate ecosign agreement and receive agreement id if succesful and recieve error code if failure                 
                    string ecoSignAgreementResponseJSON = myHelp.getEcoSignAgreement(jsonForAgreement, objTokenRefSuccessResponse.access_token);
                    ecoSignCreateAgreementSuccessResponse objAgreemntSuccessRes = new ecoSignCreateAgreementSuccessResponse();
                    ecoSignCreateAgreementFailureResponse objAgreemntFailureRes = new ecoSignCreateAgreementFailureResponse();
                    if (!(ecoSignAgreementResponseJSON.Contains("code") || ecoSignAgreementResponseJSON.Contains("message")))
                    {
                        // recieve the json string from successfull eco sign agreement creation call
                        objAgreemntSuccessRes = myHelp.deserializeCreateAgreementSuccessResponse(ecoSignAgreementResponseJSON);

                        //Log if agreement id created successfully
                        myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "AgreementID", "Success", objAgreemntSuccessRes.agreementId, "JSONForAgreement & AccessToken are input", DateTime.Now);
                    }
                    else
                    {
                        // recieve the json string from failed eco sign agreement creation call
                        objAgreemntFailureRes = myHelp.deserializeCreateAgreementFailureResponse(ecoSignAgreementResponseJSON);

                        //Log if agreement id creation failed
                        myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "AgreementID", "Failure", objAgreemntFailureRes.message, "JSONForAgreement & AccessToken are input", DateTime.Now);
                        throw new HttpUnhandledException("Adobe EcoSign Agreement Generation Failure");
                    }
                                      
                    agreementid = objAgreemntSuccessRes.agreementId;
                }
                else
                {
                    agreementid = "None";
                }
             


                Thread.Sleep(1000);
                //WSOnlineinfo Values
                myWSinfo.AgreementID = agreementid;  //objAgreemntSuccessRes.agreementId;
                myWSinfo.ContractSession = ContractSessionKey;
                myWSinfo.EnrollmentFee = objMemFinInfo.Down_payment;
                myWSinfo.MonthlyFee = objMemFinInfo.Monthly_fee;
                myWSinfo.LeadIDType = "dbMi";
                myWSinfo.AdditionalValue01 = PromoID.Text;
                myWSinfo.AdditionalValue02 = objMemFinInfo.Membership_cost;

                //Create membership in TOPS
                //CreateMemberInTOPS(objAgreemntSuccessRes.agreementId, ContractSessionKey, objMemFinInfo.Membership_type_id, objMemFinInfo.Tops_membership_cost, objMemFinInfo.Monthly_fee, objTokenRefSuccessResponse.access_token);
                CreateMemberInTOPS(agreementid, ContractSessionKey, objMemFinInfo.Membership_type_id, objMemFinInfo.Tops_membership_cost, objMemFinInfo.Monthly_fee, objTokenRefSuccessResponse.access_token);



                //if membership requires to skip the EcoSign contact sign then we dont need to have the EcoSign URL
                ecoSignGETAgreemntURLSuccessResponse objAgURLSuccessResponse = new ecoSignGETAgreemntURLSuccessResponse();
                ecoSignGETAgreemntURLFailureResponse objAgURLFailureResponse = new ecoSignGETAgreemntURLFailureResponse();
                if (!TrackingIDS_SkipDocumentSign.Contains(memberObj.TrackingID))
                {
                    // agreement sign URL                
                    //string ecoSignURLResponseJSON = myHelp.getEcoSignAgreementURL(objAgreemntSuccessRes.agreementId, objTokenRefSuccessResponse.access_token);
                    string ecoSignURLResponseJSON = myHelp.getEcoSignAgreementURL(agreementid, objTokenRefSuccessResponse.access_token);
                   

                    if (!(ecoSignURLResponseJSON.Contains("code") || ecoSignURLResponseJSON.Contains("message")))
                    {
                        // recieve the json string from successful eco sign agreement URL creation call
                        objAgURLSuccessResponse = myHelp.deserializeAgreementURLSuccessResponse(ecoSignURLResponseJSON);

                        //Log if Agreement URL is fetched successfully
                        myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "AgreementURL", "Success", objAgURLSuccessResponse.signingUrls[0].esignUrl, "AgreementID & AccessToken are input", DateTime.Now);
                        //  ViewState.Add("AgreementURL", objAgURLSuccessResponse.signingUrls[0].esignUrl);

                        memberObj.url = objAgURLSuccessResponse.signingUrls[0].esignUrl;
                        Session.Add("memberObj", memberObj);
                    }
                    else
                    {
                        // recieve the json string from failed eco sign agreement URL creation call
                        objAgURLFailureResponse = myHelp.deserializeAgreementURLFailureResponse(ecoSignURLResponseJSON);

                        //Log if Agreement URL is fetching failed
                        myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "AgreementURL", "Failure", objAgURLFailureResponse.message, "AgreementID & AccessToken are input", DateTime.Now);
                        // cancel agreement because agreement id is generated and URL is not fetched 
                        // cancelAgreement(objAgreemntSuccessRes.agreementId, objTokenRefSuccessResponse.access_token);
                        cancelAgreement(agreementid, objTokenRefSuccessResponse.access_token);
                        throw new HttpUnhandledException("Adobe EcoSign Agreement URL Generation Failure");
                    }
                }
                

                //check if manual or automated execution
                string ManualExecution = ConfigurationManager.AppSettings["ManualExecution"].ToString();

                // based on execution mode get the payment processing application URL
                string RedirectURL = string.Empty;
                if (ManualExecution.ToUpper() == "TRUE")
                {
                    RedirectURL = ConfigurationManager.AppSettings["EcoSignRedirectURL_Staging"].ToString();
                    RedirectURL = string.Concat(RedirectURL, "/?", ContractSessionKey);
                }
                else
                {
                    RedirectURL = ConfigurationManager.AppSettings["EcoSignRedirectURL_PROD"].ToString();
                    RedirectURL = string.Concat(RedirectURL, "/?", ContractSessionKey);
                }


                // if membership type requires skipping document sign process then create URL that will either
                // take you directly to the payment page or bypass that also(in case of FTO we dont need payment page) 
                if (TrackingIDS_SkipDocumentSign.Contains(memberObj.TrackingID)) 
                {
                    if (TrackingIDS_SkipPaymentProcessing.Contains(memberObj.TrackingID))
                    {                       
                        //process id = 1 will instruct payment gateway application to show credentials directly
                        ClientScript.RegisterStartupScript(this.GetType(), "openColorBox", "openColorBox('" + RedirectURL + "&ProcessId=1');", true);
                    }
                    else
                    {
                        memberObj.Contract_Signed = "NO";
                        ClientScript.RegisterStartupScript(this.GetType(), "openColorBox", "openColorBox('" + RedirectURL + "');", true);
                    }
                }
                else //  show the pdf documentn
                {
                    memberObj.Contract_Signed = "YES";
                    ClientScript.RegisterStartupScript(this.GetType(), "openColorBox", "openColorBox('" + objAgURLSuccessResponse.signingUrls[0].esignUrl + "');", true);
                }
            }
            catch (HttpUnhandledException ex)
            {
                myHelp.WriteLogToDB("Error", "HomePage --> CreateEchosignAgreement()", "", ex.Message, DateTime.Now);
                Server.Transfer("ErrorPage.aspx");
            }
            catch (Exception ex)
            {
                myHelp.WriteLogToDB("Error", "HomePage --> CreateEchosignAgreement()", "", ex.Message, DateTime.Now);
                throw;
            }
            finally
            {
                Session.Add("memberObj", memberObj);
                myHelp = null;
            }
        }

        internal void CreateMemberInTOPS(string agID, string contractSessionKey, string memshipType, string retailFee, string monthFee, string AccessToken)
        {
            Helper myHelp = new Helper();
            string[] returnVars = new string[2];
            string ManualExecution = string.Empty;
            string UserName = string.Empty;
            string Password = string.Empty;
            string OOTUSAClubID = string.Empty;
            string OOTCANClubID = string.Empty;
            string EnterpriseUSAClubID = string.Empty;
            string EnterpriseCANClubID = string.Empty;
            string defaultTrackingID = string.Empty;
            string defaultDirectorID = string.Empty;
            string defaultMembershipTerm = string.Empty;
            string TrackingIDforRefer = string.Empty;
            string referPageDefaultReferCode = string.Empty;
            string promoCode = string.Empty;

            try
            {               

                ManualExecution = ConfigurationManager.AppSettings["ManualExecution"].ToString();
                defaultTrackingID = ConfigurationManager.AppSettings["defaultTrackingID"].ToString();
                defaultDirectorID = ConfigurationManager.AppSettings["defaultDirectorID"].ToString();
                defaultMembershipTerm = ConfigurationManager.AppSettings["defaultMembershipTerm"].ToString();
                TrackingIDforRefer = ConfigurationManager.AppSettings["trackingIDforRefer"].ToString();
                referPageDefaultReferCode = ConfigurationManager.AppSettings["defaultReferCode"].ToString();

                //when we get redirected from refer page, regardless of which member refer to whom, we get a default refer code called "refer"
                //this is true even if valid RCID present on refer page or not
                if (memberObj.TrackingID == TrackingIDforRefer)
                {
                    promoCode = referPageDefaultReferCode;
                }
                else
                {
                    promoCode = PromoID.Text;
                }

                if (ManualExecution.ToUpper() == "TRUE")
                {
                    UserName = ConfigurationManager.AppSettings["TOPSUserID_Stage"].ToString();
                    Password = ConfigurationManager.AppSettings["TOPSPwd_Stage"].ToString();
                }
                else
                {
                    UserName = ConfigurationManager.AppSettings["TOPSUserID_Prod"].ToString();
                    Password = ConfigurationManager.AppSettings["TOPSPwd_Prod"].ToString();
                }

                OOTUSAClubID = ConfigurationManager.AppSettings["OOT_USAClubID"].ToString();
                OOTCANClubID = ConfigurationManager.AppSettings["OOT_CANClubID"].ToString();
                EnterpriseUSAClubID = ConfigurationManager.AppSettings["EnterpriseUSAClubID"].ToString();
                EnterpriseCANClubID = ConfigurationManager.AppSettings["EnterpriseCANClubID"].ToString();
                string[] EnterpriseTrackingIDs = ConfigurationManager.AppSettings["EnterpriseTrackingIDs"].ToString().Split(',');               

                GetMemberID GetMemberID = new GetMemberID();
                GetMemberID.input = new GetMemberID._Input();
                GetMemberID.input.Login = new GetMemberID._Login();
                GetMemberID.input.Login.UserID = UserName;
                GetMemberID.input.Login.Password = Password;
                GetMemberID.input.GoToStatus = "0";

                GetMemberID.input.MemberInfo = new GetMemberID._MemberInfo();

                //if tracking id belongs to enterprise the then we have to change the center ids 
                // US = 927,  CANADA = 937
                if (EnterpriseTrackingIDs.Contains(memberObj.TrackingID))
                {
                    GetMemberID.input.MemberInfo.CenterID = ddlCountry.SelectedValue == "US" ? EnterpriseUSAClubID : EnterpriseCANClubID;
                }
                else
                {
                    GetMemberID.input.MemberInfo.CenterID = ddlCountry.SelectedValue == "US" ? OOTUSAClubID : OOTCANClubID;
                }
               
                GetMemberID.input.MemberInfo.BionicLeadID = memberObj.LeadID;
                GetMemberID.input.MemberInfo.ApplicationDate = myHelp.getCurrentDateJSON(); //supported format : "2016-10-18T05:00:00.000Z "; //json converted date
                GetMemberID.input.MemberInfo.DriverLicenseState = ddlState.SelectedItem.Value;
                GetMemberID.input.MemberInfo.PrimaryFirstName = fname.Text.Trim();
                GetMemberID.input.MemberInfo.PrimaryLastName = lname.Text.Trim();
                GetMemberID.input.MemberInfo.PrimaryStreet1 = address1.Text.Trim();
                GetMemberID.input.MemberInfo.PrimaryCity = city1.Text.Trim();
                GetMemberID.input.MemberInfo.PrimaryState = ddlState.SelectedItem.Value;
                GetMemberID.input.MemberInfo.PrimaryZip = zip.Text.Trim();
                GetMemberID.input.MemberInfo.PrimaryCountry = ddlCountry.SelectedItem.Value;
                GetMemberID.input.MemberInfo.PrimaryEmail = email1.Text.Trim();
                GetMemberID.input.MemberInfo.PrimaryPhone = phone1.Text.Trim();
                GetMemberID.input.MemberInfo.PrimaryEducation = "0";
                GetMemberID.input.MemberInfo.SecondaryFirstName = spousefname.Text.Trim();
                GetMemberID.input.MemberInfo.SecondaryLastName = spouselname.Text.Trim();
                GetMemberID.input.MemberInfo.SecondaryEmail = spouseemail.Text.Trim();
                GetMemberID.input.MemberInfo.SecondaryEducation = "0";
                GetMemberID.input.MemberInfo.DirectorID = defaultDirectorID;

                GetMemberID.input.Membership = new GetMemberID._Membership();
                GetMemberID.input.Membership.RetailMembershipFee = Double.Parse(retailFee).ToString("####.00");
                GetMemberID.input.Membership.ActualMembershipFee = Double.Parse(downpayment.Value).ToString("####.00");
                GetMemberID.input.Membership.FinanceOption = "6";
                GetMemberID.input.Membership.MembershipType = memshipType;
                //The membership term is driven by TOPS webservice, we were sending static term as 12 months to TOPS via ION, 
                //now we are using the same webservice so we are sending the same static term to TOPS                 
                GetMemberID.input.Membership.MembershipTerm = defaultMembershipTerm;
                GetMemberID.input.Membership.ContractSignedDate = myHelp.getCurrentDateJSON();
                GetMemberID.input.Membership.ExpirationDate = myHelp.getContractEndDateJSON();
                GetMemberID.input.Membership.TaxExempt = "0";
                GetMemberID.input.Membership.TaxExemptInfo = "Tracking ID:" + defaultTrackingID + ";AgreementID:" + agID + ";LeadID:" + memberObj.LeadID + ";promocode:" + promoCode + "";

                GetMemberID.input.WSOnlineInfo = new GetMemberID._WSOnlineInfo();
                GetMemberID.input.WSOnlineInfo.AgreementID = agID;
                GetMemberID.input.WSOnlineInfo.EnrollmentFee = Double.Parse(downpayment.Value).ToString("####.00"); ;
                GetMemberID.input.WSOnlineInfo.MonthlyFee = Double.Parse(monthFee).ToString("####.00"); ;
                GetMemberID.input.WSOnlineInfo.LeadIDType = "dbMi";
                GetMemberID.input.WSOnlineInfo.ContractSession = contractSessionKey;
                GetMemberID.input.WSOnlineInfo.AdditionalNotes = "";
                GetMemberID.input.WSOnlineInfo.AdditionalValue01 = promoCode;
                GetMemberID.input.WSOnlineInfo.AdditionalValue02 = Double.Parse(actualmembershipfee.Value).ToString("####.00");
                GetMemberID.input.WSOnlineInfo.AdditionalValue03 = memberObj.referByMemberID;
                GetMemberID.input.WSOnlineInfo.AdditionalValue04 = "";
                GetMemberID.input.WSOnlineInfo.AdditionalValue05 = "";

                returnVars = myHelp.CreateMemberInTops(GetMemberID);
                string memberID = returnVars[0];

                if (memberID != null && memberID != "")
                {
                    //L O G G the successful member creation attempt into user activity tracker table
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "CreateMemberInTOPS", "Success", "Member ID : " + memberID, returnVars[1], DateTime.Now);
                    memberObj.MemberID = memberID;
                }
                else
                {
                    //L O G G the failed member creation attempt into user activity tracker table
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "CreateMemberInTOPS", "Failure", "", returnVars[1], DateTime.Now);
                    throw new HttpUnhandledException("Member Creation failed");
                }
            }
            catch (HttpUnhandledException ex)
            {
                myHelp.WriteLogToDB("Error", "HomePage --> CreateMemberInTOPS()", "", ex.Message, DateTime.Now);
                cancelAgreement(agID, AccessToken);
                Server.Transfer("ErrorPage.aspx");
            }
            catch (Exception ex)
            {
                myHelp.WriteLogToDB("Error", "HomePage -> CreateMemberInTOPS()", "", ex.Message, DateTime.Now);
                cancelAgreement(agID, AccessToken);
                throw;
            }
            finally
            {
                myHelp = null;
            }
        }

        internal void UpdateLeadWithMemberID()
        {
            memberObj = (clsMember)Session["memberObj"];
            if (memberObj.LeadID != null)
            {
                Helper myHelp = new Helper();
                string dataSent, dataReceived = string.Empty;
                string leadStatus = "";
                SqlDataReader myRdr;
                try
                {
                    // get lead status to be updated from database based on member id
                    myRdr = myHelp.executeSPGetDR("GetLeadStatusByMembershipTypeID", "@MembershipID", memberObj.MemberID);
                    if (myRdr.HasRows)
                    {
                        while (myRdr.Read())
                        {
                            leadStatus = myRdr["leadStatus"].ToString();
                        }
                    }
                    else
                    { 
                        leadStatus = "60";
                    }

                    clsUpdateLeadParams myLead = new clsUpdateLeadParams();
                    myLead.leads_id = memberObj.LeadID;
                    myLead.first_name = "";
                    myLead.last_name = "";
                    myLead.email = "";
                    myLead.spouse_first_name = "";
                    myLead.spouse_last_name = "";
                    myLead.address1 = "";
                    myLead.address2 = "";
                    myLead.city = "";
                    myLead.postal_code = "";
                    myLead.state = "";
                    myLead.country = "";
                    myLead.primary_phone = "";
                    myLead.primary_phone_type = "";
                    myLead.secondary_phone = "";
                    myLead.secondary_phone_type = "";
                    myLead.member_id = memberObj.MemberID;
                    myLead.lead_status = leadStatus;
                    myLead.notes = memberObj.ContractSessionKey;     // enter contract session key in notes field as this also is used in email campaigns
                    myLead.url = memberObj.url;
                    myLead.Contract_Signed = memberObj.Contract_Signed;
                    myLead.bionic_status = "1";    //we dont want any sales lead to be posted to bionic, if this status is 1 then leads will not go to bionic

                    updateLeadResponse objUpdateLead = new updateLeadResponse();
                    objUpdateLead = myHelp.MkAPICall_UpdateLeadInfo(myLead);
                    dataSent = "LeadID : " + memberObj.LeadID + " & MemberID : " + memberObj.MemberID;
                    dataReceived = "response_message : " + objUpdateLead.response_message + " & response_message_variables : " + objUpdateLead.response_message_variables;
                    if (objUpdateLead.response_status == "success")
                    {
                        //L O G G the successful member id updation attempt into user activity tracker table
                        myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "Update Lead in DBMI - MemberID", "Success", dataReceived, dataSent, DateTime.Now);
                    }
                    else
                    {
                        //L O G G the Failed member creation attempt into user activity tracker table
                        myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "Update Lead in DBMI - MemberID", "Failure", dataReceived, dataSent, DateTime.Now);
                        throw new HttpUnhandledException("Update Lead failed");
                    }
                }
                catch (Exception ex)
                {
                    myHelp.WriteLogToDB("Error", "HomePage --> UpdateLead()", "", ex.Message, DateTime.Now);
                    throw;
                }
                finally
                {
                    myHelp = null;
                }
            }
        }

        internal ContractInfoResponse GetContractInfo(string country)
        {
            Helper myHelp = new Helper();
            string dataSent, dataReceived = string.Empty;
            try
            {
                clsGetContractInfoParams myContract = new clsGetContractInfoParams();
                myContract.state_id = ddlState.SelectedItem.Value;

                ContractInfoResponse objcontractInfo = new ContractInfoResponse();
                string contractInfoJSON = myHelp.MkAPICall_GetContractInfo(myContract);
                dataSent = "StateID: " + ddlState.SelectedItem.Value;
                dataReceived = contractInfoJSON;

                ContractInfoResponse objContractInfo = new ContractInfoResponse();
                objContractInfo = myHelp.deserializeContractInfoResponse(contractInfoJSON);

                //contract info API will always sent back state_active as 'N' for canada. we have to by pass if country is canada
                if (country != "CA" && objContractInfo.contract_information.state_abbreviation == "ZZ")  // failed
                {
                    //L O G G the Failed attempt get contract information from DBMI
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "Get Contract info from DBMI", "Failure", dataReceived, dataSent, DateTime.Now);
                    throw new HttpUnhandledException("Get Contract Information Failed");
                }
                else //success
                {
                    //L O G G the successful attempt get contract information from DBMI
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "Get Contract info from DBMI", "Success", dataReceived, dataSent, DateTime.Now);
                }
                return objContractInfo;
            }
            catch (Exception ex)
            {
                myHelp.WriteLogToDB("Error", "HomePage --> GetContractInfo()", "", ex.Message, DateTime.Now);
                throw;
            }
            finally
            {
                myHelp = null;
            }
        }

        public void cancelAgreement(string AgreementID, string AccessToken)
        {
            Helper myHelp = new Helper();
            string cancelAgreementResponse = string.Empty;
            ecoSignCancelagreemntSuccessResponse canAgSuccessResponse = new ecoSignCancelagreemntSuccessResponse();
            ecoSignCancelagreemntFailureResponse canAgFailureResponse = new ecoSignCancelagreemntFailureResponse();
            try
            {
                cancelAgreementResponse = myHelp.cancelAgreement(AgreementID, AccessToken, myHelp.BuildJsonForCancelAgreement());
                if (!(cancelAgreementResponse.Contains("code") || cancelAgreementResponse.Contains("message")))
                {
                    // recieve the json string from successful eco sign agreement creation call
                    canAgSuccessResponse = myHelp.deserializeCancelAgreementSuccessResponse(cancelAgreementResponse);

                    //L O G G the newly generated agreementID into user activity tracker table
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "CancelAgreement", "Success", canAgSuccessResponse.result, "AgreementID is the input", DateTime.Now);
                }
                else
                {
                    // recieve the json string from failed eco sign agreement creation call
                    canAgFailureResponse = myHelp.deserializeCancelAgreementFailureResponse(cancelAgreementResponse);

                    //L O G G the failed agreementID generation attempt into user activity tracker table
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "CancelAgreement", "Failure", canAgFailureResponse.message, "AgreementID & AccessToken are input", DateTime.Now);
                }
            }
            catch (Exception ex)
            {
                myHelp.WriteLogToDB("Error", "HomePage --> cancelAgreement()", "", ex.Message, DateTime.Now);
                throw;
            }
            finally
            {
                myHelp = null;
            }
        }

        protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            Helper myHelp = new Helper();
            cbxPrem.Checked = false;
            cbxSig.Checked = false;
            try
            {
                string CountryCode = string.Empty;
                if (ddlCountry.SelectedValue == "US")
                {
                    CountryCode = "USA";
                    getMemberShipTypes(CountryCode);
                }
                else if (ddlCountry.SelectedValue == "CA")
                {
                    CountryCode = "CAN";
                    getMemberShipTypes(CountryCode);
                }
                populateStates(CountryCode);
            }
            catch (Exception ex)
            {
                myHelp.WriteLogToDB("Error", "HomePage --> ddlCountry_SelectedIndexChanged()", "", ex.Message, DateTime.Now);
                throw;
            }
            finally
            {
                myHelp = null;
            }
        }

        internal string getAgreementStatus(string AgreementID, string accessToken)
        {
            Helper myHelp = new Helper();
            string AgreementStatusResponseJSON = string.Empty;
            string result = string.Empty;
            try
            {
                ecoSignAgreementStatusSuccessResponse agreementStatusSuccessResponse = new ecoSignAgreementStatusSuccessResponse();
                ecoSignAgreementStatusFailureResponse agreementStatusFailResponse = new ecoSignAgreementStatusFailureResponse();
                AgreementStatusResponseJSON = myHelp.getAgreementStatus(AgreementID, accessToken);

                if (!(AgreementStatusResponseJSON.Contains("NO_ACCESS_TOKEN_HEADER") || AgreementStatusResponseJSON.Contains("INVALID_ACCESS_TOKEN") || AgreementStatusResponseJSON.Contains("PERMISSION_DENIED") || AgreementStatusResponseJSON.Contains("INVALID_AGREEMENT_ID")))
                {
                    // recieve the json string from successful eco sign agreement creation call
                    agreementStatusSuccessResponse = myHelp.deserializeEcoSignAgreementStatusSuccessResponse(AgreementStatusResponseJSON);

                    //L O G G the get agreement status success response to user activity tracker table
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, "", "Get Agreement Status", "Success", AgreementStatusResponseJSON, AgreementID, DateTime.Now);

                    result = agreementStatusSuccessResponse.status;
                }
                else
                {
                    // recieve the json string from failed eco sign agreement creation call
                    agreementStatusFailResponse = myHelp.deserializeEcoSignAgreementStatusFailureResponse(AgreementStatusResponseJSON);

                    //L O G G the failed get agreement status response into user activity tracker table
                    myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, "", "Get Agreement Status", "Failure", AgreementStatusResponseJSON, AgreementID, DateTime.Now);

                    result = agreementStatusFailResponse.message;
                }
                return result;
            }
            catch (Exception ex)
            {
                myHelp.WriteLogToDB("Error", "HomePage --> getAgreementStatus", "", ex.Message, DateTime.Now);
                throw;
            }
            finally
            {
                myHelp = null;
            }

        }

        // this update lead funciton is used when we prepopulate the lead ddata and update the same based on lead id
        //s o we dont need to provide contract session id where as , update lead with memberid will be called later and that will update the contract session key
        internal void UpdateLead()
        {
            ExistingLead = (Lead)Session["existingLead"];

            //populate the user entered and updated data into memberobject so that we can send the same to the lead update API
            memberObj = (clsMember)Session["memberObj"];
            memberObj.FirstName = fname.Text.Trim();
            memberObj.LastName = lname.Text.Trim();
            memberObj.EMail = email1.Text.Trim();
            memberObj.AddressLine1 = address1.Text.Trim();
            memberObj.AddressLine2 = address2.Text.Trim();
            memberObj.City = city1.Text.Trim();
            memberObj.State = ddlState.SelectedValue;
            memberObj.Country = ddlCountry.SelectedValue;
            memberObj.Zip = zip.Text.Trim();

            if (ExistingLead.LeadID != null)
            {
                Helper myHelp = new Helper();
                string dataSent, dataReceived = string.Empty;
                try
                {
                    clsUpdateLeadParams myLead = new clsUpdateLeadParams();
                    //myLead.leads_id = ExistingLead.LeadID;
                    //myLead.first_name = ExistingLead.FirstName;
                    //myLead.last_name = ExistingLead.LastName;
                    //myLead.email = ExistingLead.Email;
                    //myLead.spouse_first_name = "";
                    //myLead.spouse_last_name = "";
                    //myLead.address1 = ExistingLead.Street1;
                    //myLead.address2 = ExistingLead.Street2;
                    //myLead.city = ExistingLead.City;
                    //myLead.postal_code = ExistingLead.PostalCode;
                    //myLead.state = ExistingLead.State;
                    //myLead.country = ExistingLead.Country;
                    //myLead.primary_phone = ExistingLead.PrimaryPhone;
                    //myLead.primary_phone_type = "";
                    //myLead.secondary_phone = "";
                    //myLead.secondary_phone_type = "";
                    //myLead.member_id = "";


                    myLead.leads_id = ExistingLead.LeadID;
                    // myLead.first_name = "";
                    myLead.last_name = memberObj.LastName;
                    // myLead.email = ExistingLead.Email;
                    myLead.spouse_first_name = "";
                    myLead.spouse_last_name = "";
                    myLead.address1 = memberObj.AddressLine1;
                    myLead.address2 = memberObj.AddressLine2;
                    myLead.city = memberObj.City;
                    myLead.postal_code = memberObj.Zip;
                    myLead.state = memberObj.State;
                    myLead.country = memberObj.Country;
                    myLead.primary_phone = memberObj.Phone;
                    myLead.primary_phone_type = "";
                    myLead.secondary_phone = "";
                    myLead.secondary_phone_type = "";
                    myLead.member_id = "";

                    updateLeadResponse objUpdateLead = new updateLeadResponse();
                    objUpdateLead = myHelp.MkAPICall_UpdateLeadInfo(myLead);
                    dataSent = "LeadID : " + memberObj.LeadID;
                    dataReceived = "response_message : " + objUpdateLead.response_message + " & response_message_variables : " + objUpdateLead.response_message_variables;
                    if (objUpdateLead.response_status == "success")
                    {
                        //L O G G the successful member id updation attempt into user activity tracker table
                        myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "Update Lead in DBMI - Using LeadID from QueryString", "Success", dataReceived, dataSent, DateTime.Now);
                    }
                    else
                    {
                        //L O G G the Failed member creation attempt into user activity tracker table
                        myHelp.WriteUserActitytoDB(HttpContext.Current.Session.SessionID, memberObj.TrackingID, "Update Lead in DBMI - Using LeadID from QueryString", "Failure", dataReceived, dataSent, DateTime.Now);
                        throw new HttpUnhandledException("Update Lead failed");
                    }
                }
                catch (Exception ex)
                {
                    myHelp.WriteLogToDB("Error", "HomePage --> UpdateLead()", "", ex.Message, DateTime.Now);
                    throw;
                }
                finally
                {
                    myHelp = null;
                }
            }
        }

        #region "To be used"

        //public FetchedLeadResponse GetFetchedLeadData()
        //{
        //    Helper myHelp = new Helper();

        //   clsGetFetchedLeadDataParams myLeadData = new clsGetFetchedLeadDataParams();
        //    myLeadData.phone = "2195886889";
        //    myLeadData.email = "vjindal@directbuy.com";
        //    myLeadData.current_club_id = "";
        //    myLeadData.exclude_membership_purchased = "";

        //    FetchedLeadResponse objFetchedLead = new FetchedLeadResponse();
        //    objFetchedLead = myHelp.MkAPICall_GetFetchedLeadData(myLeadData);
        //    myHelp = null;
        //    return objFetchedLead;

        //}

        //public ReferralResponse GetReferralInfo()
        //{
        //    Helper myHelp = new Helper();

        //    clsGetReferralInfoParams referralInfo = new clsGetReferralInfoParams();
        //    referralInfo.referral_code = "13dolphin";

        //    ReferralResponse objRefferal = new ReferralResponse();
        //    objRefferal = myHelp.MkAPICall_GetReferralInfo(referralInfo);
        //    myHelp = null;
        //    return objRefferal;

        //}

        //public ClubInfoResponse GetClubByPostalCode()
        //{
        //    Helper myHelp = new Helper();

        //    clsGetClubByPostalCode myClubData = new clsGetClubByPostalCode();
        //    myClubData.postal_code = "99090";

        //    ClubInfoResponse objClubInfo = new ClubInfoResponse();
        //    objClubInfo = myHelp.MkAPICall_GetClubByPostalCode(myClubData);
        //    myHelp = null;
        //    return objClubInfo;
        //}
        #endregion

    }
}
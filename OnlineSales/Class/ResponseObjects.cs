﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineSales
{

    #region "Directbuy APIs response objects "

    //Promocode response class 
    public class PromoCodeResponse
    {
        public string response_status { get; set; }
        public string response_message { get; set; }
        public string response_message_variables { get; set; }
        public Promo_Code_Data promo_code_data { get; set; }
    }

    public class Promo_Code_Data
    {
        public string promo_code_id { get; set; }
        public string promo_code { get; set; }
        public string promo_description { get; set; }
        public string promo_type { get; set; }
        public string promo_value { get; set; }
        public string promo_value_type { get; set; }
        public string processed_value { get; set; }
        public string promo_code_status { get; set; }
        public string additional_credit { get; set; }
        public string enterprise_club_id { get; set; }
        public string enterprise_email { get; set; }
        public string enterprise_phone { get; set; }
    }


    // Post Data Response
    public class PostDataResponse
    {
        public string response_status { get; set; }
        public string response_message { get; set; }
        public string response_message_variables { get; set; }
    }

    // Contract Information Response from DBMI , when you send an state code for ex 'IN' 
    public class ContractInfoResponse
    {
        public string response_status { get; set; }
        public string response_message { get; set; }
        public string response_message_variables { get; set; }
        public Contract_Information contract_information { get; set; }
    }

    public class Contract_Information
    {
        public string state_abbreviation { get; set; }
        public string state_name { get; set; }
        public string cancel_notice { get; set; }
        public string section_j { get; set; }
        public string use_alt_section_c { get; set; }
        public string cancel_text { get; set; }
        public string notice_format { get; set; }
        public string state_active { get; set; }
        public string cancel_format { get; set; }
        public string section_jend { get; set; }
        public string section_j_format { get; set; }
        public string section_jend_format { get; set; }
        public string section_k { get; set; }
        public string section_k_format { get; set; }
        public string organization_name { get; set; }
    }

    // fetch lead data based on lead id by phone and email id
    public class FetchedLeadResponse
    {
        public string response_status { get; set; }
        public string response_message { get; set; }
        public string response_message_variables { get; set; }
        public Lead_Data lead_data { get; set; }
    }

    public class Lead_Data
    {
        public string lead_id { get; set; }
        public string clubs_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string postal_code { get; set; }
        public string email { get; set; }
        public string primary_phone { get; set; }
        public string primary_phone_type { get; set; }
        public string secondary_phone { get; set; }
        public string secondary_phone_type { get; set; }
        public object member_id { get; set; }
        public string appointment_date_time { get; set; }
    }

    // Referral Response
    public class ReferralResponse
    {
        public string response_status { get; set; }
        public string response_message { get; set; }
        public string response_message_variables { get; set; }
        public Referral_Information referral_information { get; set; }
    }

    public class Referral_Information
    {
        public string referral_code { get; set; }
        public string member_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string display_name { get; set; }
    }

    // update lead response    
    public class updateLeadResponse
    {
        public string response_status { get; set; }
        public string response_message { get; set; }
        public string response_message_variables { get; set; }
    }

    // club information response based on zip code
    public class ClubInfoResponse
    {
        public string response_status { get; set; }
        public string response_message { get; set; }
        public string response_message_variables { get; set; }
        public string clubs_id { get; set; }
        public Club_Information club_information { get; set; }
    }

    public class Club_Information
    {
        public string id { get; set; }
        public string name { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postal_code { get; set; }
        public string country { get; set; }
        public string phone1 { get; set; }
        public object phone2 { get; set; }
        public object fax { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string email { get; set; }
        public string hubspot_email { get; set; }
        public string last_updated_date { get; set; }
        public string contact_name { get; set; }
        public string current_club_id { get; set; }
        public string warehouse_address1 { get; set; }
        public object warehouse_address2 { get; set; }
        public string warehouse_city { get; set; }
        public string warehouse_state { get; set; }
        public string warehouse_zip { get; set; }
        public string warehouse_country { get; set; }
        public string warehouse_phone { get; set; }
        public string converted_club_code { get; set; }
        public string driving_directions { get; set; }
        public string showroom_hours { get; set; }
        public string warehouse_hours { get; set; }
        public string corporate { get; set; }
        public string test { get; set; }
        public string service_only { get; set; }
        public string home_shopping { get; set; }
        public string club_url { get; set; }
        public object enterprise_email { get; set; }
        public object enterprise_phone { get; set; }
    }

    #endregion

    #region "Eco sign response objects"

    // oauth token refresh success response
    public class ecoSighTokenRefreshSuccessResponse
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
    }

    // oauth token refresh failure response
    public class ecoSighTokenRefreshFailureResponse
    {
        public string error_description { get; set; }
        public string error { get; set; }
    }



    // eco sign agreement Success response
    public class ecoSignCreateAgreementSuccessResponse
    {
        public string agreementId { get; set; }
    }

    // eco sign agreement Failure response
    public class ecoSignCreateAgreementFailureResponse
    {
        public string code { get; set; }
        public string message { get; set; }
    }



    // get ecosign agreement URL success response
    public class ecoSignGETAgreemntURLSuccessResponse
    {
        public Signingurl[] signingUrls { get; set; }
    }

    // get ecosign agreement URL Failure response
    public class ecoSignGETAgreemntURLFailureResponse
    {
        public string code { get; set; }
        public string message { get; set; }
    }

    public class Signingurl
    {
        public string email { get; set; }
        public string esignUrl { get; set; }
    }

    // cancel agreement success Response
    public class ecoSignCancelagreemntSuccessResponse
    {
        public string result { get; set; }
    }

    // cancel agreement Response
    public class ecoSignCancelagreemntFailureResponse
    {
        public string code { get; set; }
        public string message { get; set; }
    }

 
    // get agreement successs status using agreement ID
     // start class

        public class ecoSignAgreementStatusSuccessResponse
        {
            public Event[] events { get; set; }
            public string latestVersionId { get; set; }
            public string locale { get; set; }
            public string message { get; set; }
            public string name { get; set; }
            public Participantsetinfo[] participantSetInfos { get; set; }
            public string status { get; set; }
            public string agreementId { get; set; }
            public bool modifiable { get; set; }
            public Nextparticipantsetinfo[] nextParticipantSetInfos { get; set; }
            public bool vaultingEnabled { get; set; }
        }

        public class Event
        {
            public string actingUserEmail { get; set; }
            public string actingUserIpAddress { get; set; }
            public DateTime date { get; set; }
            public string description { get; set; }
            public string participantEmail { get; set; }
            public string type { get; set; }
            public string versionId { get; set; }
            public string comment { get; set; }
        }

        public class Participantsetinfo
        {
            public string participantSetId { get; set; }
            public Participantsetmemberinfo[] participantSetMemberInfos { get; set; }
            public string[] roles { get; set; }
            public int signingOrder { get; set; }
            public string status { get; set; }
        }

        public class Participantsetmemberinfo
        {
            public string email { get; set; }
            public string name { get; set; }
            public string participantId { get; set; }
            public string company { get; set; }
            public string title { get; set; }
        }

        public class Nextparticipantsetinfo
        {
            public Nextparticipantsetmemberinfo[] nextParticipantSetMemberInfos { get; set; }
        }

        public class Nextparticipantsetmemberinfo
        {
            public string email { get; set; }
            public string name { get; set; }
            public DateTime waitingSince { get; set; }
        }

    // end class

        // get agreement failure status response
        public class ecoSignAgreementStatusFailureResponse
        {
            public string code { get; set; }
            public string message { get; set; }
        }

    #endregion

    #region "Online Sales Service Responses"

    public class MembershipTypeResposne
    {
        public string id { get; set; }
        public string LeadSource { get; set; }
        public string TrackingID { get; set; }
        public string MemberTypeID { get; set; }
        public string MemberTypeDesc { get; set; }
        public string MembershipFee { get; set; }
        public string Term { get; set; }
        public string MembershipRenewalFee { get; set; }
    }

    #endregion


}
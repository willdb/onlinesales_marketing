﻿jQuery(document).ready(function ($) {
    $("#ApplyButton").attr("disabled", "disabled");
    $("#ContractBtn").attr("disabled", "disabled");
    $("#spousefname").attr("disabled", "disabled");
    $("#spouselname").attr("disabled", "disabled");
    $("#spouseemail").attr("disabled", "disabled");
    $("#coApplicant").hide();
    $("#coApplicant1").hide();  
    $("#savings").hide();
    $("#promoapplied").hide();

    if ($("#PromoID").val() == '') {
        $("#PromoID").hide();
        $("#ApplyButton").hide();
    }
    else
    {    

        $("#PromoID").removeAttr("disabled");
        $("#PromoID").show(40);
        $("#ApplyButton").show(40);
      //  $("#ApplyButton").removeAttr("disabled");
        $('#invalid').val('');       
    }

  
    var myHF = document.getElementById('hfMemberTypeJson');

    var cbxSig = $("label[for=cbxSig]").text();
    var cbxPrem = $("label[for=cbxPrem]").text();
    var cbxDiscover = $("label[for=cbxDiscover]").text();
    var cbxEntSignature = $("label[for=cbxEntSignature]").text();
    var cbxNationalEntTrial = $("label[for=cbxNationalEntTrial]").text();
    var cbxDiscover180 = $("label[for=cbxDiscover180]").text();
    var cbxNationalEntSigCAN = $("label[for=cbxNationalEntSigCAN]").text();
    var signatureText;
    var premierText;
    var discoverText;
    var entSigText;
    var nationalEntTrialText;
    var discover180Text;
    var NationalEntSigCANText;

    var MTObj;
    if (myHF.value != '') {
        MTObj = JSON.parse(myHF.value);
        if (MTObj[0].MemberTypeID == 15) {
            signatureText = MTObj[0].MemberTypeDesc.substring(19, MTObj[0].MemberTypeDesc.length) + " ($" + MTObj[0].MembershipFee + " + $" + MTObj[0].MembershipRenewalFee + "/month)";       
            premierText = '';
            discoverText = '';
            entSigText = '';
            nationalEntTrialText = '';
            discover180Text = '';
            NationalEntSigCANText= '';
        }
        else if (MTObj[0].MemberTypeID == 16) {
            premierText = MTObj[0].MemberTypeDesc.substring(19, MTObj[0].MemberTypeDesc.length) + " ($" + MTObj[0].MembershipFee + " + $" + MTObj[0].MembershipRenewalFee + "/month)";
            discoverText = '';
            signatureText = '';
            entSigText = '';
            nationalEntTrialText = '';
            discover180Text = '';
            NationalEntSigCANText= '';
        }
        else if (MTObj[0].MemberTypeID == 13) {           
            discoverText = MTObj[0].MemberTypeDesc.substring(0, MTObj[0].MemberTypeDesc.length) + " ($" + MTObj[0].MembershipFee + " + $" + MTObj[0].MembershipRenewalFee + "/month)";
            signatureText = '';
            premierText = '';
            entSigText = '';
            nationalEntTrialText = '';
            discover180Text = '';
            NationalEntSigCANText= '';
        }
        else if (MTObj[0].MemberTypeID == 30) {
            entSigText = MTObj[0].MemberTypeDesc.substring(0, MTObj[0].MemberTypeDesc.length) + " ($" + MTObj[0].MembershipFee + " + $" + MTObj[0].MembershipRenewalFee + "/month)";
            signatureText = '';
            premierText = '';
            discoverText = '';
            nationalEntTrialText = '';
            discover180Text = '';
            NationalEntSigCANText= '';
        }
        else if (MTObj[0].MemberTypeID == 36) {
            nationalEntTrialText = MTObj[0].MemberTypeDesc.substring(0, MTObj[0].MemberTypeDesc.length) + " ($" + MTObj[0].MembershipFee + " + $" + MTObj[0].MembershipRenewalFee + "/month)";
            signatureText = '';
            premierText = '';
            discoverText = '';
            entSigText = '';
            discover180Text = '';
            NationalEntSigCANText= '';
        }
        else if (MTObj[0].MemberTypeID == 42) {
            discover180Text = MTObj[0].MemberTypeDesc.substring(0, MTObj[0].MemberTypeDesc.length) + " ($" + MTObj[0].MembershipFee + " + $" + MTObj[0].MembershipRenewalFee + "/month)";
            signatureText = '';
            premierText = '';
            discoverText = '';
            nationalEntTrialText = '';
            entSigText = '';
            NationalEntSigCANText= '';
        }
        else if (MTObj[0].MemberTypeID == 34) {
            NationalEntSigCANText = MTObj[0].MemberTypeDesc.substring(0, MTObj[0].MemberTypeDesc.length) + " ($" + MTObj[0].MembershipFee + " + $" + MTObj[0].MembershipRenewalFee + "/month)";
            signatureText = '';
            premierText = '';
            discoverText = '';
            nationalEntTrialText = '';
            entSigText = '';
            discover180Text = '';
        }

        }
        if (MTObj[1] != null) {
            if (MTObj[1].MemberTypeID == 15) {
                signatureText = MTObj[1].MemberTypeDesc.substring(19, MTObj[1].MemberTypeDesc.length) + " ($" + MTObj[1].MembershipFee + " + $" + MTObj[1].MembershipRenewalFee + "/month)";               
            }
            else if (MTObj[1].MemberTypeID == 16) {
                premierText = MTObj[1].MemberTypeDesc.substring(19, MTObj[1].MemberTypeDesc.length) + " ($" + MTObj[1].MembershipFee + " + $" + MTObj[1].MembershipRenewalFee + "/month)";                
            }
            else if(MTObj[1].MemberTypeID == 13) {
                discoverText = MTObj[1].MemberTypeDesc.substring(15, MTObj[1].MemberTypeDesc.length) + " ($" + MTObj[1].MembershipFee + " + $" + MTObj[1].MembershipRenewalFee + "/month)";                
            }
            else if (MTObj[1].MemberTypeID == 30) {
                entSigText = MTObj[1].MemberTypeDesc.substring(15, MTObj[1].MemberTypeDesc.length) + " ($" + MTObj[1].MembershipFee + " + $" + MTObj[1].MembershipRenewalFee + "/month)";
            }
            else if (MTObj[1].MemberTypeID == 36) {
                nationalEntTrialText = MTObj[1].MemberTypeDesc.substring(15, MTObj[1].MemberTypeDesc.length) + " ($" + MTObj[1].MembershipFee + " + $" + MTObj[1].MembershipRenewalFee + "/month)";
            }
            else if (MTObj[1].MemberTypeID == 42) {
                discover180Text = MTObj[1].MemberTypeDesc.substring(15, MTObj[1].MemberTypeDesc.length) + " ($" + MTObj[1].MembershipFee + " + $" + MTObj[1].MembershipRenewalFee + "/month)";
            }
            else if (MTObj[1].MemberTypeID == 34) {
                NationalEntSigCANText = MTObj[1].MemberTypeDesc.substring(15, MTObj[1].MemberTypeDesc.length) + " ($" + MTObj[1].MembershipFee + " + $" + MTObj[1].MembershipRenewalFee + "/month)";
            }
        }        
    
    $("label[for='cbxSig']").text(signatureText);
    $("label[for='cbxPrem']").text(premierText);
    $("label[for='cbxDiscover']").text(discoverText);
    $("label[for='cbxEntSignature']").text(entSigText);
    $("label[for='cbxNationalEntTrial']").text(nationalEntTrialText);
    $("label[for='cbxDiscover180']").text(discover180Text);
    $("label[for='cbxNationalEntSigCAN']").text(NationalEntSigCANText);

    if (signatureText == '') {
        $("#cbxSig").hide();
        $("#cbxSig").parent().hide();
        $("#divSig").attr("style", 'display:none');
    }
    else {
        $("#divSig").attr("style", "margin-bottom:0px");
        $("#cbxSig").parent().addClass("checked");
        $("#cbxSig").attr("checked", "true");
         memberTypeDivClick();
    }
    if (premierText == '')
    {
        $("#cbxPrem").hide();
        $("#cbxPrem").parent().hide();
        $("#divPrem").attr("style", 'display:none;');  
    }
    else {
        //$("#cbxPrem").parent().addClass("checked");
        //$("#cbxPrem").attr("checked", "true");
        //memberTypeDivClick();
    }
    if (discoverText == '') {
        $("#cbxDiscover").hide();
        $("#cbxDiscover").parent().hide();
        $("#divDiscover").attr("style", 'display:none');
    }
    else {
        $("#cbxDiscover").parent().addClass("checked");  
        $("#cbxDiscover").attr("checked", "true");
        memberTypeDivClick();

    }
    if (entSigText == '') {
        $("#cbxEntSignature").hide();
        $("#cbxEntSignature").parent().hide();
        $("#divEntSignature").attr("style", 'display:none');
    }
    else {
        $("#cbxEntSignature").parent().addClass("checked"); 
        $("#cbxEntSignature").attr("checked", "true");
        memberTypeDivClick();
    }
    if (nationalEntTrialText == '') {
        $("#cbxNationalEntTrial").hide();
        $("#cbxNationalEntTrial").parent().hide();
        $("#divNationalEntTrial").attr("style", 'display:none');
    }
    else {
        $("#cbxNationalEntTrial").parent().addClass("checked");   
        $("#cbxNationalEntTrial").attr("checked", "true");
        memberTypeDivClick();
    }
    if (discover180Text == '') {
        $("#cbxDiscover180").hide();
        $("#cbxDiscover180").parent().hide();
        $("#divDiscover180").attr("style", 'display:none');
    }
    else {
        $("#cbxDiscover180").parent().addClass("checked");
        $("#cbxDiscover180").attr("checked", "true");
        memberTypeDivClick();
    }
    if (NationalEntSigCANText == '') {
        $("#cbxNationalEntSigCAN").hide();
        $("#cbxNationalEntSigCAN").parent().hide();
        $("#divNEntSigCan").attr("style", 'display:none');
    }
    else {
        $("#cbxNationalEntSigCAN").parent().addClass("checked");
        $("#cbxNationalEntSigCAN").attr("checked", "true");
        memberTypeDivClick();
    }
    
   
    //Cheking if Enter into Promo code then only enable Apply button.
    $("#PromoID").keyup(function (data) {
        if ($(this).val() != "") {
            //if membership is not selected then apply button will remain disabled
            if ($('#cbxSig').is(':checked') || $('#cbxPrem').is(':checked') || $('#cbxDiscover').is(':checked') || $('#cbxEntSignature').is(':checked') || $('#cbxNationalEntTrial').is(':checked') || $('#cbxDiscover180').is(':checked')) {
                $("#ApplyButton").removeAttr("disabled");
            }
        }
        else {
            $("#ApplyButton").attr("disabled", "disabled");
        }
    });

    // Cheking if Enter into Promo code then only enable Apply button on blur event.
    $("#PromoID").blur(function (data) {
        if ($(this).val() != "") {
            //if membership is not selected then apply button will remain disabled
            if ($('#cbxSig').is(':checked') || $('#cbxPrem').is(':checked') || $('#cbxDiscover').is(':checked') || $('#cbxEntSignature').is(':checked') || $('#cbxNationalEntTrial').is(':checked') || $('#cbxDiscover180').is(':checked')) {
                $("#ApplyButton").removeAttr("disabled");
            }
        }
        else {
            $("#ApplyButton").attr("disabled", "disabled");
        }
    });

    //when promo id field gets focus clear invalid field text
    $("#PromoID").focus(function () {
        $('#invalid').val('');
    });

    //Restricting and Formating Phone number field per US Standard
    $('#phone1').keyup(function () {
        $(this).val($(this).val().replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '$1-$2-$3'))
    });

    // Checking if Agree Terms & Condition checkbox is checked or not ?
    $("#chkIsAgreeTerms").click(function () {
        if ($("#chkIsAgreeTerms").is(':checked')) { // && $("#chkTCPAagreement").is(':checked')) {
            $("#ContractBtn").removeAttr("disabled");
        }
        else {
            $("#ContractBtn").attr("disabled", "disabled");
        }
    });

    //WI
    //Checking if TCPA Agreement checkbox is checked or not ?
    //$("#chkTCPAagreement").click(function () {
    //    if ($("#chkIsAgreeTerms").is(':checked') && $("#chkTCPAagreement").is(':checked')) {        
    //        $("#ContractBtn").removeAttr("disabled");
    //    }
    //    else {
    //        $("#ContractBtn").attr("disabled", "disabled");
    //    }
    //});

    // Toggle if Co-Applicant is check then Enable section else Disabled.
    $("#chkIsCoApplicant").click(function () {
        if ($(this).is(':checked')) {
            $("#spousefname").removeAttr("disabled");
            $("#spouselname").removeAttr("disabled");
            $("#spouseemail").removeAttr("disabled");
            $("#coApplicant").show();
            $("#coApplicant1").show();
        }
        else {
            $("#spousefname").attr("disabled", "disabled");
            $("#spouselname").attr("disabled", "disabled");
            $("#spouseemail").attr("disabled", "disabled");
            $("#coApplicant").hide();
            $("#coApplicant1").hide();
        }
    });

 

    $("#DivPromoCode").on("click", promoEnableDisable);

    function promoEnableDisable()
    {
        if ($(this).is(':checked')) {

            $("#PromoID").removeAttr("disabled");
            //$("#ApplyButton").removeAttr("disabled");
            $("#PromoID").show(40);
            $("#ApplyButton").show(40);
            $('#PromoID').val('');
            $('#invalid').val('');
            $("#PromoID").focus();
        }
        else {
            $("#PromoID").attr("disabled", "disabled");
            $("#ApplyButton").attr("disabled", "disabled");
            $("#PromoID").hide();
            $("#ApplyButton").hide();
            $('#PromoID').val('');
            $('#invalid').val('');
            $('#invalid').hide();

            memberTypeDivClick(); 
        }
    }; 

    $("#ApplyButton").click(function () {

        var login_id = '1a2ca3ddd5e77c0ca7bc5c715eb1d503';
        var login_key = '8bda89e19d045c8fc47f0de635da355077477b24';
        var url = "http://api.directbuy.com/get_promo_code_data";
        var pcode = $('#PromoID').val();

        if ($('#cbxSig').is(':checked')) {

            if (MTObj[0].MemberTypeID == 15)
            {
                var monthly = parseFloat(MTObj[0].MembershipRenewalFee).toFixed(2);
                var AmtTodiscount = parseFloat(MTObj[0].MembershipFee).toFixed(2);
            }
            else if (MTObj[1].MemberTypeID == 15) {
                var monthly = parseFloat(MTObj[1].MembershipRenewalFee).toFixed(2);
                var AmtTodiscount = parseFloat(MTObj[1].MembershipFee).toFixed(2);
            }
        }

        if ($('#cbxPrem').is(':checked')) {

            if (MTObj[0].MemberTypeID == 16) {
                var monthly = parseFloat(MTObj[0].MembershipRenewalFee).toFixed(2);
                var AmtTodiscount = parseFloat(MTObj[0].MembershipFee).toFixed(2);
            }
            else if (MTObj[1].MemberTypeID == 16) {
                var monthly = parseFloat(MTObj[1].MembershipRenewalFee).toFixed(2);
                var AmtTodiscount = parseFloat(MTObj[1].MembershipFee).toFixed(2);
            }
        }

        if ($('#cbxDiscover').is(':checked')) {

            if (MTObj[0].MemberTypeID == 13) {
                var monthly = parseFloat(MTObj[0].MembershipRenewalFee).toFixed(2);
                var AmtTodiscount = parseFloat(MTObj[0].MembershipFee).toFixed(2);
            }           
        }

        if ($('#cbxEntSignature').is(':checked')) {

            if (MTObj[0].MemberTypeID == 30) {
                var monthly = parseFloat(MTObj[0].MembershipRenewalFee).toFixed(2);
                var AmtTodiscount = parseFloat(MTObj[0].MembershipFee).toFixed(2);
            }
        }

        if ($('#cbxNationalEntTrial').is(':checked')) {

            if (MTObj[0].MemberTypeID == 36) {
                var monthly = parseFloat(MTObj[0].MembershipRenewalFee).toFixed(2);
                var AmtTodiscount = parseFloat(MTObj[0].MembershipFee).toFixed(2);
            }
        }

        if ($('#cbxDiscover180').is(':checked')) {

            if (MTObj[0].MemberTypeID == 42) {
                var monthly = parseFloat(MTObj[0].MembershipRenewalFee).toFixed(2);
                var AmtTodiscount = parseFloat(MTObj[0].MembershipFee).toFixed(2);
            }
        }

        if ($('#cbxNationalEntSigCAN').is(':checked')) {

            if (MTObj[0].MemberTypeID == 34) {
                var monthly = parseFloat(MTObj[0].MembershipRenewalFee).toFixed(2);
                var AmtTodiscount = parseFloat(MTObj[0].MembershipFee).toFixed(2);
            }
        }

        var leadInfo = {
            login_id: login_id,
            login_key: login_key,
            promo_code: pcode,
            amount_to_discount: AmtTodiscount
        };

        // console.log(leadInfo);
      
            var result = $.ajax({
                type: "POST",
                url: url,
                data: leadInfo,

                success: function (data) {
                    // console.log(data);

                    var res = JSON.parse(data);

                    if (res.promo_code_data.promo_code_status == "Active") {

                        if (parseFloat(res.promo_code_data.processed_value) > 0) {
                            $('#invalid').show();
                            $('#invalid').text("Promo Applied");

                            var processed = res.promo_code_data.processed_value;
                            var saved = (AmtTodiscount - processed).toFixed(2);
                            //  console.log(processed);

                            var total = (parseFloat(monthly) + parseFloat(processed)).toFixed(2);
                            var currency = $('#hfCurr').val();
                            $('#signupfee').text("$" + AmtTodiscount + " (" + currency + ")");
                            $('#total').text("$" + total + " (" + currency + ")");
                            $('#downpayment').val(processed);
                            $('#actualmembershipfee').val(total);
                            $('#promo').val(saved);
                            $('#savings').show();
                            $('#promoapplied').show();
                            $('#promoapplied').text("-$" + saved + " (" + currency + ")");
                        }
                        else {
                            $('#invalid').show();
                            $('#invalid').text("Promo cannot be Applied");
                            $('#savings').hide();
                            $('#promoapplied').hide();
                        }
                    }
                    else {
                        $('#invalid').show();
                        $('#invalid').text("Promo Invalid");
                        $('#savings').hide();
                        $('#promoapplied').hide();
                    }
                }
            });
        
    });

    $('#membertype').on("click", memberTypeDivClick);

    function memberTypeDivClick() {            
        if ($('#cbxSig').is(':checked')) {
            var country = $('#hfCountry').val();
            if (MTObj[0].MemberTypeID == 15) {
                var myFeeObj = MTObj[0];
            }
            else if (MTObj[1].MemberTypeID == 15) {
                var myFeeObj = MTObj[1];
            }
            if (country.substr(0, 2) == "US") {
                $('#hfCurr').val('USD');              
                processFee("USD", myFeeObj);
            }
            else {
                $('#hfCurr').val('CAD');
                processFee("CAD", myFeeObj);
            }           
        }

        if ($('#cbxPrem').is(':checked')) {
            var country = $('#hfCountry').val();
            if (MTObj[0].MemberTypeID == 16) {
                var myFeeObj = MTObj[0];
            }
            else if (MTObj[1].MemberTypeID == 16) {
                var myFeeObj = MTObj[1];
            }
            if (country.substr(0, 2) == "US") {
                $('#hfCurr').val('USD');
                processFee("USD", myFeeObj);
            }
            else {
                $('#hfCurr').val('CAD');
                processFee("CAD", myFeeObj);
            }
        }

        if ($('#cbxDiscover').is(':checked')) {
            var country = $('#hfCountry').val();
            if (MTObj[0].MemberTypeID == 13) {
                var myFeeObj = MTObj[0];
            }            
            if (country.substr(0, 2) == "US") {
                $('#hfCurr').val('USD');
                processFee("USD", myFeeObj);
            }
            else {
                $('#hfCurr').val('CAD');
                processFee("CAD", myFeeObj);
            }
        }

        if ($('#cbxEntSignature').is(':checked')) {
            var country = $('#hfCountry').val();
            if (MTObj[0].MemberTypeID == 30) {
                var myFeeObj = MTObj[0];
            }
            if (country.substr(0, 2) == "US") {
                $('#hfCurr').val('USD');
                processFee("USD", myFeeObj);
            }
            else {
                $('#hfCurr').val('CAD');
                processFee("CAD", myFeeObj);
            }
        }

        if ($('#cbxNationalEntTrial').is(':checked')) {
            var country = $('#hfCountry').val();
            if (MTObj[0].MemberTypeID == 36) {
                var myFeeObj = MTObj[0];
            }
            if (country.substr(0, 2) == "US") {
                $('#hfCurr').val('USD');
                processFee("USD", myFeeObj);
            }
            else {
                $('#hfCurr').val('CAD');
                processFee("CAD", myFeeObj);
            }
        }

        if ($('#cbxDiscover180').is(':checked')) {
            var country = $('#hfCountry').val();
            if (MTObj[0].MemberTypeID == 42) {
                var myFeeObj = MTObj[0];
            }
            if (country.substr(0, 2) == "US") {
                $('#hfCurr').val('USD');
                processFee("USD", myFeeObj);
            }
            else {
                $('#hfCurr').val('CAD');
                processFee("CAD", myFeeObj);
            }
        }

        if ($('#cbxNationalEntSigCAN').is(':checked')) {
            var country = $('#hfCountry').val();
            if (MTObj[0].MemberTypeID == 34) {
                var myFeeObj = MTObj[0];
            }
            if (country.substr(0, 2) == "US") {
                $('#hfCurr').val('USD');
                processFee("USD", myFeeObj);
            }
            else {
                $('#hfCurr').val('CAD');
                processFee("CAD", myFeeObj);
            }
        }

    };

    $('#ContractBtn').click(function () {
     //   $('#chkIsAgreeTerms').prop("checked", false);
      //  $('#chkTCPAagreement').prop("checked", false);
    });
    
    if ($("#chkIsAgreeTerms").is(':checked')) { // && $("#chkTCPAagreement").is(':checked')) {
        $("#ContractBtn").removeAttr("disabled");
    }

    function processFee(currency, myFeeObj)
    {
        if (myFeeObj != null) {
            $("#signupfee").text('$' + parseFloat(myFeeObj.MembershipFee).toFixed(2) + ' (' + currency + ')');
            $("#monthly").text('$' + parseFloat(myFeeObj.MembershipRenewalFee).toFixed(2) + ' (' + currency + ')');
            var Total = parseFloat(myFeeObj.MembershipFee, 10) + parseFloat(myFeeObj.MembershipRenewalFee, 10);
            Total = parseFloat(Total).toFixed(2);
            $("#total").text('$' + Total + ' (' + currency + ')');          
            // $('#PromoID').val('');
            $('#invalid').hide();
            $('#savings').hide();
            $('#promoapplied').hide();
            //$('#membershipfee').val(699.00);
            //var actualmembershipfee = (parseFloat(monthly) + parseFloat(signupfee)).toFixed(2);
            $('#downpayment').val(myFeeObj.MembershipFee);
            $('#actualmembershipfee').val(myFeeObj.MembershipFee);
        }
    }
   

});

$("#zip").keydown(function (e) {
    if ($("#ddlCountry").val() == 'US') {
        if (event.keyCode == 46 || event.keyCode == 8 || (event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105)) {
            // let it happen, don't do anything
        }
        else {
            // Ensure that it is a number and stop the keypress           
            event.preventDefault();
        }
    }
});


if ($('#PromoID').val() != '') {
    $("#DivPromoCode").attr("checked", "true");
}


//function changeCountry(dropdown)
//{
//    if (dropdown.value == "US") {
//        country == "USA"
//    } else {
//        country == "CAN"
//    }
//    $('#hfCountry').val(country);
    
//    if (myHF.value != '') {
//        MTObj = JSON.parse(myHF.value);
//        if (MTObj[0].MemberTypeID == 15) {
//            signatureText = MTObj[0].MemberTypeDesc.substring(19, MTObj[0].MemberTypeDesc.length) + " (one time $" + MTObj[0].MembershipFee + " + $" + MTObj[0].MembershipRenewalFee + "/month)";
//        }
//        else if (MTObj[0].MemberTypeID == 16) {
//            premierText = MTObj[0].MemberTypeDesc.substring(19, MTObj[0].MemberTypeDesc.length) + " (one time $" + MTObj[0].MembershipFee + " + $" + MTObj[0].MembershipRenewalFee + "/month)";
//        }

//        if (MTObj[1].MemberTypeID == 15) {
//            signatureText = MTObj[1].MemberTypeDesc.substring(19, MTObj[1].MemberTypeDesc.length) + " (one time $" + MTObj[1].MembershipFee + " + $" + MTObj[1].MembershipRenewalFee + "/month)";
//        }
//        else if (MTObj[1].MemberTypeID == 16) {
//            premierText = MTObj[1].MemberTypeDesc.substring(19, MTObj[1].MemberTypeDesc.length) + " (one time $" + MTObj[1].MembershipFee + " + $" + MTObj[1].MembershipRenewalFee + "/month)";
//        }
//    }
//    $("label[for='cbxSig']").text(signatureText);
//    $("label[for='cbxPrem']").text(premierText);
    



//}







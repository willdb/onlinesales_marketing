﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;

namespace OnlineSales
{
    public class DBConnection
    {        
        SqlConnection conn = new SqlConnection();
        public SqlConnection getConnection()
        {
            conn.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ToString();
            return conn;        
        }
    }
}

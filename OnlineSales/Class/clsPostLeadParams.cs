﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineSales
{
    public class clsPostLeadParams
    {
        // Mandatory Params
        public string clubs_id {get; set;}
        public string tracking_id {get; set;}
        public string first_name {get; set;}
        public string last_name {get; set;}
        public string postal_code {get; set;}
        public string email {get; set;}
        public string primary_phone {get; set;}

        // Optional Params
        public string hubspotutk {get; set;}
        public string spouse_first_name {get; set;}
        public string spouse_last_name {get; set;}
        public string address1 {get; set;}
        public string address2 { get; set; }
        public string city {get; set;}
        public string state {get; set;}
        public string country {get; set;}
        public string primary_phone_type {get; set;}
        public string secondary_phone {get; set;}
        public string secondary_phone_type {get; set;}
        public string call_disposition {get; set;}
        public string notes {get; set;}
        public string affiliate_lead_id {get; set;}
        public string utm_term {get; set;}
        public string traffic_source {get; set;}
        public string referrer { get; set; }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineSales
{
    public class clsMember
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EMail { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string LeadID { get; set; }
        public string LeadStatus { get; set; }
        public string MemberID { get; set; }
        public string SecondaryFirstName { get; set; }
        public string SecondaryLastName { get; set; }
        public string SecondaryEmail { get; set; }
        public string AccessToken { get; set; } 
        public string AgreementID { get; set; }
        public string ContractSessionKey { get; set; }
        public string LeadSource { get; set; }
        public string TrackingID { get; set; }
        public string Submit_date { get; set; }
        public OnlineSales.MembershipTypeResposne[] MTResponse { get; set; }
        public string referByMemberID { get; set; }
        public string traffic_source { get; set; }
        public string url { get; set; }
        public string Contract_Signed { get; set; }
    }
    
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://tempuri.org/", IsNullable = false)]
    public class GetMemberID
    {       
        public _Input input { get; set; }        
     
        public class _Input
        {
            public _Login Login { get; set; }
            public string GoToStatus { get; set; }
            public _MemberInfo MemberInfo { get; set; }
            public _Membership Membership { get; set; }
            public _WSOnlineInfo WSOnlineInfo { get; set; }
        }

        public class _Login
        {
            public string UserID { get; set; }
            public string Password { get; set; }
        }

        public  class _MemberInfo
        {
            public string CenterID { get; set; }
            public string BionicLeadID { get; set; }
            public string ApplicationDate { get; set; }
            public string DriverLicenseState { get; set; }
            public string PrimaryFirstName { get; set; }
            public string PrimaryLastName { get; set; }
            public string PrimaryStreet1 { get; set; }
            public string PrimaryCity { get; set; }
            public string PrimaryState { get; set; }
            public string PrimaryZip { get; set; }
            public string PrimaryCountry { get; set; }
            public string PrimaryEmail { get; set; }
            public string PrimaryPhone { get; set; }
            public string PrimaryEducation { get; set; }
            public string SecondaryFirstName { get; set; }
            public string SecondaryLastName { get; set; }
            public string SecondaryEmail { get; set; }
            public string SecondaryEducation { get; set; }
            public string DirectorID { get; set; }
        }

        public  class _Membership
        {

            public string RetailMembershipFee { get; set; }
            public string ActualMembershipFee { get; set; }
            public string FinanceOption { get; set; }
            public string MembershipType { get; set; }
            public string MembershipTerm { get; set; }
            public string ContractSignedDate { get; set; }
            public string ExpirationDate { get; set; }
            public string TaxExempt { get; set; }
            public string TaxExemptInfo { get; set; }
        }           
  
        public  class _WSOnlineInfo
        {
            public string AgreementID { get; set; }
            public string EnrollmentFee { get; set; }
            public string MonthlyFee { get; set; }
            public string LeadIDType { get; set; }
            public string ContractSession { get; set; }
            public string AdditionalNotes { get; set; }
            public string AdditionalValue01 { get; set; }
            public string AdditionalValue02 { get; set; }
            public string AdditionalValue03 { get; set; }
            public string AdditionalValue04 { get; set; }
            public string AdditionalValue05 { get; set; } 
        }       
    }


    public class Lead
    {
        public string LeadID { get; set; }
        public string SubmitDate { get; set; }
        public string ClubID { get; set; }
        public string CurrentClubID { get; set; }
        public string LeadStatus { get; set; }
        public string LeadSource { get; set; }
        public string TrackingID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; } 
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; } 
        public string PostalCode { get; set; }
        public string Email { get; set; }
        public string PrimaryPhone { get; set; }
        public string TCPA { get; set; }
    }

}